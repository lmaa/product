/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/ 	// webpack-livereload-plugin
/******/ 	(function() {
/******/ 	  if (typeof window === "undefined") { return };
/******/ 	  var id = "webpack-livereload-plugin-script";
/******/ 	  if (document.getElementById(id)) { return; }
/******/ 	  var el = document.createElement("script");
/******/ 	  el.id = id;
/******/ 	  el.async = true;
/******/ 	  el.src = "http://localhost:35729/livereload.js";
/******/ 	  document.head.appendChild(el);
/******/ 	}());

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(1);
	window.Fraction = __webpack_require__(5);
	window.interact = __webpack_require__(8);
	window.$p = __webpack_require__(9);
	window.$t = __webpack_require__(12);
	window.$l = __webpack_require__(13);
	window.$i = __webpack_require__(15);


	__webpack_require__(16);
	__webpack_require__(19);
	__webpack_require__(20);
	__webpack_require__(21);
	window.nu = __webpack_require__(10);
	window.Pattern = __webpack_require__(22);
	var T = __webpack_require__(23);
	window.Event = T.Event;
	window.Parse = __webpack_require__(24);

	__webpack_require__(25)();


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(2);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(4)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../node_modules/css-loader/index.js!./../node_modules/postcss-loader/index.js!./main.css", function() {
				var newContent = require("!!./../node_modules/css-loader/index.js!./../node_modules/postcss-loader/index.js!./main.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(3)();
	// imports


	// module
	exports.push([module.id, "* {\n  box-sizing: border-box;\n  line-height: 1.5;\n}\n\nhtml, body {\n  width: 100%;\n  height: 100%;\n  margin: 0;\n  overflow: hidden;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  color: #FFFFFF;\n}\n\ncanvas {\n  display: block;\n  position: absolute;\n  top: 0; left: 0; right: 0; bottom: 0;\n  z-index: -1;\n}\n\nbody {\n  padding: 0;\n  margin: 0;\n  font-family: sans-serif;\n  background: #111111;\n}\n\n#page-overlay {\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  position: absolute;\n  padding: 0;\n  background-color: rgba(255, 255, 255, 0.2);\n  text-shadow: 1px 1px #111111;\n  text-align: center;\n}\n\n#page-overlay button {\n    background: none;\n    border: none;\n    color: #FFFFFF;\n    text-shadow: 1px 1px #111111;\n    font-weight: bold;\n    font-size: 1em;\n    padding: 0.25em 0;\n    -webkit-transition: 300ms ease-in-out;\n    transition: 300ms ease-in-out;\n}\n\n#page-overlay button.loaded {\n    background: #2ECC40;\n    padding: 0.25em 0.75em;\n    \n}\n\n/* minimum */\n#page-overlay iframe {\n    width: calc(640/512 * 150px);\n    height: 150px;\n    margin-bottom: 0.5em;\n}\n\n/* all the landscaped mobiles */\n\n@media (orientation: landscape) and (min-height: 380px) {\n    #page-overlay iframe {\n\twidth: calc(640/512 * 220px);\n\theight: 220px;\n    }\n}\n\n@media (orientation: landscape) and (min-height: 480px) {\n    #page-overlay iframe {\n\twidth: calc(640/512 * 512px);\n\theight: 512px;\n    }\n}\n\n@media (orientation: landscape) and (min-height: 800px) {\n    #page-overlay iframe {\n\twidth: calc(640/512 * 650px);\n\theight: 650px;\n    }\n}\n\n\n/* all the portrayed devices */\n\n@media (orientation: portrait) {\n    #page-overlay iframe {\n\twidth: calc(640/512 * 200px);\n\theight: 200px;\n    }\n}\n\n@media (orientation: portrait) and (min-width: 380px) {\n    #page-overlay iframe {\n\twidth: calc(640/512 * 280px);\n\theight: 280px;\n    }\n}\n\n@media (orientation: portrait) and (min-width: 480px) {\n    #page-overlay iframe {\n\twidth: calc(640/512 * 360px);\n\theight: 360px;\n    }\n}\n\n@media (orientation: portrait) and (min-width: 600px) {\n    #page-overlay iframe {\n\twidth: calc(640/512 * 450px);\n\theight: 450px;\n    }\n}\n\n@media (orientation: portrait) and (min-width: 800px) {\n    #page-overlay iframe {\n\twidth: calc(640/512 * 550px);\n\theight: 550px;\n    }\n}\n\n\n.tool, .node {\n  font-size: 1em;\n  text-align: center;\n  font-weight: bold;\n  margin: 0 0.25em 0.25em;\n  display: block;\n  padding: 0.5em;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  opacity: 0.75;\n}\n\n.sample {\n  background-color: rgba(1, 255, 112, 0.9);\n  color: #FFFFFF;\n  text-shadow: 1px 1px #111111;\n  padding: 0;\n  border-radius: 50%;\n  z-index: 20;\n  width: 48px;\n  height: 48px;\n  text-align: center;\n  line-height: 48px;\n}\n\n.toolbar {\n  position: absolute;\n  background-color: rgba(46, 204, 64, 0.5);  \n  top: 0;\n  bottom: 0;\n  width: 33%;\n  left: 0;\n  right: 66%;\n  overflow: hidden;\n  -webkit-transform: translate(-85%, 0);\n          transform: translate(-85%, 0);\n  padding: 0.5em 0.667em;\n}\n\n.toolbar:after {\n    position: absolute;\n    bottom: 0.5em;\n    right: 0.5em;\n    content: \">>\";\n}\n\n.toolbar.active {\n  -webkit-transform: translate(0, 0);\n          transform: translate(0, 0);\n}\n\n.toolbar.active:after {\n    content: \"<<\";\n}\n\n.tool.active {\n  background-color: #FF4136;\n}\n\n.toolbar .tool {\n    display: inline-block;\n}\n\n@media (min-height: 600px) {\n    .toolbar .tool {\n\tdisplay: block;\n    }\n}\n\n@media (min-width: 667px) {\n    .toolbar {\n\twidth: 20%;\n    }\n}\n\n@media (min-height: 650px) and (min-width: 350px) {\n    .toolbar {\n\twidth: calc(48px * 2);\n    }\n}\n\n@media (min-width: 1100px) {\n    .toolbar {\n\twidth: calc(48px * 2);\n    }\n}\n\n.orbit {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n}\n\n.chain {\n  background: rgba(57, 204, 204, 0.7);\n}\n\n.toolbar.active + .orbit {\n  left: 10%;\n}\n\n\n.segment {\n  -webkit-transform-origin: 0 0;\n          transform-origin: 0 0;\n  height: 5px;\n  background-color: #01FF70;\n  z-index: 0;\n}\n\n\n.sample[data-n]:before {\n    content: \"\";\n    position: absolute;\n    z-index: 10;\n    background: transparent;\n    border: 0px solid #2ECC40;\n    display: block;\n    border-radius: 50%;\n    height: 48px;\n    width: 48px;\n    box-sizing: content-box;\n}\n\n.sample[data-n=\"1\"]:before {\n    border-width: 2px;\n    top: -2px;\n    left: -2px;\n}\n\n.sample[data-n=\"2\"]:before {\n    border-width: 4px;\n    top: -4px;\n    left: -4px;\n}\n\n.sample[data-n=\"3\"]:before {\n    border-width: 6px;\n    top: -6px;\n    left: -6px;\n}\n\n.sample[data-n=\"4\"]:before {\n    border-width: 8px;\n    top: -8px;\n    left: -8px;\n}", ""]);

	// exports


/***/ },
/* 3 */
/***/ function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];

		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};

		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];

	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}

		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();

		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";

		var styles = listToStyles(list);
		addStylesToDom(styles, options);

		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}

	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}

	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}

	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}

	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}

	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}

	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}

	function addStyle(obj, options) {
		var styleElement, update, remove;

		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}

		update(obj);

		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}

	var replaceText = (function () {
		var textStore = [];

		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();

	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;

		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}

	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;

		if(media) {
			styleElement.setAttribute("media", media)
		}

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}

	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;

		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}

		var blob = new Blob([css], { type: "text/css" });

		var oldSrc = linkElement.href;

		linkElement.href = URL.createObjectURL(blob);

		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(module) {/**
	 * @license Fraction.js v3.3.0 09/09/2015
	 * http://www.xarg.org/2014/03/precise-calculations-in-javascript/
	 *
	 * Copyright (c) 2015, Robert Eisele (robert@xarg.org)
	 * Dual licensed under the MIT or GPL Version 2 licenses.
	 **/


	/**
	 *
	 * This class offers the possibility to calculate fractions.
	 * You can pass a fraction in different formats. Either as array, as double, as string or as an integer.
	 *
	 * Array/Object form
	 * [ 0 => <nominator>, 1 => <denominator> ]
	 * [ n => <nominator>, d => <denominator> ]
	 *
	 * Integer form
	 * - Single integer value
	 *
	 * Double form
	 * - Single double value
	 *
	 * String form
	 * 123.456 - a simple double
	 * 123/456 - a string fraction
	 * 123.'456' - a double with repeating decimal places
	 * 123.(456) - synonym
	 * 123.45'6' - a double with repeating last place
	 * 123.45(6) - synonym
	 *
	 * Example:
	 *
	 * var f = new Fraction("9.4'31'");
	 * f.mul([-4, 3]).div(4.9);
	 *
	 */

	(function(root) {

	  "use strict";

	  // Maximum search depth for cyclic rational numbers. 2000 should be more than enough. 
	  // Example: 1/7 = 0.(142857) has 6 repeating decimal places.
	  // If MAX_CYCLE_LEN gets reduced, long cycles will not be detected and toString() only gets the first 10 digits
	  var MAX_CYCLE_LEN = 2000;

	  // Parsed data to avoid calling "new" all the time
	  var P = {
	    "s": 1,
	    "n": 0,
	    "d": 1
	  };

	  function assign(n, s) {

	    if (isNaN(n = parseInt(n, 10))) {
	      throwInvalidParam();
	    }
	    return n * s;
	  }

	  function throwInvalidParam() {
	    throw "Invalid Param";
	  }

	  var parse = function(p1, p2) {

	    var n = 0, d = 1, s = 1;
	    var v = 0, w = 0, x = 0, y = 1, z = 1;

	    var A = 0, B = 1;
	    var C = 1, D = 1;

	    var N = 10000000;
	    var M;

	    if (p1 === undefined || p1 === null) {
	      /* void */
	    } else if (p2 !== undefined) {
	      n = p1;
	      d = p2;
	      s = n * d;
	    } else
	      switch (typeof p1) {

	        case "object":
	        {
	          if ("d" in p1 && "n" in p1) {
	            n = p1["n"];
	            d = p1["d"];
	            if ("s" in p1)
	              n*= p1["s"];
	          } else if (0 in p1) {
	            n = p1[0];
	            if (1 in p1)
	              d = p1[1];
	          } else {
	            throwInvalidParam();
	          }
	          s = n * d;
	          break;
	        }
	        case "number":
	        {
	          if (p1 < 0) {
	            s = p1;
	            p1 = -p1;
	          }

	          if (p1 % 1 === 0) {
	            n = p1;
	          } else if (p1 > 0) { // check for != 0, scale would become NaN (log(0)), which converges really slow

	            if (p1 >= 1) {
	              z = Math.pow(10, Math.floor(1 + Math.log(p1) / Math.LN10));
	              p1/= z;
	            }

	            // Using Farey Sequences
	            // http://www.johndcook.com/blog/2010/10/20/best-rational-approximation/

	            while (B <= N && D <= N) {
	              M = (A + C) / (B + D);

	              if (p1 === M) {
	                if (B + D <= N) {
	                  n = A + C;
	                  d = B + D;
	                } else if (D > B) {
	                  n = C;
	                  d = D;
	                } else {
	                  n = A;
	                  d = B;
	                }
	                break;

	              } else {

	                if (p1 > M) {
	                  A+= C;
	                  B+= D;
	                } else {
	                  C+= A;
	                  D+= B;
	                }

	                if (B > N) {
	                  n = C;
	                  d = D;
	                } else {
	                  n = A;
	                  d = B;
	                }
	              }
	            }
	            n*= z;
	          } else if (isNaN(p1) || isNaN(p2)) {
	            d = n = NaN;
	          }
	          break;
	        }
	        case "string":
	        {
	          B = p1.match(/\d+|./g);

	          if (B[A] === '-') {// Check for minus sign at the beginning
	            s = -1;
	            A++;
	          } else if (B[A] === '+') {// Check for plus sign at the beginning
	            A++;
	          }

	          if (B.length === A + 1) { // Check if it's just a simple number "1234"
	            w = assign(B[A++], s);
	          } else if (B[A + 1] === '.' || B[A] === '.') { // Check if it's a decimal number

	            if (B[A] !== '.') { // Handle 0.5 and .5
	              v = assign(B[A++], s);
	            }
	            A++;

	            // Check for decimal places
	            if (A + 1 === B.length || B[A + 1] === '(' && B[A + 3] === ')' || B[A + 1] === "'" && B[A + 3] === "'") {
	              w = assign(B[A], s);
	              y = Math.pow(10, B[A].length);
	              A++;
	            }

	            // Check for repeating places
	            if (B[A] === '(' && B[A + 2] === ')' || B[A] === "'" && B[A + 2] === "'") {
	              x = assign(B[A + 1], s);
	              z = Math.pow(10, B[A + 1].length) - 1;
	              A+= 3;
	            }

	          } else if (B[A + 1] === '/' || B[A + 1] === ':') { // Check for a simple fraction "123/456" or "123:456"
	            w = assign(B[A], s);
	            y = assign(B[A + 2], 1);
	            A+= 3;
	          } else if (B[A + 3] === '/' && B[A + 1] === ' ') { // Check for a complex fraction "123 1/2"
	            v = assign(B[A], s);
	            w = assign(B[A + 2], s);
	            y = assign(B[A + 4], 1);
	            A+= 5;
	          }

	          if (B.length <= A) { // Check for more tokens on the stack
	            d = y * z;
	            s = /* void */
	                    n = x + d * v + z * w;
	            break;
	          }

	          /* Fall through on error */
	        }
	        default:
	          throwInvalidParam();
	      }

	    if (d === 0) {
	      throw "DIV/0";
	    }

	    P["s"] = s < 0 ? -1 : 1;
	    P["n"] = Math.abs(n);
	    P["d"] = Math.abs(d);
	  };

	  var modpow = function(b, e, m) {

	    for (var r = 1; e > 0; b = (b * b) % m, e >>= 1) {

	      if (e & 1) {
	        r = (r * b) % m;
	      }
	    }
	    return r;
	  };

	  var cycleLen = function(n, d) {

	    for (; d % 2 === 0;
	            d/= 2) {}

	    for (; d % 5 === 0;
	            d/= 5) {}

	    if (d === 1) // Catch non-cyclic numbers
	      return 0;

	    // If we would like to compute really large numbers quicker, we could make use of Fermat's little theorem:
	    // 10^(d-1) % d == 1
	    // However, we don't need such large numbers and MAX_CYCLE_LEN should be the capstone, 
	    // as we want to translate the numbers to strings.

	    var rem = 10 % d;

	    for (var t = 1; rem !== 1; t++) {
	      rem = rem * 10 % d;

	      if (t > MAX_CYCLE_LEN)
	        return 0; // Returning 0 here means that we don't print it as a cyclic number. It's likely that the answer is `d-1`
	    }
	    return t;
	  };

	  var cycleStart = function(n, d, len) {

	    var rem1 = 1;
	    var rem2 = modpow(10, len, d);

	    for (var t = 0; t < 300; t++) { // s < ~log10(Number.MAX_VALUE)
	      // Solve 10^s == 10^(s+t) (mod d)

	      if (rem1 === rem2)
	        return t;

	      rem1 = rem1 * 10 % d;
	      rem2 = rem2 * 10 % d;
	    }
	    return 0;
	  };

	  var gcd = function(a, b) {

	    if (!a) return b;
	    if (!b) return a;

	    while (1) {
	      a%= b;
	      if (!a) return b;
	      b%= a;
	      if (!b) return a;
	    }
	  };

	  /**
	   * Module constructor
	   *
	   * @constructor
	   * @param {number|Fraction} a
	   * @param {number=} b
	   */
	  function Fraction(a, b) {

	    if (!(this instanceof Fraction)) {
	      return new Fraction(a, b);
	    }

	    parse(a, b);

	    if (Fraction['REDUCE']) {
	      a = gcd(P["d"], P["n"]); // Abuse a
	    } else {
	      a = 1;
	    }

	    this["s"] = P["s"];
	    this["n"] = P["n"] / a;
	    this["d"] = P["d"] / a;
	  }

	  /**
	   * Boolean global variable to be able to disable automatic reduction of the fraction
	   *
	   */
	  Fraction['REDUCE'] = 1;

	  Fraction.prototype = {

	    "s": 1,
	    "n": 0,
	    "d": 1,

	    /**
	     * Calculates the absolute value
	     *
	     * Ex: new Fraction(-4).abs() => 4
	     **/
	    "abs": function() {

	      return new Fraction(this["n"], this["d"]);
	    },

	    /**
	     * Inverts the sign of the current fraction
	     *
	     * Ex: new Fraction(-4).neg() => 4
	     **/
	    "neg": function() {

	      return new Fraction(-this["s"] * this["n"], this["d"]);
	    },

	    /**
	     * Adds two rational numbers
	     *
	     * Ex: new Fraction({n: 2, d: 3}).add("14.9") => 467 / 30
	     **/
	    "add": function(a, b) {

	      parse(a, b);
	      return new Fraction(
	              this["s"] * this["n"] * P["d"] + P["s"] * this["d"] * P["n"],
	              this["d"] * P["d"]
	              );
	    },

	    /**
	     * Subtracts two rational numbers
	     *
	     * Ex: new Fraction({n: 2, d: 3}).add("14.9") => -427 / 30
	     **/
	    "sub": function(a, b) {

	      parse(a, b);
	      return new Fraction(
	              this["s"] * this["n"] * P["d"] - P["s"] * this["d"] * P["n"],
	              this["d"] * P["d"]
	              );
	    },

	    /**
	     * Multiplies two rational numbers
	     *
	     * Ex: new Fraction("-17.(345)").mul(3) => 5776 / 111
	     **/
	    "mul": function(a, b) {

	      parse(a, b);
	      return new Fraction(
	              this["s"] * P["s"] * this["n"] * P["n"],
	              this["d"] * P["d"]
	              );
	    },

	    /**
	     * Divides two rational numbers
	     *
	     * Ex: new Fraction("-17.(345)").inverse().div(3)
	     **/
	    "div": function(a, b) {

	      parse(a, b);
	      return new Fraction(
	              this["s"] * P["s"] * this["n"] * P["d"],
	              this["d"] * P["n"]
	              );
	    },

	    /**
	     * Clones the actual object
	     *
	     * Ex: new Fraction("-17.(345)").clone()
	     **/
	    "clone": function() {
	      return new Fraction(this);
	    },

	    /**
	     * Calculates the modulo of two rational numbers - a more precise fmod
	     *
	     * Ex: new Fraction('4.(3)').mod([7, 8]) => (13/3) % (7/8) = (5/6)
	     **/
	    "mod": function(a, b) {

	      if (isNaN(this['n']) || isNaN(this['d'])) {
	        return new Fraction(NaN);
	      }

	      if (a === undefined) {
	        return new Fraction(this["s"] * this["n"] % this["d"], 1);
	      }

	      parse(a, b);
	      if (0 === P["n"] && 0 === this["d"]) {
	        Fraction(0, 0); // Throw div/0
	      }

	      /*
	       * First silly attempt, kinda slow
	       *
	       return that["sub"]({
	       "n": num["n"] * Math.floor((this.n / this.d) / (num.n / num.d)),
	       "d": num["d"],
	       "s": this["s"]
	       });*/

	      /*
	       * New attempt: a1 / b1 = a2 / b2 * q + r
	       * => b2 * a1 = a2 * b1 * q + b1 * b2 * r
	       * => (b2 * a1 % a2 * b1) / (b1 * b2)
	       */
	      return new Fraction(
	              (this["s"] * P["d"] * this["n"]) % (P["n"] * this["d"]),
	              P["d"] * this["d"]
	              );
	    },

	    /**
	     * Calculates the fractional gcd of two rational numbers
	     *
	     * Ex: new Fraction(5,8).gcd(3,7) => 1/56
	     */
	    "gcd": function(a, b) {

	      parse(a, b);

	      // gcd(a / b, c / d) = gcd(a, c) / lcm(b, d)

	      return new Fraction(gcd(P["n"], this["n"]), P["d"] * this["d"] / gcd(P["d"], this["d"]));
	    },

	    /**
	     * Calculates the fractional lcm of two rational numbers
	     *
	     * Ex: new Fraction(5,8).lcm(3,7) => 15
	     */
	    "lcm": function(a, b) {

	      parse(a, b);

	      // lcm(a / b, c / d) = lcm(a, c) / gcd(b, d)

	      if (P["n"] === 0 && this["n"] === 0) {
	        return new Fraction;
	      }
	      return new Fraction(P["n"] * this["n"] / gcd(P["n"], this["n"]), gcd(P["d"], this["d"]));
	    },

	    /**
	     * Calculates the ceil of a rational number
	     *
	     * Ex: new Fraction('4.(3)').ceil() => (5 / 1)
	     **/
	    "ceil": function(places) {

	      places = Math.pow(10, places || 0);

	      if (isNaN(this["n"]) || isNaN(this["d"])) {
	        return new Fraction(NaN);
	      }
	      return new Fraction(Math.ceil(places * this["s"] * this["n"] / this["d"]), places);
	    },

	    /**
	     * Calculates the floor of a rational number
	     *
	     * Ex: new Fraction('4.(3)').floor() => (4 / 1)
	     **/
	    "floor": function(places) {

	      places = Math.pow(10, places || 0);

	      if (isNaN(this["n"]) || isNaN(this["d"])) {
	        return new Fraction(NaN);
	      }
	      return new Fraction(Math.floor(places * this["s"] * this["n"] / this["d"]), places);
	    },

	    /**
	     * Rounds a rational numbers
	     *
	     * Ex: new Fraction('4.(3)').round() => (4 / 1)
	     **/
	    "round": function(places) {

	      places = Math.pow(10, places || 0);

	      if (isNaN(this["n"]) || isNaN(this["d"])) {
	        return new Fraction(NaN);
	      }
	      return new Fraction(Math.round(places * this["s"] * this["n"] / this["d"]), places);
	    },

	    /**
	     * Gets the inverse of the fraction, means numerator and denumerator are exchanged
	     *
	     * Ex: new Fraction([-3, 4]).inverse() => -4 / 3
	     **/
	    "inverse": function() {

	      return new Fraction(this["s"] * this["d"], this["n"]);
	    },

	    /**
	     * Calculates the fraction to some integer exponent
	     *
	     * Ex: new Fraction(-1,2).pow(-3) => -8
	     */
	    "pow": function(m) {

	      if (m < 0) {
	        return new Fraction(Math.pow(this['s'] * this["d"],-m), Math.pow(this["n"],-m));
	      } else {
	        return new Fraction(Math.pow(this['s'] * this["n"], m), Math.pow(this["d"], m));
	      }
	    },

	    /**
	     * Check if two rational numbers are the same
	     *
	     * Ex: new Fraction(19.6).equals([98, 5]);
	     **/
	    "equals": function(a, b) {

	      parse(a, b);
	      return this["s"] * this["n"] * P["d"] === P["s"] * P["n"] * this["d"]; // Same as compare() === 0
	    },

	    /**
	     * Check if two rational numbers are the same
	     *
	     * Ex: new Fraction(19.6).equals([98, 5]);
	     **/
	    "compare": function(a, b) {

	      parse(a, b);
	      var t = (this["s"] * this["n"] * P["d"] - P["s"] * P["n"] * this["d"]);
	      return (0 < t) - (t < 0);
	    },

	    /**
	     * Check if two rational numbers are divisible
	     *
	     * Ex: new Fraction(19.6).divisible(1.5);
	     */
	    "divisible": function(a, b) {

	      parse(a, b);
	      return !(!(P["n"] * this["d"]) || ((this["n"] * P["d"]) % (P["n"] * this["d"])));
	    },

	    /**
	     * Returns a decimal representation of the fraction
	     *
	     * Ex: new Fraction("100.'91823'").valueOf() => 100.91823918239183
	     **/
	    'valueOf': function() {

	      return this["s"] * this["n"] / this["d"];
	    },

	    /**
	     * Returns a string-fraction representation of a Fraction object
	     *
	     * Ex: new Fraction("1.'3'").toFraction() => "4 1/3"
	     **/
	    'toFraction': function(excludeWhole) {

	      var whole, str = "";
	      var n = this["n"];
	      var d = this["d"];
	      if (this["s"] < 0) {
	        str+= '-';
	      }

	      if (d === 1) {
	        str+= n;
	      } else {

	        if (excludeWhole && (whole = Math.floor(n / d)) > 0) {
	          str+= whole;
	          str+= " ";
	          n%= d;
	        }

	        str+= n;
	        str+= '/';
	        str+= d;
	      }
	      return str;
	    },

	    /**
	     * Returns a latex representation of a Fraction object
	     *
	     * Ex: new Fraction("1.'3'").toLatex() => "\frac{4}{3}"
	     **/
	    'toLatex': function(excludeWhole) {

	      var whole, str = "";
	      var n = this["n"];
	      var d = this["d"];
	      if (this["s"] < 0) {
	        str+= '-';
	      }

	      if (d === 1) {
	        str+= n;
	      } else {

	        if (excludeWhole && (whole = Math.floor(n / d)) > 0) {
	          str+= whole;
	          n%= d;
	        }

	        str+= "\\frac{";
	        str+= n;
	        str+= '}{';
	        str+= d;
	        str+= '}';
	      }
	      return str;
	    },

	    /**
	     * Returns an array of continued fraction elements
	     * 
	     * Ex: new Fraction("7/8").toContinued() => [0,1,7]
	     */
	    'toContinued': function() {

	      var t;
	      var a = this['n'];
	      var b = this['d'];
	      var res = [];

	      do {
	        res.push(Math.floor(a / b));
	        t = a % b;
	        a = b;
	        b = t;
	      } while (a !== 1);

	      return res;
	    },

	    /**
	     * Creates a string representation of a fraction with all digits
	     *
	     * Ex: new Fraction("100.'91823'").toString() => "100.(91823)"
	     **/
	    'toString': function() {

	      var g;
	      var N = this["n"];
	      var D = this["d"];

	      if (isNaN(N) || isNaN(D)) {
	        return "NaN";
	      }

	      if (!Fraction['REDUCE']) {
	        g = gcd(N, D);
	        N/= g;
	        D/= g;
	      }

	      var p = String(N).split(""); // Numerator chars
	      var t = 0; // Tmp var

	      var ret = [~this["s"] ? "" : "-", "", ""]; // Return array, [0] is zero sign, [1] before comma, [2] after
	      var zeros = ""; // Collection variable for zeros

	      var cycLen = cycleLen(N, D); // Cycle length
	      var cycOff = cycleStart(N, D, cycLen); // Cycle start

	      var j = -1;
	      var n = 1; // str index

	      // rough estimate to fill zeros
	      var length = 10 + cycLen + cycOff + p.length; // 10 = decimal places when no repitation

	      for (var i = 0; i < length; i++, t*= 10) {

	        if (i < p.length) {
	          t+= Number(p[i]);
	        } else {
	          n = 2;
	          j++; // Start now => after comma
	        }

	        if (cycLen > 0) { // If we have a repeating part
	          if (j === cycOff) {
	            ret[n]+= zeros + "(";
	            zeros = "";
	          } else if (j === cycLen + cycOff) {
	            ret[n]+= zeros + ")";
	            break;
	          }
	        }

	        if (t >= D) {
	          ret[n]+= zeros + ((t / D) | 0); // Flush zeros, Add current digit
	          zeros = "";
	          t = t % D;
	        } else if (n > 1) { // Add zeros to the zero buffer
	          zeros+= "0";
	        } else if (ret[n]) { // If before comma, add zero only if already something was added
	          ret[n]+= "0";
	        }
	      }

	      // If it's empty, it's a leading zero only
	      ret[0]+= ret[1] || "0";

	      // If there is something after the comma, add the comma sign
	      if (ret[2]) {
	        return ret[0] + "." + ret[2];
	      }
	      return ret[0];
	    }
	  };

	  if ("function" === "function" && __webpack_require__(7)["amd"]) {
	    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function() {
	      return Fraction;
	    }.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  } else if (true) {
	    module["exports"] = Fraction;
	  } else {
	    root['Fraction'] = Fraction;
	  }

	})(this);

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(6)(module)))

/***/ },
/* 6 */
/***/ function(module, exports) {

	module.exports = function(module) {
		if(!module.webpackPolyfill) {
			module.deprecate = function() {};
			module.paths = [];
			// module.parent = undefined by default
			module.children = [];
			module.webpackPolyfill = 1;
		}
		return module;
	}


/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = function() { throw new Error("define cannot be used indirect"); };


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * interact.js v1.2.6
	 *
	 * Copyright (c) 2012-2015 Taye Adeyemi <dev@taye.me>
	 * Open source under the MIT License.
	 * https://raw.github.com/taye/interact.js/master/LICENSE
	 */
	(function (realWindow) {
	    'use strict';

	    // return early if there's no window to work with (eg. Node.js)
	    if (!realWindow) { return; }

	    var // get wrapped window if using Shadow DOM polyfill
	        window = (function () {
	            // create a TextNode
	            var el = realWindow.document.createTextNode('');

	            // check if it's wrapped by a polyfill
	            if (el.ownerDocument !== realWindow.document
	                && typeof realWindow.wrap === 'function'
	                && realWindow.wrap(el) === el) {
	                // return wrapped window
	                return realWindow.wrap(realWindow);
	            }

	            // no Shadow DOM polyfil or native implementation
	            return realWindow;
	        }()),

	        document           = window.document,
	        DocumentFragment   = window.DocumentFragment   || blank,
	        SVGElement         = window.SVGElement         || blank,
	        SVGSVGElement      = window.SVGSVGElement      || blank,
	        SVGElementInstance = window.SVGElementInstance || blank,
	        HTMLElement        = window.HTMLElement        || window.Element,

	        PointerEvent = (window.PointerEvent || window.MSPointerEvent),
	        pEventTypes,

	        hypot = Math.hypot || function (x, y) { return Math.sqrt(x * x + y * y); },

	        tmpXY = {},     // reduce object creation in getXY()

	        documents       = [],   // all documents being listened to

	        interactables   = [],   // all set interactables
	        interactions    = [],   // all interactions

	        dynamicDrop     = false,

	        // {
	        //      type: {
	        //          selectors: ['selector', ...],
	        //          contexts : [document, ...],
	        //          listeners: [[listener, useCapture], ...]
	        //      }
	        //  }
	        delegatedEvents = {},

	        defaultOptions = {
	            base: {
	                accept        : null,
	                actionChecker : null,
	                styleCursor   : true,
	                preventDefault: 'auto',
	                origin        : { x: 0, y: 0 },
	                deltaSource   : 'page',
	                allowFrom     : null,
	                ignoreFrom    : null,
	                _context      : document,
	                dropChecker   : null
	            },

	            drag: {
	                enabled: false,
	                manualStart: true,
	                max: Infinity,
	                maxPerElement: 1,

	                snap: null,
	                restrict: null,
	                inertia: null,
	                autoScroll: null,

	                axis: 'xy'
	            },

	            drop: {
	                enabled: false,
	                accept: null,
	                overlap: 'pointer'
	            },

	            resize: {
	                enabled: false,
	                manualStart: false,
	                max: Infinity,
	                maxPerElement: 1,

	                snap: null,
	                restrict: null,
	                inertia: null,
	                autoScroll: null,

	                square: false,
	                preserveAspectRatio: false,
	                axis: 'xy',

	                // use default margin
	                margin: NaN,

	                // object with props left, right, top, bottom which are
	                // true/false values to resize when the pointer is over that edge,
	                // CSS selectors to match the handles for each direction
	                // or the Elements for each handle
	                edges: null,

	                // a value of 'none' will limit the resize rect to a minimum of 0x0
	                // 'negate' will alow the rect to have negative width/height
	                // 'reposition' will keep the width/height positive by swapping
	                // the top and bottom edges and/or swapping the left and right edges
	                invert: 'none'
	            },

	            gesture: {
	                manualStart: false,
	                enabled: false,
	                max: Infinity,
	                maxPerElement: 1,

	                restrict: null
	            },

	            perAction: {
	                manualStart: false,
	                max: Infinity,
	                maxPerElement: 1,

	                snap: {
	                    enabled     : false,
	                    endOnly     : false,
	                    range       : Infinity,
	                    targets     : null,
	                    offsets     : null,

	                    relativePoints: null
	                },

	                restrict: {
	                    enabled: false,
	                    endOnly: false
	                },

	                autoScroll: {
	                    enabled     : false,
	                    container   : null,     // the item that is scrolled (Window or HTMLElement)
	                    margin      : 60,
	                    speed       : 300       // the scroll speed in pixels per second
	                },

	                inertia: {
	                    enabled          : false,
	                    resistance       : 10,    // the lambda in exponential decay
	                    minSpeed         : 100,   // target speed must be above this for inertia to start
	                    endSpeed         : 10,    // the speed at which inertia is slow enough to stop
	                    allowResume      : true,  // allow resuming an action in inertia phase
	                    zeroResumeDelta  : true,  // if an action is resumed after launch, set dx/dy to 0
	                    smoothEndDuration: 300    // animate to snap/restrict endOnly if there's no inertia
	                }
	            },

	            _holdDuration: 600
	        },

	        // Things related to autoScroll
	        autoScroll = {
	            interaction: null,
	            i: null,    // the handle returned by window.setInterval
	            x: 0, y: 0, // Direction each pulse is to scroll in

	            // scroll the window by the values in scroll.x/y
	            scroll: function () {
	                var options = autoScroll.interaction.target.options[autoScroll.interaction.prepared.name].autoScroll,
	                    container = options.container || getWindow(autoScroll.interaction.element),
	                    now = new Date().getTime(),
	                    // change in time in seconds
	                    dtx = (now - autoScroll.prevTimeX) / 1000,
	                    dty = (now - autoScroll.prevTimeY) / 1000,
	                    vx, vy, sx, sy;

	                // displacement
	                if (options.velocity) {
	                  vx = options.velocity.x;
	                  vy = options.velocity.y;
	                }
	                else {
	                  vx = vy = options.speed
	                }
	 
	                sx = vx * dtx;
	                sy = vy * dty;

	                if (sx >= 1 || sy >= 1) {
	                    if (isWindow(container)) {
	                        container.scrollBy(autoScroll.x * sx, autoScroll.y * sy);
	                    }
	                    else if (container) {
	                        container.scrollLeft += autoScroll.x * sx;
	                        container.scrollTop  += autoScroll.y * sy;
	                    }

	                    if (sx >=1) autoScroll.prevTimeX = now;
	                    if (sy >= 1) autoScroll.prevTimeY = now;
	                }

	                if (autoScroll.isScrolling) {
	                    cancelFrame(autoScroll.i);
	                    autoScroll.i = reqFrame(autoScroll.scroll);
	                }
	            },

	            isScrolling: false,
	            prevTimeX: 0,
	            prevTimeY: 0,

	            start: function (interaction) {
	                autoScroll.isScrolling = true;
	                cancelFrame(autoScroll.i);

	                autoScroll.interaction = interaction;
	                autoScroll.prevTimeX = new Date().getTime();
	                autoScroll.prevTimeY = new Date().getTime();
	                autoScroll.i = reqFrame(autoScroll.scroll);
	            },

	            stop: function () {
	                autoScroll.isScrolling = false;
	                cancelFrame(autoScroll.i);
	            }
	        },

	        // Does the browser support touch input?
	        supportsTouch = (('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch),

	        // Does the browser support PointerEvents
	        supportsPointerEvent = !!PointerEvent,

	        // Less Precision with touch input
	        margin = supportsTouch || supportsPointerEvent? 20: 10,

	        pointerMoveTolerance = 1,

	        // for ignoring browser's simulated mouse events
	        prevTouchTime = 0,

	        // Allow this many interactions to happen simultaneously
	        maxInteractions = Infinity,

	        // Check if is IE9 or older
	        actionCursors = (document.all && !window.atob) ? {
	            drag    : 'move',
	            resizex : 'e-resize',
	            resizey : 's-resize',
	            resizexy: 'se-resize',

	            resizetop        : 'n-resize',
	            resizeleft       : 'w-resize',
	            resizebottom     : 's-resize',
	            resizeright      : 'e-resize',
	            resizetopleft    : 'se-resize',
	            resizebottomright: 'se-resize',
	            resizetopright   : 'ne-resize',
	            resizebottomleft : 'ne-resize',

	            gesture : ''
	        } : {
	            drag    : 'move',
	            resizex : 'ew-resize',
	            resizey : 'ns-resize',
	            resizexy: 'nwse-resize',

	            resizetop        : 'ns-resize',
	            resizeleft       : 'ew-resize',
	            resizebottom     : 'ns-resize',
	            resizeright      : 'ew-resize',
	            resizetopleft    : 'nwse-resize',
	            resizebottomright: 'nwse-resize',
	            resizetopright   : 'nesw-resize',
	            resizebottomleft : 'nesw-resize',

	            gesture : ''
	        },

	        actionIsEnabled = {
	            drag   : true,
	            resize : true,
	            gesture: true
	        },

	        // because Webkit and Opera still use 'mousewheel' event type
	        wheelEvent = 'onmousewheel' in document? 'mousewheel': 'wheel',

	        eventTypes = [
	            'dragstart',
	            'dragmove',
	            'draginertiastart',
	            'dragend',
	            'dragenter',
	            'dragleave',
	            'dropactivate',
	            'dropdeactivate',
	            'dropmove',
	            'drop',
	            'resizestart',
	            'resizemove',
	            'resizeinertiastart',
	            'resizeend',
	            'gesturestart',
	            'gesturemove',
	            'gestureinertiastart',
	            'gestureend',

	            'down',
	            'move',
	            'up',
	            'cancel',
	            'tap',
	            'doubletap',
	            'hold'
	        ],

	        globalEvents = {},

	        // Opera Mobile must be handled differently
	        isOperaMobile = navigator.appName == 'Opera' &&
	            supportsTouch &&
	            navigator.userAgent.match('Presto'),

	        // scrolling doesn't change the result of getClientRects on iOS 7
	        isIOS7 = (/iP(hone|od|ad)/.test(navigator.platform)
	                         && /OS 7[^\d]/.test(navigator.appVersion)),

	        // prefix matchesSelector
	        prefixedMatchesSelector = 'matches' in Element.prototype?
	                'matches': 'webkitMatchesSelector' in Element.prototype?
	                    'webkitMatchesSelector': 'mozMatchesSelector' in Element.prototype?
	                        'mozMatchesSelector': 'oMatchesSelector' in Element.prototype?
	                            'oMatchesSelector': 'msMatchesSelector',

	        // will be polyfill function if browser is IE8
	        ie8MatchesSelector,

	        // native requestAnimationFrame or polyfill
	        reqFrame = realWindow.requestAnimationFrame,
	        cancelFrame = realWindow.cancelAnimationFrame,

	        // Events wrapper
	        events = (function () {
	            var useAttachEvent = ('attachEvent' in window) && !('addEventListener' in window),
	                addEvent       = useAttachEvent?  'attachEvent': 'addEventListener',
	                removeEvent    = useAttachEvent?  'detachEvent': 'removeEventListener',
	                on             = useAttachEvent? 'on': '',

	                elements          = [],
	                targets           = [],
	                attachedListeners = [];

	            function add (element, type, listener, useCapture) {
	                var elementIndex = indexOf(elements, element),
	                    target = targets[elementIndex];

	                if (!target) {
	                    target = {
	                        events: {},
	                        typeCount: 0
	                    };

	                    elementIndex = elements.push(element) - 1;
	                    targets.push(target);

	                    attachedListeners.push((useAttachEvent ? {
	                            supplied: [],
	                            wrapped : [],
	                            useCount: []
	                        } : null));
	                }

	                if (!target.events[type]) {
	                    target.events[type] = [];
	                    target.typeCount++;
	                }

	                if (!contains(target.events[type], listener)) {
	                    var ret;

	                    if (useAttachEvent) {
	                        var listeners = attachedListeners[elementIndex],
	                            listenerIndex = indexOf(listeners.supplied, listener);

	                        var wrapped = listeners.wrapped[listenerIndex] || function (event) {
	                            if (!event.immediatePropagationStopped) {
	                                event.target = event.srcElement;
	                                event.currentTarget = element;

	                                event.preventDefault = event.preventDefault || preventDef;
	                                event.stopPropagation = event.stopPropagation || stopProp;
	                                event.stopImmediatePropagation = event.stopImmediatePropagation || stopImmProp;

	                                if (/mouse|click/.test(event.type)) {
	                                    event.pageX = event.clientX + getWindow(element).document.documentElement.scrollLeft;
	                                    event.pageY = event.clientY + getWindow(element).document.documentElement.scrollTop;
	                                }

	                                listener(event);
	                            }
	                        };

	                        ret = element[addEvent](on + type, wrapped, Boolean(useCapture));

	                        if (listenerIndex === -1) {
	                            listeners.supplied.push(listener);
	                            listeners.wrapped.push(wrapped);
	                            listeners.useCount.push(1);
	                        }
	                        else {
	                            listeners.useCount[listenerIndex]++;
	                        }
	                    }
	                    else {
	                        ret = element[addEvent](type, listener, useCapture || false);
	                    }
	                    target.events[type].push(listener);

	                    return ret;
	                }
	            }

	            function remove (element, type, listener, useCapture) {
	                var i,
	                    elementIndex = indexOf(elements, element),
	                    target = targets[elementIndex],
	                    listeners,
	                    listenerIndex,
	                    wrapped = listener;

	                if (!target || !target.events) {
	                    return;
	                }

	                if (useAttachEvent) {
	                    listeners = attachedListeners[elementIndex];
	                    listenerIndex = indexOf(listeners.supplied, listener);
	                    wrapped = listeners.wrapped[listenerIndex];
	                }

	                if (type === 'all') {
	                    for (type in target.events) {
	                        if (target.events.hasOwnProperty(type)) {
	                            remove(element, type, 'all');
	                        }
	                    }
	                    return;
	                }

	                if (target.events[type]) {
	                    var len = target.events[type].length;

	                    if (listener === 'all') {
	                        for (i = 0; i < len; i++) {
	                            remove(element, type, target.events[type][i], Boolean(useCapture));
	                        }
	                        return;
	                    } else {
	                        for (i = 0; i < len; i++) {
	                            if (target.events[type][i] === listener) {
	                                element[removeEvent](on + type, wrapped, useCapture || false);
	                                target.events[type].splice(i, 1);

	                                if (useAttachEvent && listeners) {
	                                    listeners.useCount[listenerIndex]--;
	                                    if (listeners.useCount[listenerIndex] === 0) {
	                                        listeners.supplied.splice(listenerIndex, 1);
	                                        listeners.wrapped.splice(listenerIndex, 1);
	                                        listeners.useCount.splice(listenerIndex, 1);
	                                    }
	                                }

	                                break;
	                            }
	                        }
	                    }

	                    if (target.events[type] && target.events[type].length === 0) {
	                        target.events[type] = null;
	                        target.typeCount--;
	                    }
	                }

	                if (!target.typeCount) {
	                    targets.splice(elementIndex, 1);
	                    elements.splice(elementIndex, 1);
	                    attachedListeners.splice(elementIndex, 1);
	                }
	            }

	            function preventDef () {
	                this.returnValue = false;
	            }

	            function stopProp () {
	                this.cancelBubble = true;
	            }

	            function stopImmProp () {
	                this.cancelBubble = true;
	                this.immediatePropagationStopped = true;
	            }

	            return {
	                add: add,
	                remove: remove,
	                useAttachEvent: useAttachEvent,

	                _elements: elements,
	                _targets: targets,
	                _attachedListeners: attachedListeners
	            };
	        }());

	    function blank () {}

	    function isElement (o) {
	        if (!o || (typeof o !== 'object')) { return false; }

	        var _window = getWindow(o) || window;

	        return (/object|function/.test(typeof _window.Element)
	            ? o instanceof _window.Element //DOM2
	            : o.nodeType === 1 && typeof o.nodeName === "string");
	    }
	    function isWindow (thing) { return thing === window || !!(thing && thing.Window) && (thing instanceof thing.Window); }
	    function isDocFrag (thing) { return !!thing && thing instanceof DocumentFragment; }
	    function isArray (thing) {
	        return isObject(thing)
	                && (typeof thing.length !== undefined)
	                && isFunction(thing.splice);
	    }
	    function isObject   (thing) { return !!thing && (typeof thing === 'object'); }
	    function isFunction (thing) { return typeof thing === 'function'; }
	    function isNumber   (thing) { return typeof thing === 'number'  ; }
	    function isBool     (thing) { return typeof thing === 'boolean' ; }
	    function isString   (thing) { return typeof thing === 'string'  ; }

	    function trySelector (value) {
	        if (!isString(value)) { return false; }

	        // an exception will be raised if it is invalid
	        document.querySelector(value);
	        return true;
	    }

	    function extend (dest, source) {
	        for (var prop in source) {
	            dest[prop] = source[prop];
	        }
	        return dest;
	    }

	    var prefixedPropREs = {
	      webkit: /(Movement[XY]|Radius[XY]|RotationAngle|Force)$/
	    };

	    function pointerExtend (dest, source) {
	        for (var prop in source) {
	          var deprecated = false;

	          // skip deprecated prefixed properties
	          for (var vendor in prefixedPropREs) {
	            if (prop.indexOf(vendor) === 0 && prefixedPropREs[vendor].test(prop)) {
	              deprecated = true;
	              break;
	            }
	          }

	          if (!deprecated) {
	            dest[prop] = source[prop];
	          }
	        }
	        return dest;
	    }

	    function copyCoords (dest, src) {
	        dest.page = dest.page || {};
	        dest.page.x = src.page.x;
	        dest.page.y = src.page.y;

	        dest.client = dest.client || {};
	        dest.client.x = src.client.x;
	        dest.client.y = src.client.y;

	        dest.timeStamp = src.timeStamp;
	    }

	    function setEventXY (targetObj, pointers, interaction) {
	        var pointer = (pointers.length > 1
	                       ? pointerAverage(pointers)
	                       : pointers[0]);

	        getPageXY(pointer, tmpXY, interaction);
	        targetObj.page.x = tmpXY.x;
	        targetObj.page.y = tmpXY.y;

	        getClientXY(pointer, tmpXY, interaction);
	        targetObj.client.x = tmpXY.x;
	        targetObj.client.y = tmpXY.y;

	        targetObj.timeStamp = new Date().getTime();
	    }

	    function setEventDeltas (targetObj, prev, cur) {
	        targetObj.page.x     = cur.page.x      - prev.page.x;
	        targetObj.page.y     = cur.page.y      - prev.page.y;
	        targetObj.client.x   = cur.client.x    - prev.client.x;
	        targetObj.client.y   = cur.client.y    - prev.client.y;
	        targetObj.timeStamp = new Date().getTime() - prev.timeStamp;

	        // set pointer velocity
	        var dt = Math.max(targetObj.timeStamp / 1000, 0.001);
	        targetObj.page.speed   = hypot(targetObj.page.x, targetObj.page.y) / dt;
	        targetObj.page.vx      = targetObj.page.x / dt;
	        targetObj.page.vy      = targetObj.page.y / dt;

	        targetObj.client.speed = hypot(targetObj.client.x, targetObj.page.y) / dt;
	        targetObj.client.vx    = targetObj.client.x / dt;
	        targetObj.client.vy    = targetObj.client.y / dt;
	    }

	    function isNativePointer (pointer) {
	        return (pointer instanceof window.Event
	            || (supportsTouch && window.Touch && pointer instanceof window.Touch));
	    }

	    // Get specified X/Y coords for mouse or event.touches[0]
	    function getXY (type, pointer, xy) {
	        xy = xy || {};
	        type = type || 'page';

	        xy.x = pointer[type + 'X'];
	        xy.y = pointer[type + 'Y'];

	        return xy;
	    }

	    function getPageXY (pointer, page) {
	        page = page || {};

	        // Opera Mobile handles the viewport and scrolling oddly
	        if (isOperaMobile && isNativePointer(pointer)) {
	            getXY('screen', pointer, page);

	            page.x += window.scrollX;
	            page.y += window.scrollY;
	        }
	        else {
	            getXY('page', pointer, page);
	        }

	        return page;
	    }

	    function getClientXY (pointer, client) {
	        client = client || {};

	        if (isOperaMobile && isNativePointer(pointer)) {
	            // Opera Mobile handles the viewport and scrolling oddly
	            getXY('screen', pointer, client);
	        }
	        else {
	          getXY('client', pointer, client);
	        }

	        return client;
	    }

	    function getScrollXY (win) {
	        win = win || window;
	        return {
	            x: win.scrollX || win.document.documentElement.scrollLeft,
	            y: win.scrollY || win.document.documentElement.scrollTop
	        };
	    }

	    function getPointerId (pointer) {
	        return isNumber(pointer.pointerId)? pointer.pointerId : pointer.identifier;
	    }

	    function getActualElement (element) {
	        return (element instanceof SVGElementInstance
	            ? element.correspondingUseElement
	            : element);
	    }

	    function getWindow (node) {
	        if (isWindow(node)) {
	            return node;
	        }

	        var rootNode = (node.ownerDocument || node);

	        return rootNode.defaultView || rootNode.parentWindow || window;
	    }

	    function getElementClientRect (element) {
	        var clientRect = (element instanceof SVGElement
	                            ? element.getBoundingClientRect()
	                            : element.getClientRects()[0]);

	        return clientRect && {
	            left  : clientRect.left,
	            right : clientRect.right,
	            top   : clientRect.top,
	            bottom: clientRect.bottom,
	            width : clientRect.width || clientRect.right - clientRect.left,
	            height: clientRect.height || clientRect.bottom - clientRect.top
	        };
	    }

	    function getElementRect (element) {
	        var clientRect = getElementClientRect(element);

	        if (!isIOS7 && clientRect) {
	            var scroll = getScrollXY(getWindow(element));

	            clientRect.left   += scroll.x;
	            clientRect.right  += scroll.x;
	            clientRect.top    += scroll.y;
	            clientRect.bottom += scroll.y;
	        }

	        return clientRect;
	    }

	    function getTouchPair (event) {
	        var touches = [];

	        // array of touches is supplied
	        if (isArray(event)) {
	            touches[0] = event[0];
	            touches[1] = event[1];
	        }
	        // an event
	        else {
	            if (event.type === 'touchend') {
	                if (event.touches.length === 1) {
	                    touches[0] = event.touches[0];
	                    touches[1] = event.changedTouches[0];
	                }
	                else if (event.touches.length === 0) {
	                    touches[0] = event.changedTouches[0];
	                    touches[1] = event.changedTouches[1];
	                }
	            }
	            else {
	                touches[0] = event.touches[0];
	                touches[1] = event.touches[1];
	            }
	        }

	        return touches;
	    }

	    function pointerAverage (pointers) {
	        var average = {
	            pageX  : 0,
	            pageY  : 0,
	            clientX: 0,
	            clientY: 0,
	            screenX: 0,
	            screenY: 0
	        };
	        var prop;

	        for (var i = 0; i < pointers.length; i++) {
	            for (prop in average) {
	                average[prop] += pointers[i][prop];
	            }
	        }
	        for (prop in average) {
	            average[prop] /= pointers.length;
	        }

	        return average;
	    }

	    function touchBBox (event) {
	        if (!event.length && !(event.touches && event.touches.length > 1)) {
	            return;
	        }

	        var touches = getTouchPair(event),
	            minX = Math.min(touches[0].pageX, touches[1].pageX),
	            minY = Math.min(touches[0].pageY, touches[1].pageY),
	            maxX = Math.max(touches[0].pageX, touches[1].pageX),
	            maxY = Math.max(touches[0].pageY, touches[1].pageY);

	        return {
	            x: minX,
	            y: minY,
	            left: minX,
	            top: minY,
	            width: maxX - minX,
	            height: maxY - minY
	        };
	    }

	    function touchDistance (event, deltaSource) {
	        deltaSource = deltaSource || defaultOptions.deltaSource;

	        var sourceX = deltaSource + 'X',
	            sourceY = deltaSource + 'Y',
	            touches = getTouchPair(event);


	        var dx = touches[0][sourceX] - touches[1][sourceX],
	            dy = touches[0][sourceY] - touches[1][sourceY];

	        return hypot(dx, dy);
	    }

	    function touchAngle (event, prevAngle, deltaSource) {
	        deltaSource = deltaSource || defaultOptions.deltaSource;

	        var sourceX = deltaSource + 'X',
	            sourceY = deltaSource + 'Y',
	            touches = getTouchPair(event),
	            dx = touches[0][sourceX] - touches[1][sourceX],
	            dy = touches[0][sourceY] - touches[1][sourceY],
	            angle = 180 * Math.atan(dy / dx) / Math.PI;

	        if (isNumber(prevAngle)) {
	            var dr = angle - prevAngle,
	                drClamped = dr % 360;

	            if (drClamped > 315) {
	                angle -= 360 + (angle / 360)|0 * 360;
	            }
	            else if (drClamped > 135) {
	                angle -= 180 + (angle / 360)|0 * 360;
	            }
	            else if (drClamped < -315) {
	                angle += 360 + (angle / 360)|0 * 360;
	            }
	            else if (drClamped < -135) {
	                angle += 180 + (angle / 360)|0 * 360;
	            }
	        }

	        return  angle;
	    }

	    function getOriginXY (interactable, element) {
	        var origin = interactable
	                ? interactable.options.origin
	                : defaultOptions.origin;

	        if (origin === 'parent') {
	            origin = parentElement(element);
	        }
	        else if (origin === 'self') {
	            origin = interactable.getRect(element);
	        }
	        else if (trySelector(origin)) {
	            origin = closest(element, origin) || { x: 0, y: 0 };
	        }

	        if (isFunction(origin)) {
	            origin = origin(interactable && element);
	        }

	        if (isElement(origin))  {
	            origin = getElementRect(origin);
	        }

	        origin.x = ('x' in origin)? origin.x : origin.left;
	        origin.y = ('y' in origin)? origin.y : origin.top;

	        return origin;
	    }

	    // http://stackoverflow.com/a/5634528/2280888
	    function _getQBezierValue(t, p1, p2, p3) {
	        var iT = 1 - t;
	        return iT * iT * p1 + 2 * iT * t * p2 + t * t * p3;
	    }

	    function getQuadraticCurvePoint(startX, startY, cpX, cpY, endX, endY, position) {
	        return {
	            x:  _getQBezierValue(position, startX, cpX, endX),
	            y:  _getQBezierValue(position, startY, cpY, endY)
	        };
	    }

	    // http://gizma.com/easing/
	    function easeOutQuad (t, b, c, d) {
	        t /= d;
	        return -c * t*(t-2) + b;
	    }

	    function nodeContains (parent, child) {
	        while (child) {
	            if (child === parent) {
	                return true;
	            }

	            child = child.parentNode;
	        }

	        return false;
	    }

	    function closest (child, selector) {
	        var parent = parentElement(child);

	        while (isElement(parent)) {
	            if (matchesSelector(parent, selector)) { return parent; }

	            parent = parentElement(parent);
	        }

	        return null;
	    }

	    function parentElement (node) {
	        var parent = node.parentNode;

	        if (isDocFrag(parent)) {
	            // skip past #shado-root fragments
	            while ((parent = parent.host) && isDocFrag(parent)) {}

	            return parent;
	        }

	        return parent;
	    }

	    function inContext (interactable, element) {
	        return interactable._context === element.ownerDocument
	                || nodeContains(interactable._context, element);
	    }

	    function testIgnore (interactable, interactableElement, element) {
	        var ignoreFrom = interactable.options.ignoreFrom;

	        if (!ignoreFrom || !isElement(element)) { return false; }

	        if (isString(ignoreFrom)) {
	            return matchesUpTo(element, ignoreFrom, interactableElement);
	        }
	        else if (isElement(ignoreFrom)) {
	            return nodeContains(ignoreFrom, element);
	        }

	        return false;
	    }

	    function testAllow (interactable, interactableElement, element) {
	        var allowFrom = interactable.options.allowFrom;

	        if (!allowFrom) { return true; }

	        if (!isElement(element)) { return false; }

	        if (isString(allowFrom)) {
	            return matchesUpTo(element, allowFrom, interactableElement);
	        }
	        else if (isElement(allowFrom)) {
	            return nodeContains(allowFrom, element);
	        }

	        return false;
	    }

	    function checkAxis (axis, interactable) {
	        if (!interactable) { return false; }

	        var thisAxis = interactable.options.drag.axis;

	        return (axis === 'xy' || thisAxis === 'xy' || thisAxis === axis);
	    }

	    function checkSnap (interactable, action) {
	        var options = interactable.options;

	        if (/^resize/.test(action)) {
	            action = 'resize';
	        }

	        return options[action].snap && options[action].snap.enabled;
	    }

	    function checkRestrict (interactable, action) {
	        var options = interactable.options;

	        if (/^resize/.test(action)) {
	            action = 'resize';
	        }

	        return  options[action].restrict && options[action].restrict.enabled;
	    }

	    function checkAutoScroll (interactable, action) {
	        var options = interactable.options;

	        if (/^resize/.test(action)) {
	            action = 'resize';
	        }

	        return  options[action].autoScroll && options[action].autoScroll.enabled;
	    }

	    function withinInteractionLimit (interactable, element, action) {
	        var options = interactable.options,
	            maxActions = options[action.name].max,
	            maxPerElement = options[action.name].maxPerElement,
	            activeInteractions = 0,
	            targetCount = 0,
	            targetElementCount = 0;

	        for (var i = 0, len = interactions.length; i < len; i++) {
	            var interaction = interactions[i],
	                otherAction = interaction.prepared.name,
	                active = interaction.interacting();

	            if (!active) { continue; }

	            activeInteractions++;

	            if (activeInteractions >= maxInteractions) {
	                return false;
	            }

	            if (interaction.target !== interactable) { continue; }

	            targetCount += (otherAction === action.name)|0;

	            if (targetCount >= maxActions) {
	                return false;
	            }

	            if (interaction.element === element) {
	                targetElementCount++;

	                if (otherAction !== action.name || targetElementCount >= maxPerElement) {
	                    return false;
	                }
	            }
	        }

	        return maxInteractions > 0;
	    }

	    // Test for the element that's "above" all other qualifiers
	    function indexOfDeepestElement (elements) {
	        var dropzone,
	            deepestZone = elements[0],
	            index = deepestZone? 0: -1,
	            parent,
	            deepestZoneParents = [],
	            dropzoneParents = [],
	            child,
	            i,
	            n;

	        for (i = 1; i < elements.length; i++) {
	            dropzone = elements[i];

	            // an element might belong to multiple selector dropzones
	            if (!dropzone || dropzone === deepestZone) {
	                continue;
	            }

	            if (!deepestZone) {
	                deepestZone = dropzone;
	                index = i;
	                continue;
	            }

	            // check if the deepest or current are document.documentElement or document.rootElement
	            // - if the current dropzone is, do nothing and continue
	            if (dropzone.parentNode === dropzone.ownerDocument) {
	                continue;
	            }
	            // - if deepest is, update with the current dropzone and continue to next
	            else if (deepestZone.parentNode === dropzone.ownerDocument) {
	                deepestZone = dropzone;
	                index = i;
	                continue;
	            }

	            if (!deepestZoneParents.length) {
	                parent = deepestZone;
	                while (parent.parentNode && parent.parentNode !== parent.ownerDocument) {
	                    deepestZoneParents.unshift(parent);
	                    parent = parent.parentNode;
	                }
	            }

	            // if this element is an svg element and the current deepest is
	            // an HTMLElement
	            if (deepestZone instanceof HTMLElement
	                && dropzone instanceof SVGElement
	                && !(dropzone instanceof SVGSVGElement)) {

	                if (dropzone === deepestZone.parentNode) {
	                    continue;
	                }

	                parent = dropzone.ownerSVGElement;
	            }
	            else {
	                parent = dropzone;
	            }

	            dropzoneParents = [];

	            while (parent.parentNode !== parent.ownerDocument) {
	                dropzoneParents.unshift(parent);
	                parent = parent.parentNode;
	            }

	            n = 0;

	            // get (position of last common ancestor) + 1
	            while (dropzoneParents[n] && dropzoneParents[n] === deepestZoneParents[n]) {
	                n++;
	            }

	            var parents = [
	                dropzoneParents[n - 1],
	                dropzoneParents[n],
	                deepestZoneParents[n]
	            ];

	            child = parents[0].lastChild;

	            while (child) {
	                if (child === parents[1]) {
	                    deepestZone = dropzone;
	                    index = i;
	                    deepestZoneParents = [];

	                    break;
	                }
	                else if (child === parents[2]) {
	                    break;
	                }

	                child = child.previousSibling;
	            }
	        }

	        return index;
	    }

	    function Interaction () {
	        this.target          = null; // current interactable being interacted with
	        this.element         = null; // the target element of the interactable
	        this.dropTarget      = null; // the dropzone a drag target might be dropped into
	        this.dropElement     = null; // the element at the time of checking
	        this.prevDropTarget  = null; // the dropzone that was recently dragged away from
	        this.prevDropElement = null; // the element at the time of checking

	        this.prepared        = {     // action that's ready to be fired on next move event
	            name : null,
	            axis : null,
	            edges: null
	        };

	        this.matches         = [];   // all selectors that are matched by target element
	        this.matchElements   = [];   // corresponding elements

	        this.inertiaStatus = {
	            active       : false,
	            smoothEnd    : false,
	            ending       : false,

	            startEvent: null,
	            upCoords: {},

	            xe: 0, ye: 0,
	            sx: 0, sy: 0,

	            t0: 0,
	            vx0: 0, vys: 0,
	            duration: 0,

	            resumeDx: 0,
	            resumeDy: 0,

	            lambda_v0: 0,
	            one_ve_v0: 0,
	            i  : null
	        };

	        if (isFunction(Function.prototype.bind)) {
	            this.boundInertiaFrame = this.inertiaFrame.bind(this);
	            this.boundSmoothEndFrame = this.smoothEndFrame.bind(this);
	        }
	        else {
	            var that = this;

	            this.boundInertiaFrame = function () { return that.inertiaFrame(); };
	            this.boundSmoothEndFrame = function () { return that.smoothEndFrame(); };
	        }

	        this.activeDrops = {
	            dropzones: [],      // the dropzones that are mentioned below
	            elements : [],      // elements of dropzones that accept the target draggable
	            rects    : []       // the rects of the elements mentioned above
	        };

	        // keep track of added pointers
	        this.pointers    = [];
	        this.pointerIds  = [];
	        this.downTargets = [];
	        this.downTimes   = [];
	        this.holdTimers  = [];

	        // Previous native pointer move event coordinates
	        this.prevCoords = {
	            page     : { x: 0, y: 0 },
	            client   : { x: 0, y: 0 },
	            timeStamp: 0
	        };
	        // current native pointer move event coordinates
	        this.curCoords = {
	            page     : { x: 0, y: 0 },
	            client   : { x: 0, y: 0 },
	            timeStamp: 0
	        };

	        // Starting InteractEvent pointer coordinates
	        this.startCoords = {
	            page     : { x: 0, y: 0 },
	            client   : { x: 0, y: 0 },
	            timeStamp: 0
	        };

	        // Change in coordinates and time of the pointer
	        this.pointerDelta = {
	            page     : { x: 0, y: 0, vx: 0, vy: 0, speed: 0 },
	            client   : { x: 0, y: 0, vx: 0, vy: 0, speed: 0 },
	            timeStamp: 0
	        };

	        this.downEvent   = null;    // pointerdown/mousedown/touchstart event
	        this.downPointer = {};

	        this._eventTarget    = null;
	        this._curEventTarget = null;

	        this.prevEvent = null;      // previous action event
	        this.tapTime   = 0;         // time of the most recent tap event
	        this.prevTap   = null;

	        this.startOffset    = { left: 0, right: 0, top: 0, bottom: 0 };
	        this.restrictOffset = { left: 0, right: 0, top: 0, bottom: 0 };
	        this.snapOffsets    = [];

	        this.gesture = {
	            start: { x: 0, y: 0 },

	            startDistance: 0,   // distance between two touches of touchStart
	            prevDistance : 0,
	            distance     : 0,

	            scale: 1,           // gesture.distance / gesture.startDistance

	            startAngle: 0,      // angle of line joining two touches
	            prevAngle : 0       // angle of the previous gesture event
	        };

	        this.snapStatus = {
	            x       : 0, y       : 0,
	            dx      : 0, dy      : 0,
	            realX   : 0, realY   : 0,
	            snappedX: 0, snappedY: 0,
	            targets : [],
	            locked  : false,
	            changed : false
	        };

	        this.restrictStatus = {
	            dx         : 0, dy         : 0,
	            restrictedX: 0, restrictedY: 0,
	            snap       : null,
	            restricted : false,
	            changed    : false
	        };

	        this.restrictStatus.snap = this.snapStatus;

	        this.pointerIsDown   = false;
	        this.pointerWasMoved = false;
	        this.gesturing       = false;
	        this.dragging        = false;
	        this.resizing        = false;
	        this.resizeAxes      = 'xy';

	        this.mouse = false;

	        interactions.push(this);
	    }

	    Interaction.prototype = {
	        getPageXY  : function (pointer, xy) { return   getPageXY(pointer, xy, this); },
	        getClientXY: function (pointer, xy) { return getClientXY(pointer, xy, this); },
	        setEventXY : function (target, ptr) { return  setEventXY(target, ptr, this); },

	        pointerOver: function (pointer, event, eventTarget) {
	            if (this.prepared.name || !this.mouse) { return; }

	            var curMatches = [],
	                curMatchElements = [],
	                prevTargetElement = this.element;

	            this.addPointer(pointer);

	            if (this.target
	                && (testIgnore(this.target, this.element, eventTarget)
	                    || !testAllow(this.target, this.element, eventTarget))) {
	                // if the eventTarget should be ignored or shouldn't be allowed
	                // clear the previous target
	                this.target = null;
	                this.element = null;
	                this.matches = [];
	                this.matchElements = [];
	            }

	            var elementInteractable = interactables.get(eventTarget),
	                elementAction = (elementInteractable
	                                 && !testIgnore(elementInteractable, eventTarget, eventTarget)
	                                 && testAllow(elementInteractable, eventTarget, eventTarget)
	                                 && validateAction(
	                                     elementInteractable.getAction(pointer, event, this, eventTarget),
	                                     elementInteractable));

	            if (elementAction && !withinInteractionLimit(elementInteractable, eventTarget, elementAction)) {
	                 elementAction = null;
	            }

	            function pushCurMatches (interactable, selector) {
	                if (interactable
	                    && inContext(interactable, eventTarget)
	                    && !testIgnore(interactable, eventTarget, eventTarget)
	                    && testAllow(interactable, eventTarget, eventTarget)
	                    && matchesSelector(eventTarget, selector)) {

	                    curMatches.push(interactable);
	                    curMatchElements.push(eventTarget);
	                }
	            }

	            if (elementAction) {
	                this.target = elementInteractable;
	                this.element = eventTarget;
	                this.matches = [];
	                this.matchElements = [];
	            }
	            else {
	                interactables.forEachSelector(pushCurMatches);

	                if (this.validateSelector(pointer, event, curMatches, curMatchElements)) {
	                    this.matches = curMatches;
	                    this.matchElements = curMatchElements;

	                    this.pointerHover(pointer, event, this.matches, this.matchElements);
	                    events.add(eventTarget,
	                                        PointerEvent? pEventTypes.move : 'mousemove',
	                                        listeners.pointerHover);
	                }
	                else if (this.target) {
	                    if (nodeContains(prevTargetElement, eventTarget)) {
	                        this.pointerHover(pointer, event, this.matches, this.matchElements);
	                        events.add(this.element,
	                                            PointerEvent? pEventTypes.move : 'mousemove',
	                                            listeners.pointerHover);
	                    }
	                    else {
	                        this.target = null;
	                        this.element = null;
	                        this.matches = [];
	                        this.matchElements = [];
	                    }
	                }
	            }
	        },

	        // Check what action would be performed on pointerMove target if a mouse
	        // button were pressed and change the cursor accordingly
	        pointerHover: function (pointer, event, eventTarget, curEventTarget, matches, matchElements) {
	            var target = this.target;

	            if (!this.prepared.name && this.mouse) {

	                var action;

	                // update pointer coords for defaultActionChecker to use
	                this.setEventXY(this.curCoords, [pointer]);

	                if (matches) {
	                    action = this.validateSelector(pointer, event, matches, matchElements);
	                }
	                else if (target) {
	                    action = validateAction(target.getAction(this.pointers[0], event, this, this.element), this.target);
	                }

	                if (target && target.options.styleCursor) {
	                    if (action) {
	                        target._doc.documentElement.style.cursor = getActionCursor(action);
	                    }
	                    else {
	                        target._doc.documentElement.style.cursor = '';
	                    }
	                }
	            }
	            else if (this.prepared.name) {
	                this.checkAndPreventDefault(event, target, this.element);
	            }
	        },

	        pointerOut: function (pointer, event, eventTarget) {
	            if (this.prepared.name) { return; }

	            // Remove temporary event listeners for selector Interactables
	            if (!interactables.get(eventTarget)) {
	                events.remove(eventTarget,
	                                       PointerEvent? pEventTypes.move : 'mousemove',
	                                       listeners.pointerHover);
	            }

	            if (this.target && this.target.options.styleCursor && !this.interacting()) {
	                this.target._doc.documentElement.style.cursor = '';
	            }
	        },

	        selectorDown: function (pointer, event, eventTarget, curEventTarget) {
	            var that = this,
	                // copy event to be used in timeout for IE8
	                eventCopy = events.useAttachEvent? extend({}, event) : event,
	                element = eventTarget,
	                pointerIndex = this.addPointer(pointer),
	                action;

	            this.holdTimers[pointerIndex] = setTimeout(function () {
	                that.pointerHold(events.useAttachEvent? eventCopy : pointer, eventCopy, eventTarget, curEventTarget);
	            }, defaultOptions._holdDuration);

	            this.pointerIsDown = true;

	            // Check if the down event hits the current inertia target
	            if (this.inertiaStatus.active && this.target.selector) {
	                // climb up the DOM tree from the event target
	                while (isElement(element)) {

	                    // if this element is the current inertia target element
	                    if (element === this.element
	                        // and the prospective action is the same as the ongoing one
	                        && validateAction(this.target.getAction(pointer, event, this, this.element), this.target).name === this.prepared.name) {

	                        // stop inertia so that the next move will be a normal one
	                        cancelFrame(this.inertiaStatus.i);
	                        this.inertiaStatus.active = false;

	                        this.collectEventTargets(pointer, event, eventTarget, 'down');
	                        return;
	                    }
	                    element = parentElement(element);
	                }
	            }

	            // do nothing if interacting
	            if (this.interacting()) {
	                this.collectEventTargets(pointer, event, eventTarget, 'down');
	                return;
	            }

	            function pushMatches (interactable, selector, context) {
	                var elements = ie8MatchesSelector
	                    ? context.querySelectorAll(selector)
	                    : undefined;

	                if (inContext(interactable, element)
	                    && !testIgnore(interactable, element, eventTarget)
	                    && testAllow(interactable, element, eventTarget)
	                    && matchesSelector(element, selector, elements)) {

	                    that.matches.push(interactable);
	                    that.matchElements.push(element);
	                }
	            }

	            // update pointer coords for defaultActionChecker to use
	            this.setEventXY(this.curCoords, [pointer]);
	            this.downEvent = event;

	            while (isElement(element) && !action) {
	                this.matches = [];
	                this.matchElements = [];

	                interactables.forEachSelector(pushMatches);

	                action = this.validateSelector(pointer, event, this.matches, this.matchElements);
	                element = parentElement(element);
	            }

	            if (action) {
	                this.prepared.name  = action.name;
	                this.prepared.axis  = action.axis;
	                this.prepared.edges = action.edges;

	                this.collectEventTargets(pointer, event, eventTarget, 'down');

	                return this.pointerDown(pointer, event, eventTarget, curEventTarget, action);
	            }
	            else {
	                // do these now since pointerDown isn't being called from here
	                this.downTimes[pointerIndex] = new Date().getTime();
	                this.downTargets[pointerIndex] = eventTarget;
	                pointerExtend(this.downPointer, pointer);

	                copyCoords(this.prevCoords, this.curCoords);
	                this.pointerWasMoved = false;
	            }

	            this.collectEventTargets(pointer, event, eventTarget, 'down');
	        },

	        // Determine action to be performed on next pointerMove and add appropriate
	        // style and event Listeners
	        pointerDown: function (pointer, event, eventTarget, curEventTarget, forceAction) {
	            if (!forceAction && !this.inertiaStatus.active && this.pointerWasMoved && this.prepared.name) {
	                this.checkAndPreventDefault(event, this.target, this.element);

	                return;
	            }

	            this.pointerIsDown = true;
	            this.downEvent = event;

	            var pointerIndex = this.addPointer(pointer),
	                action;

	            // If it is the second touch of a multi-touch gesture, keep the
	            // target the same and get a new action if a target was set by the
	            // first touch
	            if (this.pointerIds.length > 1 && this.target._element === this.element) {
	                var newAction = validateAction(forceAction || this.target.getAction(pointer, event, this, this.element), this.target);

	                if (withinInteractionLimit(this.target, this.element, newAction)) {
	                    action = newAction;
	                }

	                this.prepared.name = null;
	            }
	            // Otherwise, set the target if there is no action prepared
	            else if (!this.prepared.name) {
	                var interactable = interactables.get(curEventTarget);

	                if (interactable
	                    && !testIgnore(interactable, curEventTarget, eventTarget)
	                    && testAllow(interactable, curEventTarget, eventTarget)
	                    && (action = validateAction(forceAction || interactable.getAction(pointer, event, this, curEventTarget), interactable, eventTarget))
	                    && withinInteractionLimit(interactable, curEventTarget, action)) {
	                    this.target = interactable;
	                    this.element = curEventTarget;
	                }
	            }

	            var target = this.target,
	                options = target && target.options;

	            if (target && (forceAction || !this.prepared.name)) {
	                action = action || validateAction(forceAction || target.getAction(pointer, event, this, curEventTarget), target, this.element);

	                this.setEventXY(this.startCoords, this.pointers);

	                if (!action) { return; }

	                if (options.styleCursor) {
	                    target._doc.documentElement.style.cursor = getActionCursor(action);
	                }

	                this.resizeAxes = action.name === 'resize'? action.axis : null;

	                if (action === 'gesture' && this.pointerIds.length < 2) {
	                    action = null;
	                }

	                this.prepared.name  = action.name;
	                this.prepared.axis  = action.axis;
	                this.prepared.edges = action.edges;

	                this.snapStatus.snappedX = this.snapStatus.snappedY =
	                    this.restrictStatus.restrictedX = this.restrictStatus.restrictedY = NaN;

	                this.downTimes[pointerIndex] = new Date().getTime();
	                this.downTargets[pointerIndex] = eventTarget;
	                pointerExtend(this.downPointer, pointer);

	                copyCoords(this.prevCoords, this.startCoords);
	                this.pointerWasMoved = false;

	                this.checkAndPreventDefault(event, target, this.element);
	            }
	            // if inertia is active try to resume action
	            else if (this.inertiaStatus.active
	                && curEventTarget === this.element
	                && validateAction(target.getAction(pointer, event, this, this.element), target).name === this.prepared.name) {

	                cancelFrame(this.inertiaStatus.i);
	                this.inertiaStatus.active = false;

	                this.checkAndPreventDefault(event, target, this.element);
	            }
	        },

	        setModifications: function (coords, preEnd) {
	            var target         = this.target,
	                shouldMove     = true,
	                shouldSnap     = checkSnap(target, this.prepared.name)     && (!target.options[this.prepared.name].snap.endOnly     || preEnd),
	                shouldRestrict = checkRestrict(target, this.prepared.name) && (!target.options[this.prepared.name].restrict.endOnly || preEnd);

	            if (shouldSnap    ) { this.setSnapping   (coords); } else { this.snapStatus    .locked     = false; }
	            if (shouldRestrict) { this.setRestriction(coords); } else { this.restrictStatus.restricted = false; }

	            if (shouldSnap && this.snapStatus.locked && !this.snapStatus.changed) {
	                shouldMove = shouldRestrict && this.restrictStatus.restricted && this.restrictStatus.changed;
	            }
	            else if (shouldRestrict && this.restrictStatus.restricted && !this.restrictStatus.changed) {
	                shouldMove = false;
	            }

	            return shouldMove;
	        },

	        setStartOffsets: function (action, interactable, element) {
	            var rect = interactable.getRect(element),
	                origin = getOriginXY(interactable, element),
	                snap = interactable.options[this.prepared.name].snap,
	                restrict = interactable.options[this.prepared.name].restrict,
	                width, height;

	            if (rect) {
	                this.startOffset.left = this.startCoords.page.x - rect.left;
	                this.startOffset.top  = this.startCoords.page.y - rect.top;

	                this.startOffset.right  = rect.right  - this.startCoords.page.x;
	                this.startOffset.bottom = rect.bottom - this.startCoords.page.y;

	                if ('width' in rect) { width = rect.width; }
	                else { width = rect.right - rect.left; }
	                if ('height' in rect) { height = rect.height; }
	                else { height = rect.bottom - rect.top; }
	            }
	            else {
	                this.startOffset.left = this.startOffset.top = this.startOffset.right = this.startOffset.bottom = 0;
	            }

	            this.snapOffsets.splice(0);

	            var snapOffset = snap && snap.offset === 'startCoords'
	                                ? {
	                                    x: this.startCoords.page.x - origin.x,
	                                    y: this.startCoords.page.y - origin.y
	                                }
	                                : snap && snap.offset || { x: 0, y: 0 };

	            if (rect && snap && snap.relativePoints && snap.relativePoints.length) {
	                for (var i = 0; i < snap.relativePoints.length; i++) {
	                    this.snapOffsets.push({
	                        x: this.startOffset.left - (width  * snap.relativePoints[i].x) + snapOffset.x,
	                        y: this.startOffset.top  - (height * snap.relativePoints[i].y) + snapOffset.y
	                    });
	                }
	            }
	            else {
	                this.snapOffsets.push(snapOffset);
	            }

	            if (rect && restrict.elementRect) {
	                this.restrictOffset.left = this.startOffset.left - (width  * restrict.elementRect.left);
	                this.restrictOffset.top  = this.startOffset.top  - (height * restrict.elementRect.top);

	                this.restrictOffset.right  = this.startOffset.right  - (width  * (1 - restrict.elementRect.right));
	                this.restrictOffset.bottom = this.startOffset.bottom - (height * (1 - restrict.elementRect.bottom));
	            }
	            else {
	                this.restrictOffset.left = this.restrictOffset.top = this.restrictOffset.right = this.restrictOffset.bottom = 0;
	            }
	        },

	        /*\
	         * Interaction.start
	         [ method ]
	         *
	         * Start an action with the given Interactable and Element as tartgets. The
	         * action must be enabled for the target Interactable and an appropriate number
	         * of pointers must be held down – 1 for drag/resize, 2 for gesture.
	         *
	         * Use it with `interactable.<action>able({ manualStart: false })` to always
	         * [start actions manually](https://github.com/taye/interact.js/issues/114)
	         *
	         - action       (object)  The action to be performed - drag, resize, etc.
	         - interactable (Interactable) The Interactable to target
	         - element      (Element) The DOM Element to target
	         = (object) interact
	         **
	         | interact(target)
	         |   .draggable({
	         |     // disable the default drag start by down->move
	         |     manualStart: true
	         |   })
	         |   // start dragging after the user holds the pointer down
	         |   .on('hold', function (event) {
	         |     var interaction = event.interaction;
	         |
	         |     if (!interaction.interacting()) {
	         |       interaction.start({ name: 'drag' },
	         |                         event.interactable,
	         |                         event.currentTarget);
	         |     }
	         | });
	        \*/
	        start: function (action, interactable, element) {
	            if (this.interacting()
	                || !this.pointerIsDown
	                || this.pointerIds.length < (action.name === 'gesture'? 2 : 1)) {
	                return;
	            }

	            // if this interaction had been removed after stopping
	            // add it back
	            if (indexOf(interactions, this) === -1) {
	                interactions.push(this);
	            }

	            // set the startCoords if there was no prepared action
	            if (!this.prepared.name) {
	                this.setEventXY(this.startCoords);
	            }

	            this.prepared.name  = action.name;
	            this.prepared.axis  = action.axis;
	            this.prepared.edges = action.edges;
	            this.target         = interactable;
	            this.element        = element;

	            this.setStartOffsets(action.name, interactable, element);
	            this.setModifications(this.startCoords.page);

	            this.prevEvent = this[this.prepared.name + 'Start'](this.downEvent);
	        },

	        pointerMove: function (pointer, event, eventTarget, curEventTarget, preEnd) {
	            if (this.inertiaStatus.active) {
	                var pageUp   = this.inertiaStatus.upCoords.page;
	                var clientUp = this.inertiaStatus.upCoords.client;

	                var inertiaPosition = {
	                    pageX  : pageUp.x   + this.inertiaStatus.sx,
	                    pageY  : pageUp.y   + this.inertiaStatus.sy,
	                    clientX: clientUp.x + this.inertiaStatus.sx,
	                    clientY: clientUp.y + this.inertiaStatus.sy
	                };

	                this.setEventXY(this.curCoords, [inertiaPosition]);
	            }
	            else {
	                this.recordPointer(pointer);
	                this.setEventXY(this.curCoords, this.pointers);
	            }

	            var duplicateMove = (this.curCoords.page.x === this.prevCoords.page.x
	                                 && this.curCoords.page.y === this.prevCoords.page.y
	                                 && this.curCoords.client.x === this.prevCoords.client.x
	                                 && this.curCoords.client.y === this.prevCoords.client.y);

	            var dx, dy,
	                pointerIndex = this.mouse? 0 : indexOf(this.pointerIds, getPointerId(pointer));

	            // register movement greater than pointerMoveTolerance
	            if (this.pointerIsDown && !this.pointerWasMoved) {
	                dx = this.curCoords.client.x - this.startCoords.client.x;
	                dy = this.curCoords.client.y - this.startCoords.client.y;

	                this.pointerWasMoved = hypot(dx, dy) > pointerMoveTolerance;
	            }

	            if (!duplicateMove && (!this.pointerIsDown || this.pointerWasMoved)) {
	                if (this.pointerIsDown) {
	                    clearTimeout(this.holdTimers[pointerIndex]);
	                }

	                this.collectEventTargets(pointer, event, eventTarget, 'move');
	            }

	            if (!this.pointerIsDown) { return; }

	            if (duplicateMove && this.pointerWasMoved && !preEnd) {
	                this.checkAndPreventDefault(event, this.target, this.element);
	                return;
	            }

	            // set pointer coordinate, time changes and speeds
	            setEventDeltas(this.pointerDelta, this.prevCoords, this.curCoords);

	            if (!this.prepared.name) { return; }

	            if (this.pointerWasMoved
	                // ignore movement while inertia is active
	                && (!this.inertiaStatus.active || (pointer instanceof InteractEvent && /inertiastart/.test(pointer.type)))) {

	                // if just starting an action, calculate the pointer speed now
	                if (!this.interacting()) {
	                    setEventDeltas(this.pointerDelta, this.prevCoords, this.curCoords);

	                    // check if a drag is in the correct axis
	                    if (this.prepared.name === 'drag') {
	                        var absX = Math.abs(dx),
	                            absY = Math.abs(dy),
	                            targetAxis = this.target.options.drag.axis,
	                            axis = (absX > absY ? 'x' : absX < absY ? 'y' : 'xy');

	                        // if the movement isn't in the axis of the interactable
	                        if (axis !== 'xy' && targetAxis !== 'xy' && targetAxis !== axis) {
	                            // cancel the prepared action
	                            this.prepared.name = null;

	                            // then try to get a drag from another ineractable

	                            var element = eventTarget;

	                            // check element interactables
	                            while (isElement(element)) {
	                                var elementInteractable = interactables.get(element);

	                                if (elementInteractable
	                                    && elementInteractable !== this.target
	                                    && !elementInteractable.options.drag.manualStart
	                                    && elementInteractable.getAction(this.downPointer, this.downEvent, this, element).name === 'drag'
	                                    && checkAxis(axis, elementInteractable)) {

	                                    this.prepared.name = 'drag';
	                                    this.target = elementInteractable;
	                                    this.element = element;
	                                    break;
	                                }

	                                element = parentElement(element);
	                            }

	                            // if there's no drag from element interactables,
	                            // check the selector interactables
	                            if (!this.prepared.name) {
	                                var thisInteraction = this;

	                                var getDraggable = function (interactable, selector, context) {
	                                    var elements = ie8MatchesSelector
	                                        ? context.querySelectorAll(selector)
	                                        : undefined;

	                                    if (interactable === thisInteraction.target) { return; }

	                                    if (inContext(interactable, eventTarget)
	                                        && !interactable.options.drag.manualStart
	                                        && !testIgnore(interactable, element, eventTarget)
	                                        && testAllow(interactable, element, eventTarget)
	                                        && matchesSelector(element, selector, elements)
	                                        && interactable.getAction(thisInteraction.downPointer, thisInteraction.downEvent, thisInteraction, element).name === 'drag'
	                                        && checkAxis(axis, interactable)
	                                        && withinInteractionLimit(interactable, element, 'drag')) {

	                                        return interactable;
	                                    }
	                                };

	                                element = eventTarget;

	                                while (isElement(element)) {
	                                    var selectorInteractable = interactables.forEachSelector(getDraggable);

	                                    if (selectorInteractable) {
	                                        this.prepared.name = 'drag';
	                                        this.target = selectorInteractable;
	                                        this.element = element;
	                                        break;
	                                    }

	                                    element = parentElement(element);
	                                }
	                            }
	                        }
	                    }
	                }

	                var starting = !!this.prepared.name && !this.interacting();

	                if (starting
	                    && (this.target.options[this.prepared.name].manualStart
	                        || !withinInteractionLimit(this.target, this.element, this.prepared))) {
	                    this.stop(event);
	                    return;
	                }

	                if (this.prepared.name && this.target) {
	                    if (starting) {
	                        this.start(this.prepared, this.target, this.element);
	                    }

	                    var shouldMove = this.setModifications(this.curCoords.page, preEnd);

	                    // move if snapping or restriction doesn't prevent it
	                    if (shouldMove || starting) {
	                        this.prevEvent = this[this.prepared.name + 'Move'](event);
	                    }

	                    this.checkAndPreventDefault(event, this.target, this.element);
	                }
	            }

	            copyCoords(this.prevCoords, this.curCoords);

	            if (this.dragging || this.resizing) {
	                this.autoScrollMove(pointer);
	            }
	        },

	        dragStart: function (event) {
	            var dragEvent = new InteractEvent(this, event, 'drag', 'start', this.element);

	            this.dragging = true;
	            this.target.fire(dragEvent);

	            // reset active dropzones
	            this.activeDrops.dropzones = [];
	            this.activeDrops.elements  = [];
	            this.activeDrops.rects     = [];

	            if (!this.dynamicDrop) {
	                this.setActiveDrops(this.element);
	            }

	            var dropEvents = this.getDropEvents(event, dragEvent);

	            if (dropEvents.activate) {
	                this.fireActiveDrops(dropEvents.activate);
	            }

	            return dragEvent;
	        },

	        dragMove: function (event) {
	            var target = this.target,
	                dragEvent  = new InteractEvent(this, event, 'drag', 'move', this.element),
	                draggableElement = this.element,
	                drop = this.getDrop(dragEvent, event, draggableElement);

	            this.dropTarget = drop.dropzone;
	            this.dropElement = drop.element;

	            var dropEvents = this.getDropEvents(event, dragEvent);

	            target.fire(dragEvent);

	            if (dropEvents.leave) { this.prevDropTarget.fire(dropEvents.leave); }
	            if (dropEvents.enter) {     this.dropTarget.fire(dropEvents.enter); }
	            if (dropEvents.move ) {     this.dropTarget.fire(dropEvents.move ); }

	            this.prevDropTarget  = this.dropTarget;
	            this.prevDropElement = this.dropElement;

	            return dragEvent;
	        },

	        resizeStart: function (event) {
	            var resizeEvent = new InteractEvent(this, event, 'resize', 'start', this.element);

	            if (this.prepared.edges) {
	                var startRect = this.target.getRect(this.element);

	                /*
	                 * When using the `resizable.square` or `resizable.preserveAspectRatio` options, resizing from one edge
	                 * will affect another. E.g. with `resizable.square`, resizing to make the right edge larger will make
	                 * the bottom edge larger by the same amount. We call these 'linked' edges. Any linked edges will depend
	                 * on the active edges and the edge being interacted with.
	                 */
	                if (this.target.options.resize.square || this.target.options.resize.preserveAspectRatio) {
	                    var linkedEdges = extend({}, this.prepared.edges);

	                    linkedEdges.top    = linkedEdges.top    || (linkedEdges.left   && !linkedEdges.bottom);
	                    linkedEdges.left   = linkedEdges.left   || (linkedEdges.top    && !linkedEdges.right );
	                    linkedEdges.bottom = linkedEdges.bottom || (linkedEdges.right  && !linkedEdges.top   );
	                    linkedEdges.right  = linkedEdges.right  || (linkedEdges.bottom && !linkedEdges.left  );

	                    this.prepared._linkedEdges = linkedEdges;
	                }
	                else {
	                    this.prepared._linkedEdges = null;
	                }

	                // if using `resizable.preserveAspectRatio` option, record aspect ratio at the start of the resize
	                if (this.target.options.resize.preserveAspectRatio) {
	                    this.resizeStartAspectRatio = startRect.width / startRect.height;
	                }

	                this.resizeRects = {
	                    start     : startRect,
	                    current   : extend({}, startRect),
	                    restricted: extend({}, startRect),
	                    previous  : extend({}, startRect),
	                    delta     : {
	                        left: 0, right : 0, width : 0,
	                        top : 0, bottom: 0, height: 0
	                    }
	                };

	                resizeEvent.rect = this.resizeRects.restricted;
	                resizeEvent.deltaRect = this.resizeRects.delta;
	            }

	            this.target.fire(resizeEvent);

	            this.resizing = true;

	            return resizeEvent;
	        },

	        resizeMove: function (event) {
	            var resizeEvent = new InteractEvent(this, event, 'resize', 'move', this.element);

	            var edges = this.prepared.edges,
	                invert = this.target.options.resize.invert,
	                invertible = invert === 'reposition' || invert === 'negate';

	            if (edges) {
	                var dx = resizeEvent.dx,
	                    dy = resizeEvent.dy,

	                    start      = this.resizeRects.start,
	                    current    = this.resizeRects.current,
	                    restricted = this.resizeRects.restricted,
	                    delta      = this.resizeRects.delta,
	                    previous   = extend(this.resizeRects.previous, restricted),

	                    originalEdges = edges;

	                // `resize.preserveAspectRatio` takes precedence over `resize.square`
	                if (this.target.options.resize.preserveAspectRatio) {
	                    var resizeStartAspectRatio = this.resizeStartAspectRatio;

	                    edges = this.prepared._linkedEdges;

	                    if ((originalEdges.left && originalEdges.bottom)
	                        || (originalEdges.right && originalEdges.top)) {
	                        dy = -dx / resizeStartAspectRatio;
	                    }
	                    else if (originalEdges.left || originalEdges.right) { dy = dx / resizeStartAspectRatio; }
	                    else if (originalEdges.top || originalEdges.bottom) { dx = dy * resizeStartAspectRatio; }
	                }
	                else if (this.target.options.resize.square) {
	                    edges = this.prepared._linkedEdges;

	                    if ((originalEdges.left && originalEdges.bottom)
	                        || (originalEdges.right && originalEdges.top)) {
	                        dy = -dx;
	                    }
	                    else if (originalEdges.left || originalEdges.right) { dy = dx; }
	                    else if (originalEdges.top || originalEdges.bottom) { dx = dy; }
	                }

	                // update the 'current' rect without modifications
	                if (edges.top   ) { current.top    += dy; }
	                if (edges.bottom) { current.bottom += dy; }
	                if (edges.left  ) { current.left   += dx; }
	                if (edges.right ) { current.right  += dx; }

	                if (invertible) {
	                    // if invertible, copy the current rect
	                    extend(restricted, current);

	                    if (invert === 'reposition') {
	                        // swap edge values if necessary to keep width/height positive
	                        var swap;

	                        if (restricted.top > restricted.bottom) {
	                            swap = restricted.top;

	                            restricted.top = restricted.bottom;
	                            restricted.bottom = swap;
	                        }
	                        if (restricted.left > restricted.right) {
	                            swap = restricted.left;

	                            restricted.left = restricted.right;
	                            restricted.right = swap;
	                        }
	                    }
	                }
	                else {
	                    // if not invertible, restrict to minimum of 0x0 rect
	                    restricted.top    = Math.min(current.top, start.bottom);
	                    restricted.bottom = Math.max(current.bottom, start.top);
	                    restricted.left   = Math.min(current.left, start.right);
	                    restricted.right  = Math.max(current.right, start.left);
	                }

	                restricted.width  = restricted.right  - restricted.left;
	                restricted.height = restricted.bottom - restricted.top ;

	                for (var edge in restricted) {
	                    delta[edge] = restricted[edge] - previous[edge];
	                }

	                resizeEvent.edges = this.prepared.edges;
	                resizeEvent.rect = restricted;
	                resizeEvent.deltaRect = delta;
	            }

	            this.target.fire(resizeEvent);

	            return resizeEvent;
	        },

	        gestureStart: function (event) {
	            var gestureEvent = new InteractEvent(this, event, 'gesture', 'start', this.element);

	            gestureEvent.ds = 0;

	            this.gesture.startDistance = this.gesture.prevDistance = gestureEvent.distance;
	            this.gesture.startAngle = this.gesture.prevAngle = gestureEvent.angle;
	            this.gesture.scale = 1;

	            this.gesturing = true;

	            this.target.fire(gestureEvent);

	            return gestureEvent;
	        },

	        gestureMove: function (event) {
	            if (!this.pointerIds.length) {
	                return this.prevEvent;
	            }

	            var gestureEvent;

	            gestureEvent = new InteractEvent(this, event, 'gesture', 'move', this.element);
	            gestureEvent.ds = gestureEvent.scale - this.gesture.scale;

	            this.target.fire(gestureEvent);

	            this.gesture.prevAngle = gestureEvent.angle;
	            this.gesture.prevDistance = gestureEvent.distance;

	            if (gestureEvent.scale !== Infinity &&
	                gestureEvent.scale !== null &&
	                gestureEvent.scale !== undefined  &&
	                !isNaN(gestureEvent.scale)) {

	                this.gesture.scale = gestureEvent.scale;
	            }

	            return gestureEvent;
	        },

	        pointerHold: function (pointer, event, eventTarget) {
	            this.collectEventTargets(pointer, event, eventTarget, 'hold');
	        },

	        pointerUp: function (pointer, event, eventTarget, curEventTarget) {
	            var pointerIndex = this.mouse? 0 : indexOf(this.pointerIds, getPointerId(pointer));

	            clearTimeout(this.holdTimers[pointerIndex]);

	            this.collectEventTargets(pointer, event, eventTarget, 'up' );
	            this.collectEventTargets(pointer, event, eventTarget, 'tap');

	            this.pointerEnd(pointer, event, eventTarget, curEventTarget);

	            this.removePointer(pointer);
	        },

	        pointerCancel: function (pointer, event, eventTarget, curEventTarget) {
	            var pointerIndex = this.mouse? 0 : indexOf(this.pointerIds, getPointerId(pointer));

	            clearTimeout(this.holdTimers[pointerIndex]);

	            this.collectEventTargets(pointer, event, eventTarget, 'cancel');
	            this.pointerEnd(pointer, event, eventTarget, curEventTarget);

	            this.removePointer(pointer);
	        },

	        // http://www.quirksmode.org/dom/events/click.html
	        // >Events leading to dblclick
	        //
	        // IE8 doesn't fire down event before dblclick.
	        // This workaround tries to fire a tap and doubletap after dblclick
	        ie8Dblclick: function (pointer, event, eventTarget) {
	            if (this.prevTap
	                && event.clientX === this.prevTap.clientX
	                && event.clientY === this.prevTap.clientY
	                && eventTarget   === this.prevTap.target) {

	                this.downTargets[0] = eventTarget;
	                this.downTimes[0] = new Date().getTime();
	                this.collectEventTargets(pointer, event, eventTarget, 'tap');
	            }
	        },

	        // End interact move events and stop auto-scroll unless inertia is enabled
	        pointerEnd: function (pointer, event, eventTarget, curEventTarget) {
	            var endEvent,
	                target = this.target,
	                options = target && target.options,
	                inertiaOptions = options && this.prepared.name && options[this.prepared.name].inertia,
	                inertiaStatus = this.inertiaStatus;

	            if (this.interacting()) {

	                if (inertiaStatus.active && !inertiaStatus.ending) { return; }

	                var pointerSpeed,
	                    now = new Date().getTime(),
	                    inertiaPossible = false,
	                    inertia = false,
	                    smoothEnd = false,
	                    endSnap = checkSnap(target, this.prepared.name) && options[this.prepared.name].snap.endOnly,
	                    endRestrict = checkRestrict(target, this.prepared.name) && options[this.prepared.name].restrict.endOnly,
	                    dx = 0,
	                    dy = 0,
	                    startEvent;

	                if (this.dragging) {
	                    if      (options.drag.axis === 'x' ) { pointerSpeed = Math.abs(this.pointerDelta.client.vx); }
	                    else if (options.drag.axis === 'y' ) { pointerSpeed = Math.abs(this.pointerDelta.client.vy); }
	                    else   /*options.drag.axis === 'xy'*/{ pointerSpeed = this.pointerDelta.client.speed; }
	                }
	                else {
	                    pointerSpeed = this.pointerDelta.client.speed;
	                }

	                // check if inertia should be started
	                inertiaPossible = (inertiaOptions && inertiaOptions.enabled
	                                   && this.prepared.name !== 'gesture'
	                                   && event !== inertiaStatus.startEvent);

	                inertia = (inertiaPossible
	                           && (now - this.curCoords.timeStamp) < 50
	                           && pointerSpeed > inertiaOptions.minSpeed
	                           && pointerSpeed > inertiaOptions.endSpeed);

	                if (inertiaPossible && !inertia && (endSnap || endRestrict)) {

	                    var snapRestrict = {};

	                    snapRestrict.snap = snapRestrict.restrict = snapRestrict;

	                    if (endSnap) {
	                        this.setSnapping(this.curCoords.page, snapRestrict);
	                        if (snapRestrict.locked) {
	                            dx += snapRestrict.dx;
	                            dy += snapRestrict.dy;
	                        }
	                    }

	                    if (endRestrict) {
	                        this.setRestriction(this.curCoords.page, snapRestrict);
	                        if (snapRestrict.restricted) {
	                            dx += snapRestrict.dx;
	                            dy += snapRestrict.dy;
	                        }
	                    }

	                    if (dx || dy) {
	                        smoothEnd = true;
	                    }
	                }

	                if (inertia || smoothEnd) {
	                    copyCoords(inertiaStatus.upCoords, this.curCoords);

	                    this.pointers[0] = inertiaStatus.startEvent = startEvent =
	                        new InteractEvent(this, event, this.prepared.name, 'inertiastart', this.element);

	                    inertiaStatus.t0 = now;

	                    target.fire(inertiaStatus.startEvent);

	                    if (inertia) {
	                        inertiaStatus.vx0 = this.pointerDelta.client.vx;
	                        inertiaStatus.vy0 = this.pointerDelta.client.vy;
	                        inertiaStatus.v0 = pointerSpeed;

	                        this.calcInertia(inertiaStatus);

	                        var page = extend({}, this.curCoords.page),
	                            origin = getOriginXY(target, this.element),
	                            statusObject;

	                        page.x = page.x + inertiaStatus.xe - origin.x;
	                        page.y = page.y + inertiaStatus.ye - origin.y;

	                        statusObject = {
	                            useStatusXY: true,
	                            x: page.x,
	                            y: page.y,
	                            dx: 0,
	                            dy: 0,
	                            snap: null
	                        };

	                        statusObject.snap = statusObject;

	                        dx = dy = 0;

	                        if (endSnap) {
	                            var snap = this.setSnapping(this.curCoords.page, statusObject);

	                            if (snap.locked) {
	                                dx += snap.dx;
	                                dy += snap.dy;
	                            }
	                        }

	                        if (endRestrict) {
	                            var restrict = this.setRestriction(this.curCoords.page, statusObject);

	                            if (restrict.restricted) {
	                                dx += restrict.dx;
	                                dy += restrict.dy;
	                            }
	                        }

	                        inertiaStatus.modifiedXe += dx;
	                        inertiaStatus.modifiedYe += dy;

	                        inertiaStatus.i = reqFrame(this.boundInertiaFrame);
	                    }
	                    else {
	                        inertiaStatus.smoothEnd = true;
	                        inertiaStatus.xe = dx;
	                        inertiaStatus.ye = dy;

	                        inertiaStatus.sx = inertiaStatus.sy = 0;

	                        inertiaStatus.i = reqFrame(this.boundSmoothEndFrame);
	                    }

	                    inertiaStatus.active = true;
	                    return;
	                }

	                if (endSnap || endRestrict) {
	                    // fire a move event at the snapped coordinates
	                    this.pointerMove(pointer, event, eventTarget, curEventTarget, true);
	                }
	            }

	            if (this.dragging) {
	                endEvent = new InteractEvent(this, event, 'drag', 'end', this.element);

	                var draggableElement = this.element,
	                    drop = this.getDrop(endEvent, event, draggableElement);

	                this.dropTarget = drop.dropzone;
	                this.dropElement = drop.element;

	                var dropEvents = this.getDropEvents(event, endEvent);

	                if (dropEvents.leave) { this.prevDropTarget.fire(dropEvents.leave); }
	                if (dropEvents.enter) {     this.dropTarget.fire(dropEvents.enter); }
	                if (dropEvents.drop ) {     this.dropTarget.fire(dropEvents.drop ); }
	                if (dropEvents.deactivate) {
	                    this.fireActiveDrops(dropEvents.deactivate);
	                }

	                target.fire(endEvent);
	            }
	            else if (this.resizing) {
	                endEvent = new InteractEvent(this, event, 'resize', 'end', this.element);
	                target.fire(endEvent);
	            }
	            else if (this.gesturing) {
	                endEvent = new InteractEvent(this, event, 'gesture', 'end', this.element);
	                target.fire(endEvent);
	            }

	            this.stop(event);
	        },

	        collectDrops: function (element) {
	            var drops = [],
	                elements = [],
	                i;

	            element = element || this.element;

	            // collect all dropzones and their elements which qualify for a drop
	            for (i = 0; i < interactables.length; i++) {
	                if (!interactables[i].options.drop.enabled) { continue; }

	                var current = interactables[i],
	                    accept = current.options.drop.accept;

	                // test the draggable element against the dropzone's accept setting
	                if ((isElement(accept) && accept !== element)
	                    || (isString(accept)
	                        && !matchesSelector(element, accept))) {

	                    continue;
	                }

	                // query for new elements if necessary
	                var dropElements = current.selector? current._context.querySelectorAll(current.selector) : [current._element];

	                for (var j = 0, len = dropElements.length; j < len; j++) {
	                    var currentElement = dropElements[j];

	                    if (currentElement === element) {
	                        continue;
	                    }

	                    drops.push(current);
	                    elements.push(currentElement);
	                }
	            }

	            return {
	                dropzones: drops,
	                elements: elements
	            };
	        },

	        fireActiveDrops: function (event) {
	            var i,
	                current,
	                currentElement,
	                prevElement;

	            // loop through all active dropzones and trigger event
	            for (i = 0; i < this.activeDrops.dropzones.length; i++) {
	                current = this.activeDrops.dropzones[i];
	                currentElement = this.activeDrops.elements [i];

	                // prevent trigger of duplicate events on same element
	                if (currentElement !== prevElement) {
	                    // set current element as event target
	                    event.target = currentElement;
	                    current.fire(event);
	                }
	                prevElement = currentElement;
	            }
	        },

	        // Collect a new set of possible drops and save them in activeDrops.
	        // setActiveDrops should always be called when a drag has just started or a
	        // drag event happens while dynamicDrop is true
	        setActiveDrops: function (dragElement) {
	            // get dropzones and their elements that could receive the draggable
	            var possibleDrops = this.collectDrops(dragElement, true);

	            this.activeDrops.dropzones = possibleDrops.dropzones;
	            this.activeDrops.elements  = possibleDrops.elements;
	            this.activeDrops.rects     = [];

	            for (var i = 0; i < this.activeDrops.dropzones.length; i++) {
	                this.activeDrops.rects[i] = this.activeDrops.dropzones[i].getRect(this.activeDrops.elements[i]);
	            }
	        },

	        getDrop: function (dragEvent, event, dragElement) {
	            var validDrops = [];

	            if (dynamicDrop) {
	                this.setActiveDrops(dragElement);
	            }

	            // collect all dropzones and their elements which qualify for a drop
	            for (var j = 0; j < this.activeDrops.dropzones.length; j++) {
	                var current        = this.activeDrops.dropzones[j],
	                    currentElement = this.activeDrops.elements [j],
	                    rect           = this.activeDrops.rects    [j];

	                validDrops.push(current.dropCheck(dragEvent, event, this.target, dragElement, currentElement, rect)
	                                ? currentElement
	                                : null);
	            }

	            // get the most appropriate dropzone based on DOM depth and order
	            var dropIndex = indexOfDeepestElement(validDrops),
	                dropzone  = this.activeDrops.dropzones[dropIndex] || null,
	                element   = this.activeDrops.elements [dropIndex] || null;

	            return {
	                dropzone: dropzone,
	                element: element
	            };
	        },

	        getDropEvents: function (pointerEvent, dragEvent) {
	            var dropEvents = {
	                enter     : null,
	                leave     : null,
	                activate  : null,
	                deactivate: null,
	                move      : null,
	                drop      : null
	            };

	            if (this.dropElement !== this.prevDropElement) {
	                // if there was a prevDropTarget, create a dragleave event
	                if (this.prevDropTarget) {
	                    dropEvents.leave = {
	                        target       : this.prevDropElement,
	                        dropzone     : this.prevDropTarget,
	                        relatedTarget: dragEvent.target,
	                        draggable    : dragEvent.interactable,
	                        dragEvent    : dragEvent,
	                        interaction  : this,
	                        timeStamp    : dragEvent.timeStamp,
	                        type         : 'dragleave'
	                    };

	                    dragEvent.dragLeave = this.prevDropElement;
	                    dragEvent.prevDropzone = this.prevDropTarget;
	                }
	                // if the dropTarget is not null, create a dragenter event
	                if (this.dropTarget) {
	                    dropEvents.enter = {
	                        target       : this.dropElement,
	                        dropzone     : this.dropTarget,
	                        relatedTarget: dragEvent.target,
	                        draggable    : dragEvent.interactable,
	                        dragEvent    : dragEvent,
	                        interaction  : this,
	                        timeStamp    : dragEvent.timeStamp,
	                        type         : 'dragenter'
	                    };

	                    dragEvent.dragEnter = this.dropElement;
	                    dragEvent.dropzone = this.dropTarget;
	                }
	            }

	            if (dragEvent.type === 'dragend' && this.dropTarget) {
	                dropEvents.drop = {
	                    target       : this.dropElement,
	                    dropzone     : this.dropTarget,
	                    relatedTarget: dragEvent.target,
	                    draggable    : dragEvent.interactable,
	                    dragEvent    : dragEvent,
	                    interaction  : this,
	                    timeStamp    : dragEvent.timeStamp,
	                    type         : 'drop'
	                };

	                dragEvent.dropzone = this.dropTarget;
	            }
	            if (dragEvent.type === 'dragstart') {
	                dropEvents.activate = {
	                    target       : null,
	                    dropzone     : null,
	                    relatedTarget: dragEvent.target,
	                    draggable    : dragEvent.interactable,
	                    dragEvent    : dragEvent,
	                    interaction  : this,
	                    timeStamp    : dragEvent.timeStamp,
	                    type         : 'dropactivate'
	                };
	            }
	            if (dragEvent.type === 'dragend') {
	                dropEvents.deactivate = {
	                    target       : null,
	                    dropzone     : null,
	                    relatedTarget: dragEvent.target,
	                    draggable    : dragEvent.interactable,
	                    dragEvent    : dragEvent,
	                    interaction  : this,
	                    timeStamp    : dragEvent.timeStamp,
	                    type         : 'dropdeactivate'
	                };
	            }
	            if (dragEvent.type === 'dragmove' && this.dropTarget) {
	                dropEvents.move = {
	                    target       : this.dropElement,
	                    dropzone     : this.dropTarget,
	                    relatedTarget: dragEvent.target,
	                    draggable    : dragEvent.interactable,
	                    dragEvent    : dragEvent,
	                    interaction  : this,
	                    dragmove     : dragEvent,
	                    timeStamp    : dragEvent.timeStamp,
	                    type         : 'dropmove'
	                };
	                dragEvent.dropzone = this.dropTarget;
	            }

	            return dropEvents;
	        },

	        currentAction: function () {
	            return (this.dragging && 'drag') || (this.resizing && 'resize') || (this.gesturing && 'gesture') || null;
	        },

	        interacting: function () {
	            return this.dragging || this.resizing || this.gesturing;
	        },

	        clearTargets: function () {
	            this.target = this.element = null;

	            this.dropTarget = this.dropElement = this.prevDropTarget = this.prevDropElement = null;
	        },

	        stop: function (event) {
	            if (this.interacting()) {
	                autoScroll.stop();
	                this.matches = [];
	                this.matchElements = [];

	                var target = this.target;

	                if (target.options.styleCursor) {
	                    target._doc.documentElement.style.cursor = '';
	                }

	                // prevent Default only if were previously interacting
	                if (event && isFunction(event.preventDefault)) {
	                    this.checkAndPreventDefault(event, target, this.element);
	                }

	                if (this.dragging) {
	                    this.activeDrops.dropzones = this.activeDrops.elements = this.activeDrops.rects = null;
	                }
	            }

	            this.clearTargets();

	            this.pointerIsDown = this.snapStatus.locked = this.dragging = this.resizing = this.gesturing = false;
	            this.prepared.name = this.prevEvent = null;
	            this.inertiaStatus.resumeDx = this.inertiaStatus.resumeDy = 0;

	            // remove pointers if their ID isn't in this.pointerIds
	            for (var i = 0; i < this.pointers.length; i++) {
	                if (indexOf(this.pointerIds, getPointerId(this.pointers[i])) === -1) {
	                    this.pointers.splice(i, 1);
	                }
	            }
	        },

	        inertiaFrame: function () {
	            var inertiaStatus = this.inertiaStatus,
	                options = this.target.options[this.prepared.name].inertia,
	                lambda = options.resistance,
	                t = new Date().getTime() / 1000 - inertiaStatus.t0;

	            if (t < inertiaStatus.te) {

	                var progress =  1 - (Math.exp(-lambda * t) - inertiaStatus.lambda_v0) / inertiaStatus.one_ve_v0;

	                if (inertiaStatus.modifiedXe === inertiaStatus.xe && inertiaStatus.modifiedYe === inertiaStatus.ye) {
	                    inertiaStatus.sx = inertiaStatus.xe * progress;
	                    inertiaStatus.sy = inertiaStatus.ye * progress;
	                }
	                else {
	                    var quadPoint = getQuadraticCurvePoint(
	                            0, 0,
	                            inertiaStatus.xe, inertiaStatus.ye,
	                            inertiaStatus.modifiedXe, inertiaStatus.modifiedYe,
	                            progress);

	                    inertiaStatus.sx = quadPoint.x;
	                    inertiaStatus.sy = quadPoint.y;
	                }

	                this.pointerMove(inertiaStatus.startEvent, inertiaStatus.startEvent);

	                inertiaStatus.i = reqFrame(this.boundInertiaFrame);
	            }
	            else {
	                inertiaStatus.ending = true;

	                inertiaStatus.sx = inertiaStatus.modifiedXe;
	                inertiaStatus.sy = inertiaStatus.modifiedYe;

	                this.pointerMove(inertiaStatus.startEvent, inertiaStatus.startEvent);
	                this.pointerEnd(inertiaStatus.startEvent, inertiaStatus.startEvent);

	                inertiaStatus.active = inertiaStatus.ending = false;
	            }
	        },

	        smoothEndFrame: function () {
	            var inertiaStatus = this.inertiaStatus,
	                t = new Date().getTime() - inertiaStatus.t0,
	                duration = this.target.options[this.prepared.name].inertia.smoothEndDuration;

	            if (t < duration) {
	                inertiaStatus.sx = easeOutQuad(t, 0, inertiaStatus.xe, duration);
	                inertiaStatus.sy = easeOutQuad(t, 0, inertiaStatus.ye, duration);

	                this.pointerMove(inertiaStatus.startEvent, inertiaStatus.startEvent);

	                inertiaStatus.i = reqFrame(this.boundSmoothEndFrame);
	            }
	            else {
	                inertiaStatus.ending = true;

	                inertiaStatus.sx = inertiaStatus.xe;
	                inertiaStatus.sy = inertiaStatus.ye;

	                this.pointerMove(inertiaStatus.startEvent, inertiaStatus.startEvent);
	                this.pointerEnd(inertiaStatus.startEvent, inertiaStatus.startEvent);

	                inertiaStatus.smoothEnd =
	                  inertiaStatus.active = inertiaStatus.ending = false;
	            }
	        },

	        addPointer: function (pointer) {
	            var id = getPointerId(pointer),
	                index = this.mouse? 0 : indexOf(this.pointerIds, id);

	            if (index === -1) {
	                index = this.pointerIds.length;
	            }

	            this.pointerIds[index] = id;
	            this.pointers[index] = pointer;

	            return index;
	        },

	        removePointer: function (pointer) {
	            var id = getPointerId(pointer),
	                index = this.mouse? 0 : indexOf(this.pointerIds, id);

	            if (index === -1) { return; }

	            this.pointers   .splice(index, 1);
	            this.pointerIds .splice(index, 1);
	            this.downTargets.splice(index, 1);
	            this.downTimes  .splice(index, 1);
	            this.holdTimers .splice(index, 1);
	        },

	        recordPointer: function (pointer) {
	            var index = this.mouse? 0: indexOf(this.pointerIds, getPointerId(pointer));

	            if (index === -1) { return; }

	            this.pointers[index] = pointer;
	        },

	        collectEventTargets: function (pointer, event, eventTarget, eventType) {
	            var pointerIndex = this.mouse? 0 : indexOf(this.pointerIds, getPointerId(pointer));

	            // do not fire a tap event if the pointer was moved before being lifted
	            if (eventType === 'tap' && (this.pointerWasMoved
	                // or if the pointerup target is different to the pointerdown target
	                || !(this.downTargets[pointerIndex] && this.downTargets[pointerIndex] === eventTarget))) {
	                return;
	            }

	            var targets = [],
	                elements = [],
	                element = eventTarget;

	            function collectSelectors (interactable, selector, context) {
	                var els = ie8MatchesSelector
	                        ? context.querySelectorAll(selector)
	                        : undefined;

	                if (interactable._iEvents[eventType]
	                    && isElement(element)
	                    && inContext(interactable, element)
	                    && !testIgnore(interactable, element, eventTarget)
	                    && testAllow(interactable, element, eventTarget)
	                    && matchesSelector(element, selector, els)) {

	                    targets.push(interactable);
	                    elements.push(element);
	                }
	            }

	            while (element) {
	                if (interact.isSet(element) && interact(element)._iEvents[eventType]) {
	                    targets.push(interact(element));
	                    elements.push(element);
	                }

	                interactables.forEachSelector(collectSelectors);

	                element = parentElement(element);
	            }

	            // create the tap event even if there are no listeners so that
	            // doubletap can still be created and fired
	            if (targets.length || eventType === 'tap') {
	                this.firePointers(pointer, event, eventTarget, targets, elements, eventType);
	            }
	        },

	        firePointers: function (pointer, event, eventTarget, targets, elements, eventType) {
	            var pointerIndex = this.mouse? 0 : indexOf(this.pointerIds, getPointerId(pointer)),
	                pointerEvent = {},
	                i,
	                // for tap events
	                interval, createNewDoubleTap;

	            // if it's a doubletap then the event properties would have been
	            // copied from the tap event and provided as the pointer argument
	            if (eventType === 'doubletap') {
	                pointerEvent = pointer;
	            }
	            else {
	                pointerExtend(pointerEvent, event);
	                if (event !== pointer) {
	                    pointerExtend(pointerEvent, pointer);
	                }

	                pointerEvent.preventDefault           = preventOriginalDefault;
	                pointerEvent.stopPropagation          = InteractEvent.prototype.stopPropagation;
	                pointerEvent.stopImmediatePropagation = InteractEvent.prototype.stopImmediatePropagation;
	                pointerEvent.interaction              = this;

	                pointerEvent.timeStamp       = new Date().getTime();
	                pointerEvent.originalEvent   = event;
	                pointerEvent.originalPointer = pointer;
	                pointerEvent.type            = eventType;
	                pointerEvent.pointerId       = getPointerId(pointer);
	                pointerEvent.pointerType     = this.mouse? 'mouse' : !supportsPointerEvent? 'touch'
	                                                    : isString(pointer.pointerType)
	                                                        ? pointer.pointerType
	                                                        : [,,'touch', 'pen', 'mouse'][pointer.pointerType];
	            }

	            if (eventType === 'tap') {
	                pointerEvent.dt = pointerEvent.timeStamp - this.downTimes[pointerIndex];

	                interval = pointerEvent.timeStamp - this.tapTime;
	                createNewDoubleTap = !!(this.prevTap && this.prevTap.type !== 'doubletap'
	                       && this.prevTap.target === pointerEvent.target
	                       && interval < 500);

	                pointerEvent.double = createNewDoubleTap;

	                this.tapTime = pointerEvent.timeStamp;
	            }

	            for (i = 0; i < targets.length; i++) {
	                pointerEvent.currentTarget = elements[i];
	                pointerEvent.interactable = targets[i];
	                targets[i].fire(pointerEvent);

	                if (pointerEvent.immediatePropagationStopped
	                    ||(pointerEvent.propagationStopped && elements[i + 1] !== pointerEvent.currentTarget)) {
	                    break;
	                }
	            }

	            if (createNewDoubleTap) {
	                var doubleTap = {};

	                extend(doubleTap, pointerEvent);

	                doubleTap.dt   = interval;
	                doubleTap.type = 'doubletap';

	                this.collectEventTargets(doubleTap, event, eventTarget, 'doubletap');

	                this.prevTap = doubleTap;
	            }
	            else if (eventType === 'tap') {
	                this.prevTap = pointerEvent;
	            }
	        },

	        validateSelector: function (pointer, event, matches, matchElements) {
	            for (var i = 0, len = matches.length; i < len; i++) {
	                var match = matches[i],
	                    matchElement = matchElements[i],
	                    action = validateAction(match.getAction(pointer, event, this, matchElement), match);

	                if (action && withinInteractionLimit(match, matchElement, action)) {
	                    this.target = match;
	                    this.element = matchElement;

	                    return action;
	                }
	            }
	        },

	        setSnapping: function (pageCoords, status) {
	            var snap = this.target.options[this.prepared.name].snap,
	                targets = [],
	                target,
	                page,
	                i;

	            status = status || this.snapStatus;

	            if (status.useStatusXY) {
	                page = { x: status.x, y: status.y };
	            }
	            else {
	                var origin = getOriginXY(this.target, this.element);

	                page = extend({}, pageCoords);

	                page.x -= origin.x;
	                page.y -= origin.y;
	            }

	            status.realX = page.x;
	            status.realY = page.y;

	            page.x = page.x - this.inertiaStatus.resumeDx;
	            page.y = page.y - this.inertiaStatus.resumeDy;

	            var len = snap.targets? snap.targets.length : 0;

	            for (var relIndex = 0; relIndex < this.snapOffsets.length; relIndex++) {
	                var relative = {
	                    x: page.x - this.snapOffsets[relIndex].x,
	                    y: page.y - this.snapOffsets[relIndex].y
	                };

	                for (i = 0; i < len; i++) {
	                    if (isFunction(snap.targets[i])) {
	                        target = snap.targets[i](relative.x, relative.y, this);
	                    }
	                    else {
	                        target = snap.targets[i];
	                    }

	                    if (!target) { continue; }

	                    targets.push({
	                        x: isNumber(target.x) ? (target.x + this.snapOffsets[relIndex].x) : relative.x,
	                        y: isNumber(target.y) ? (target.y + this.snapOffsets[relIndex].y) : relative.y,

	                        range: isNumber(target.range)? target.range: snap.range
	                    });
	                }
	            }

	            var closest = {
	                    target: null,
	                    inRange: false,
	                    distance: 0,
	                    range: 0,
	                    dx: 0,
	                    dy: 0
	                };

	            for (i = 0, len = targets.length; i < len; i++) {
	                target = targets[i];

	                var range = target.range,
	                    dx = target.x - page.x,
	                    dy = target.y - page.y,
	                    distance = hypot(dx, dy),
	                    inRange = distance <= range;

	                // Infinite targets count as being out of range
	                // compared to non infinite ones that are in range
	                if (range === Infinity && closest.inRange && closest.range !== Infinity) {
	                    inRange = false;
	                }

	                if (!closest.target || (inRange
	                    // is the closest target in range?
	                    ? (closest.inRange && range !== Infinity
	                        // the pointer is relatively deeper in this target
	                        ? distance / range < closest.distance / closest.range
	                        // this target has Infinite range and the closest doesn't
	                        : (range === Infinity && closest.range !== Infinity)
	                            // OR this target is closer that the previous closest
	                            || distance < closest.distance)
	                    // The other is not in range and the pointer is closer to this target
	                    : (!closest.inRange && distance < closest.distance))) {

	                    if (range === Infinity) {
	                        inRange = true;
	                    }

	                    closest.target = target;
	                    closest.distance = distance;
	                    closest.range = range;
	                    closest.inRange = inRange;
	                    closest.dx = dx;
	                    closest.dy = dy;

	                    status.range = range;
	                }
	            }

	            var snapChanged;

	            if (closest.target) {
	                snapChanged = (status.snappedX !== closest.target.x || status.snappedY !== closest.target.y);

	                status.snappedX = closest.target.x;
	                status.snappedY = closest.target.y;
	            }
	            else {
	                snapChanged = true;

	                status.snappedX = NaN;
	                status.snappedY = NaN;
	            }

	            status.dx = closest.dx;
	            status.dy = closest.dy;

	            status.changed = (snapChanged || (closest.inRange && !status.locked));
	            status.locked = closest.inRange;

	            return status;
	        },

	        setRestriction: function (pageCoords, status) {
	            var target = this.target,
	                restrict = target && target.options[this.prepared.name].restrict,
	                restriction = restrict && restrict.restriction,
	                page;

	            if (!restriction) {
	                return status;
	            }

	            status = status || this.restrictStatus;

	            page = status.useStatusXY
	                    ? page = { x: status.x, y: status.y }
	                    : page = extend({}, pageCoords);

	            if (status.snap && status.snap.locked) {
	                page.x += status.snap.dx || 0;
	                page.y += status.snap.dy || 0;
	            }

	            page.x -= this.inertiaStatus.resumeDx;
	            page.y -= this.inertiaStatus.resumeDy;

	            status.dx = 0;
	            status.dy = 0;
	            status.restricted = false;

	            var rect, restrictedX, restrictedY;

	            if (isString(restriction)) {
	                if (restriction === 'parent') {
	                    restriction = parentElement(this.element);
	                }
	                else if (restriction === 'self') {
	                    restriction = target.getRect(this.element);
	                }
	                else {
	                    restriction = closest(this.element, restriction);
	                }

	                if (!restriction) { return status; }
	            }

	            if (isFunction(restriction)) {
	                restriction = restriction(page.x, page.y, this.element);
	            }

	            if (isElement(restriction)) {
	                restriction = getElementRect(restriction);
	            }

	            rect = restriction;

	            if (!restriction) {
	                restrictedX = page.x;
	                restrictedY = page.y;
	            }
	            // object is assumed to have
	            // x, y, width, height or
	            // left, top, right, bottom
	            else if ('x' in restriction && 'y' in restriction) {
	                restrictedX = Math.max(Math.min(rect.x + rect.width  - this.restrictOffset.right , page.x), rect.x + this.restrictOffset.left);
	                restrictedY = Math.max(Math.min(rect.y + rect.height - this.restrictOffset.bottom, page.y), rect.y + this.restrictOffset.top );
	            }
	            else {
	                restrictedX = Math.max(Math.min(rect.right  - this.restrictOffset.right , page.x), rect.left + this.restrictOffset.left);
	                restrictedY = Math.max(Math.min(rect.bottom - this.restrictOffset.bottom, page.y), rect.top  + this.restrictOffset.top );
	            }

	            status.dx = restrictedX - page.x;
	            status.dy = restrictedY - page.y;

	            status.changed = status.restrictedX !== restrictedX || status.restrictedY !== restrictedY;
	            status.restricted = !!(status.dx || status.dy);

	            status.restrictedX = restrictedX;
	            status.restrictedY = restrictedY;

	            return status;
	        },

	        checkAndPreventDefault: function (event, interactable, element) {
	            if (!(interactable = interactable || this.target)) { return; }

	            var options = interactable.options,
	                prevent = options.preventDefault;

	            if (prevent === 'auto' && element && !/^(input|select|textarea)$/i.test(event.target.nodeName)) {
	                // do not preventDefault on pointerdown if the prepared action is a drag
	                // and dragging can only start from a certain direction - this allows
	                // a touch to pan the viewport if a drag isn't in the right direction
	                if (/down|start/i.test(event.type)
	                    && this.prepared.name === 'drag' && options.drag.axis !== 'xy') {

	                    return;
	                }

	                // with manualStart, only preventDefault while interacting
	                if (options[this.prepared.name] && options[this.prepared.name].manualStart
	                    && !this.interacting()) {
	                    return;
	                }

	                event.preventDefault();
	                return;
	            }

	            if (prevent === 'always') {
	                event.preventDefault();
	                return;
	            }
	        },

	        calcInertia: function (status) {
	            var inertiaOptions = this.target.options[this.prepared.name].inertia,
	                lambda = inertiaOptions.resistance,
	                inertiaDur = -Math.log(inertiaOptions.endSpeed / status.v0) / lambda;

	            status.x0 = this.prevEvent.pageX;
	            status.y0 = this.prevEvent.pageY;
	            status.t0 = status.startEvent.timeStamp / 1000;
	            status.sx = status.sy = 0;

	            status.modifiedXe = status.xe = (status.vx0 - inertiaDur) / lambda;
	            status.modifiedYe = status.ye = (status.vy0 - inertiaDur) / lambda;
	            status.te = inertiaDur;

	            status.lambda_v0 = lambda / status.v0;
	            status.one_ve_v0 = 1 - inertiaOptions.endSpeed / status.v0;
	        },

	        autoScrollMove: function (pointer) {
	            if (!(this.interacting()
	                && checkAutoScroll(this.target, this.prepared.name))) {
	                return;
	            }

	            if (this.inertiaStatus.active) {
	                autoScroll.x = autoScroll.y = 0;
	                return;
	            }

	            var top,
	                right,
	                bottom,
	                left,
	                options = this.target.options[this.prepared.name].autoScroll,
	                container = options.container || getWindow(this.element);

	            if (isWindow(container)) {
	                left   = pointer.clientX < autoScroll.margin;
	                top    = pointer.clientY < autoScroll.margin;
	                right  = pointer.clientX > container.innerWidth  - autoScroll.margin;
	                bottom = pointer.clientY > container.innerHeight - autoScroll.margin;
	            }
	            else {
	                var rect = getElementClientRect(container);

	                left   = pointer.clientX < rect.left   + autoScroll.margin;
	                top    = pointer.clientY < rect.top    + autoScroll.margin;
	                right  = pointer.clientX > rect.right  - autoScroll.margin;
	                bottom = pointer.clientY > rect.bottom - autoScroll.margin;
	            }

	            autoScroll.x = (right ? 1: left? -1: 0);
	            autoScroll.y = (bottom? 1:  top? -1: 0);

	            if (!autoScroll.isScrolling) {
	                // set the autoScroll properties to those of the target
	                autoScroll.margin = options.margin;
	                autoScroll.speed  = options.speed;

	                autoScroll.start(this);
	            }
	        },

	        _updateEventTargets: function (target, currentTarget) {
	            this._eventTarget    = target;
	            this._curEventTarget = currentTarget;
	        }

	    };

	    function getInteractionFromPointer (pointer, eventType, eventTarget) {
	        var i = 0, len = interactions.length,
	            mouseEvent = (/mouse/i.test(pointer.pointerType || eventType)
	                          // MSPointerEvent.MSPOINTER_TYPE_MOUSE
	                          || pointer.pointerType === 4),
	            interaction;

	        var id = getPointerId(pointer);

	        // try to resume inertia with a new pointer
	        if (/down|start/i.test(eventType)) {
	            for (i = 0; i < len; i++) {
	                interaction = interactions[i];

	                var element = eventTarget;

	                if (interaction.inertiaStatus.active && interaction.target.options[interaction.prepared.name].inertia.allowResume
	                    && (interaction.mouse === mouseEvent)) {
	                    while (element) {
	                        // if the element is the interaction element
	                        if (element === interaction.element) {
	                            return interaction;
	                        }
	                        element = parentElement(element);
	                    }
	                }
	            }
	        }

	        // if it's a mouse interaction
	        if (mouseEvent || !(supportsTouch || supportsPointerEvent)) {

	            // find a mouse interaction that's not in inertia phase
	            for (i = 0; i < len; i++) {
	                if (interactions[i].mouse && !interactions[i].inertiaStatus.active) {
	                    return interactions[i];
	                }
	            }

	            // find any interaction specifically for mouse.
	            // if the eventType is a mousedown, and inertia is active
	            // ignore the interaction
	            for (i = 0; i < len; i++) {
	                if (interactions[i].mouse && !(/down/.test(eventType) && interactions[i].inertiaStatus.active)) {
	                    return interaction;
	                }
	            }

	            // create a new interaction for mouse
	            interaction = new Interaction();
	            interaction.mouse = true;

	            return interaction;
	        }

	        // get interaction that has this pointer
	        for (i = 0; i < len; i++) {
	            if (contains(interactions[i].pointerIds, id)) {
	                return interactions[i];
	            }
	        }

	        // at this stage, a pointerUp should not return an interaction
	        if (/up|end|out/i.test(eventType)) {
	            return null;
	        }

	        // get first idle interaction
	        for (i = 0; i < len; i++) {
	            interaction = interactions[i];

	            if ((!interaction.prepared.name || (interaction.target.options.gesture.enabled))
	                && !interaction.interacting()
	                && !(!mouseEvent && interaction.mouse)) {

	                return interaction;
	            }
	        }

	        return new Interaction();
	    }

	    function doOnInteractions (method) {
	        return (function (event) {
	            var interaction,
	                eventTarget = getActualElement(event.path
	                                               ? event.path[0]
	                                               : event.target),
	                curEventTarget = getActualElement(event.currentTarget),
	                i;

	            if (supportsTouch && /touch/.test(event.type)) {
	                prevTouchTime = new Date().getTime();

	                for (i = 0; i < event.changedTouches.length; i++) {
	                    var pointer = event.changedTouches[i];

	                    interaction = getInteractionFromPointer(pointer, event.type, eventTarget);

	                    if (!interaction) { continue; }

	                    interaction._updateEventTargets(eventTarget, curEventTarget);

	                    interaction[method](pointer, event, eventTarget, curEventTarget);
	                }
	            }
	            else {
	                if (!supportsPointerEvent && /mouse/.test(event.type)) {
	                    // ignore mouse events while touch interactions are active
	                    for (i = 0; i < interactions.length; i++) {
	                        if (!interactions[i].mouse && interactions[i].pointerIsDown) {
	                            return;
	                        }
	                    }

	                    // try to ignore mouse events that are simulated by the browser
	                    // after a touch event
	                    if (new Date().getTime() - prevTouchTime < 500) {
	                        return;
	                    }
	                }

	                interaction = getInteractionFromPointer(event, event.type, eventTarget);

	                if (!interaction) { return; }

	                interaction._updateEventTargets(eventTarget, curEventTarget);

	                interaction[method](event, event, eventTarget, curEventTarget);
	            }
	        });
	    }

	    function InteractEvent (interaction, event, action, phase, element, related) {
	        var client,
	            page,
	            target      = interaction.target,
	            snapStatus  = interaction.snapStatus,
	            restrictStatus  = interaction.restrictStatus,
	            pointers    = interaction.pointers,
	            deltaSource = (target && target.options || defaultOptions).deltaSource,
	            sourceX     = deltaSource + 'X',
	            sourceY     = deltaSource + 'Y',
	            options     = target? target.options: defaultOptions,
	            origin      = getOriginXY(target, element),
	            starting    = phase === 'start',
	            ending      = phase === 'end',
	            coords      = starting? interaction.startCoords : interaction.curCoords;

	        element = element || interaction.element;

	        page   = extend({}, coords.page);
	        client = extend({}, coords.client);

	        page.x -= origin.x;
	        page.y -= origin.y;

	        client.x -= origin.x;
	        client.y -= origin.y;

	        var relativePoints = options[action].snap && options[action].snap.relativePoints ;

	        if (checkSnap(target, action) && !(starting && relativePoints && relativePoints.length)) {
	            this.snap = {
	                range  : snapStatus.range,
	                locked : snapStatus.locked,
	                x      : snapStatus.snappedX,
	                y      : snapStatus.snappedY,
	                realX  : snapStatus.realX,
	                realY  : snapStatus.realY,
	                dx     : snapStatus.dx,
	                dy     : snapStatus.dy
	            };

	            if (snapStatus.locked) {
	                page.x += snapStatus.dx;
	                page.y += snapStatus.dy;
	                client.x += snapStatus.dx;
	                client.y += snapStatus.dy;
	            }
	        }

	        if (checkRestrict(target, action) && !(starting && options[action].restrict.elementRect) && restrictStatus.restricted) {
	            page.x += restrictStatus.dx;
	            page.y += restrictStatus.dy;
	            client.x += restrictStatus.dx;
	            client.y += restrictStatus.dy;

	            this.restrict = {
	                dx: restrictStatus.dx,
	                dy: restrictStatus.dy
	            };
	        }

	        this.pageX     = page.x;
	        this.pageY     = page.y;
	        this.clientX   = client.x;
	        this.clientY   = client.y;

	        this.x0        = interaction.startCoords.page.x - origin.x;
	        this.y0        = interaction.startCoords.page.y - origin.y;
	        this.clientX0  = interaction.startCoords.client.x - origin.x;
	        this.clientY0  = interaction.startCoords.client.y - origin.y;
	        this.ctrlKey   = event.ctrlKey;
	        this.altKey    = event.altKey;
	        this.shiftKey  = event.shiftKey;
	        this.metaKey   = event.metaKey;
	        this.button    = event.button;
	        this.buttons   = event.buttons;
	        this.target    = element;
	        this.t0        = interaction.downTimes[0];
	        this.type      = action + (phase || '');

	        this.interaction = interaction;
	        this.interactable = target;

	        var inertiaStatus = interaction.inertiaStatus;

	        if (inertiaStatus.active) {
	            this.detail = 'inertia';
	        }

	        if (related) {
	            this.relatedTarget = related;
	        }

	        // end event dx, dy is difference between start and end points
	        if (ending) {
	            if (deltaSource === 'client') {
	                this.dx = client.x - interaction.startCoords.client.x;
	                this.dy = client.y - interaction.startCoords.client.y;
	            }
	            else {
	                this.dx = page.x - interaction.startCoords.page.x;
	                this.dy = page.y - interaction.startCoords.page.y;
	            }
	        }
	        else if (starting) {
	            this.dx = 0;
	            this.dy = 0;
	        }
	        // copy properties from previousmove if starting inertia
	        else if (phase === 'inertiastart') {
	            this.dx = interaction.prevEvent.dx;
	            this.dy = interaction.prevEvent.dy;
	        }
	        else {
	            if (deltaSource === 'client') {
	                this.dx = client.x - interaction.prevEvent.clientX;
	                this.dy = client.y - interaction.prevEvent.clientY;
	            }
	            else {
	                this.dx = page.x - interaction.prevEvent.pageX;
	                this.dy = page.y - interaction.prevEvent.pageY;
	            }
	        }
	        if (interaction.prevEvent && interaction.prevEvent.detail === 'inertia'
	            && !inertiaStatus.active
	            && options[action].inertia && options[action].inertia.zeroResumeDelta) {

	            inertiaStatus.resumeDx += this.dx;
	            inertiaStatus.resumeDy += this.dy;

	            this.dx = this.dy = 0;
	        }

	        if (action === 'resize' && interaction.resizeAxes) {
	            if (options.resize.square) {
	                if (interaction.resizeAxes === 'y') {
	                    this.dx = this.dy;
	                }
	                else {
	                    this.dy = this.dx;
	                }
	                this.axes = 'xy';
	            }
	            else {
	                this.axes = interaction.resizeAxes;

	                if (interaction.resizeAxes === 'x') {
	                    this.dy = 0;
	                }
	                else if (interaction.resizeAxes === 'y') {
	                    this.dx = 0;
	                }
	            }
	        }
	        else if (action === 'gesture') {
	            this.touches = [pointers[0], pointers[1]];

	            if (starting) {
	                this.distance = touchDistance(pointers, deltaSource);
	                this.box      = touchBBox(pointers);
	                this.scale    = 1;
	                this.ds       = 0;
	                this.angle    = touchAngle(pointers, undefined, deltaSource);
	                this.da       = 0;
	            }
	            else if (ending || event instanceof InteractEvent) {
	                this.distance = interaction.prevEvent.distance;
	                this.box      = interaction.prevEvent.box;
	                this.scale    = interaction.prevEvent.scale;
	                this.ds       = this.scale - 1;
	                this.angle    = interaction.prevEvent.angle;
	                this.da       = this.angle - interaction.gesture.startAngle;
	            }
	            else {
	                this.distance = touchDistance(pointers, deltaSource);
	                this.box      = touchBBox(pointers);
	                this.scale    = this.distance / interaction.gesture.startDistance;
	                this.angle    = touchAngle(pointers, interaction.gesture.prevAngle, deltaSource);

	                this.ds = this.scale - interaction.gesture.prevScale;
	                this.da = this.angle - interaction.gesture.prevAngle;
	            }
	        }

	        if (starting) {
	            this.timeStamp = interaction.downTimes[0];
	            this.dt        = 0;
	            this.duration  = 0;
	            this.speed     = 0;
	            this.velocityX = 0;
	            this.velocityY = 0;
	        }
	        else if (phase === 'inertiastart') {
	            this.timeStamp = interaction.prevEvent.timeStamp;
	            this.dt        = interaction.prevEvent.dt;
	            this.duration  = interaction.prevEvent.duration;
	            this.speed     = interaction.prevEvent.speed;
	            this.velocityX = interaction.prevEvent.velocityX;
	            this.velocityY = interaction.prevEvent.velocityY;
	        }
	        else {
	            this.timeStamp = new Date().getTime();
	            this.dt        = this.timeStamp - interaction.prevEvent.timeStamp;
	            this.duration  = this.timeStamp - interaction.downTimes[0];

	            if (event instanceof InteractEvent) {
	                var dx = this[sourceX] - interaction.prevEvent[sourceX],
	                    dy = this[sourceY] - interaction.prevEvent[sourceY],
	                    dt = this.dt / 1000;

	                this.speed = hypot(dx, dy) / dt;
	                this.velocityX = dx / dt;
	                this.velocityY = dy / dt;
	            }
	            // if normal move or end event, use previous user event coords
	            else {
	                // speed and velocity in pixels per second
	                this.speed = interaction.pointerDelta[deltaSource].speed;
	                this.velocityX = interaction.pointerDelta[deltaSource].vx;
	                this.velocityY = interaction.pointerDelta[deltaSource].vy;
	            }
	        }

	        if ((ending || phase === 'inertiastart')
	            && interaction.prevEvent.speed > 600 && this.timeStamp - interaction.prevEvent.timeStamp < 150) {

	            var angle = 180 * Math.atan2(interaction.prevEvent.velocityY, interaction.prevEvent.velocityX) / Math.PI,
	                overlap = 22.5;

	            if (angle < 0) {
	                angle += 360;
	            }

	            var left = 135 - overlap <= angle && angle < 225 + overlap,
	                up   = 225 - overlap <= angle && angle < 315 + overlap,

	                right = !left && (315 - overlap <= angle || angle <  45 + overlap),
	                down  = !up   &&   45 - overlap <= angle && angle < 135 + overlap;

	            this.swipe = {
	                up   : up,
	                down : down,
	                left : left,
	                right: right,
	                angle: angle,
	                speed: interaction.prevEvent.speed,
	                velocity: {
	                    x: interaction.prevEvent.velocityX,
	                    y: interaction.prevEvent.velocityY
	                }
	            };
	        }
	    }

	    InteractEvent.prototype = {
	        preventDefault: blank,
	        stopImmediatePropagation: function () {
	            this.immediatePropagationStopped = this.propagationStopped = true;
	        },
	        stopPropagation: function () {
	            this.propagationStopped = true;
	        }
	    };

	    function preventOriginalDefault () {
	        this.originalEvent.preventDefault();
	    }

	    function getActionCursor (action) {
	        var cursor = '';

	        if (action.name === 'drag') {
	            cursor =  actionCursors.drag;
	        }
	        if (action.name === 'resize') {
	            if (action.axis) {
	                cursor =  actionCursors[action.name + action.axis];
	            }
	            else if (action.edges) {
	                var cursorKey = 'resize',
	                    edgeNames = ['top', 'bottom', 'left', 'right'];

	                for (var i = 0; i < 4; i++) {
	                    if (action.edges[edgeNames[i]]) {
	                        cursorKey += edgeNames[i];
	                    }
	                }

	                cursor = actionCursors[cursorKey];
	            }
	        }

	        return cursor;
	    }

	    function checkResizeEdge (name, value, page, element, interactableElement, rect, margin) {
	        // false, '', undefined, null
	        if (!value) { return false; }

	        // true value, use pointer coords and element rect
	        if (value === true) {
	            // if dimensions are negative, "switch" edges
	            var width = isNumber(rect.width)? rect.width : rect.right - rect.left,
	                height = isNumber(rect.height)? rect.height : rect.bottom - rect.top;

	            if (width < 0) {
	                if      (name === 'left' ) { name = 'right'; }
	                else if (name === 'right') { name = 'left' ; }
	            }
	            if (height < 0) {
	                if      (name === 'top'   ) { name = 'bottom'; }
	                else if (name === 'bottom') { name = 'top'   ; }
	            }

	            if (name === 'left'  ) { return page.x < ((width  >= 0? rect.left: rect.right ) + margin); }
	            if (name === 'top'   ) { return page.y < ((height >= 0? rect.top : rect.bottom) + margin); }

	            if (name === 'right' ) { return page.x > ((width  >= 0? rect.right : rect.left) - margin); }
	            if (name === 'bottom') { return page.y > ((height >= 0? rect.bottom: rect.top ) - margin); }
	        }

	        // the remaining checks require an element
	        if (!isElement(element)) { return false; }

	        return isElement(value)
	                    // the value is an element to use as a resize handle
	                    ? value === element
	                    // otherwise check if element matches value as selector
	                    : matchesUpTo(element, value, interactableElement);
	    }

	    function defaultActionChecker (pointer, interaction, element) {
	        var rect = this.getRect(element),
	            shouldResize = false,
	            action = null,
	            resizeAxes = null,
	            resizeEdges,
	            page = extend({}, interaction.curCoords.page),
	            options = this.options;

	        if (!rect) { return null; }

	        if (actionIsEnabled.resize && options.resize.enabled) {
	            var resizeOptions = options.resize;

	            resizeEdges = {
	                left: false, right: false, top: false, bottom: false
	            };

	            // if using resize.edges
	            if (isObject(resizeOptions.edges)) {
	                for (var edge in resizeEdges) {
	                    resizeEdges[edge] = checkResizeEdge(edge,
	                                                        resizeOptions.edges[edge],
	                                                        page,
	                                                        interaction._eventTarget,
	                                                        element,
	                                                        rect,
	                                                        resizeOptions.margin || margin);
	                }

	                resizeEdges.left = resizeEdges.left && !resizeEdges.right;
	                resizeEdges.top  = resizeEdges.top  && !resizeEdges.bottom;

	                shouldResize = resizeEdges.left || resizeEdges.right || resizeEdges.top || resizeEdges.bottom;
	            }
	            else {
	                var right  = options.resize.axis !== 'y' && page.x > (rect.right  - margin),
	                    bottom = options.resize.axis !== 'x' && page.y > (rect.bottom - margin);

	                shouldResize = right || bottom;
	                resizeAxes = (right? 'x' : '') + (bottom? 'y' : '');
	            }
	        }

	        action = shouldResize
	            ? 'resize'
	            : actionIsEnabled.drag && options.drag.enabled
	                ? 'drag'
	                : null;

	        if (actionIsEnabled.gesture
	            && interaction.pointerIds.length >=2
	            && !(interaction.dragging || interaction.resizing)) {
	            action = 'gesture';
	        }

	        if (action) {
	            return {
	                name: action,
	                axis: resizeAxes,
	                edges: resizeEdges
	            };
	        }

	        return null;
	    }

	    // Check if action is enabled globally and the current target supports it
	    // If so, return the validated action. Otherwise, return null
	    function validateAction (action, interactable) {
	        if (!isObject(action)) { return null; }

	        var actionName = action.name,
	            options = interactable.options;

	        if ((  (actionName  === 'resize'   && options.resize.enabled )
	            || (actionName      === 'drag'     && options.drag.enabled  )
	            || (actionName      === 'gesture'  && options.gesture.enabled))
	            && actionIsEnabled[actionName]) {

	            if (actionName === 'resize' || actionName === 'resizeyx') {
	                actionName = 'resizexy';
	            }

	            return action;
	        }
	        return null;
	    }

	    var listeners = {},
	        interactionListeners = [
	            'dragStart', 'dragMove', 'resizeStart', 'resizeMove', 'gestureStart', 'gestureMove',
	            'pointerOver', 'pointerOut', 'pointerHover', 'selectorDown',
	            'pointerDown', 'pointerMove', 'pointerUp', 'pointerCancel', 'pointerEnd',
	            'addPointer', 'removePointer', 'recordPointer', 'autoScrollMove'
	        ];

	    for (var i = 0, len = interactionListeners.length; i < len; i++) {
	        var name = interactionListeners[i];

	        listeners[name] = doOnInteractions(name);
	    }

	    // bound to the interactable context when a DOM event
	    // listener is added to a selector interactable
	    function delegateListener (event, useCapture) {
	        var fakeEvent = {},
	            delegated = delegatedEvents[event.type],
	            eventTarget = getActualElement(event.path
	                                           ? event.path[0]
	                                           : event.target),
	            element = eventTarget;

	        useCapture = useCapture? true: false;

	        // duplicate the event so that currentTarget can be changed
	        for (var prop in event) {
	            fakeEvent[prop] = event[prop];
	        }

	        fakeEvent.originalEvent = event;
	        fakeEvent.preventDefault = preventOriginalDefault;

	        // climb up document tree looking for selector matches
	        while (isElement(element)) {
	            for (var i = 0; i < delegated.selectors.length; i++) {
	                var selector = delegated.selectors[i],
	                    context = delegated.contexts[i];

	                if (matchesSelector(element, selector)
	                    && nodeContains(context, eventTarget)
	                    && nodeContains(context, element)) {

	                    var listeners = delegated.listeners[i];

	                    fakeEvent.currentTarget = element;

	                    for (var j = 0; j < listeners.length; j++) {
	                        if (listeners[j][1] === useCapture) {
	                            listeners[j][0](fakeEvent);
	                        }
	                    }
	                }
	            }

	            element = parentElement(element);
	        }
	    }

	    function delegateUseCapture (event) {
	        return delegateListener.call(this, event, true);
	    }

	    interactables.indexOfElement = function indexOfElement (element, context) {
	        context = context || document;

	        for (var i = 0; i < this.length; i++) {
	            var interactable = this[i];

	            if ((interactable.selector === element
	                && (interactable._context === context))
	                || (!interactable.selector && interactable._element === element)) {

	                return i;
	            }
	        }
	        return -1;
	    };

	    interactables.get = function interactableGet (element, options) {
	        return this[this.indexOfElement(element, options && options.context)];
	    };

	    interactables.forEachSelector = function (callback) {
	        for (var i = 0; i < this.length; i++) {
	            var interactable = this[i];

	            if (!interactable.selector) {
	                continue;
	            }

	            var ret = callback(interactable, interactable.selector, interactable._context, i, this);

	            if (ret !== undefined) {
	                return ret;
	            }
	        }
	    };

	    /*\
	     * interact
	     [ method ]
	     *
	     * The methods of this variable can be used to set elements as
	     * interactables and also to change various default settings.
	     *
	     * Calling it as a function and passing an element or a valid CSS selector
	     * string returns an Interactable object which has various methods to
	     * configure it.
	     *
	     - element (Element | string) The HTML or SVG Element to interact with or CSS selector
	     = (object) An @Interactable
	     *
	     > Usage
	     | interact(document.getElementById('draggable')).draggable(true);
	     |
	     | var rectables = interact('rect');
	     | rectables
	     |     .gesturable(true)
	     |     .on('gesturemove', function (event) {
	     |         // something cool...
	     |     })
	     |     .autoScroll(true);
	    \*/
	    function interact (element, options) {
	        return interactables.get(element, options) || new Interactable(element, options);
	    }

	    /*\
	     * Interactable
	     [ property ]
	     **
	     * Object type returned by @interact
	    \*/
	    function Interactable (element, options) {
	        this._element = element;
	        this._iEvents = this._iEvents || {};

	        var _window;

	        if (trySelector(element)) {
	            this.selector = element;

	            var context = options && options.context;

	            _window = context? getWindow(context) : window;

	            if (context && (_window.Node
	                    ? context instanceof _window.Node
	                    : (isElement(context) || context === _window.document))) {

	                this._context = context;
	            }
	        }
	        else {
	            _window = getWindow(element);

	            if (isElement(element, _window)) {

	                if (PointerEvent) {
	                    events.add(this._element, pEventTypes.down, listeners.pointerDown );
	                    events.add(this._element, pEventTypes.move, listeners.pointerHover);
	                }
	                else {
	                    events.add(this._element, 'mousedown' , listeners.pointerDown );
	                    events.add(this._element, 'mousemove' , listeners.pointerHover);
	                    events.add(this._element, 'touchstart', listeners.pointerDown );
	                    events.add(this._element, 'touchmove' , listeners.pointerHover);
	                }
	            }
	        }

	        this._doc = _window.document;

	        if (!contains(documents, this._doc)) {
	            listenToDocument(this._doc);
	        }

	        interactables.push(this);

	        this.set(options);
	    }

	    Interactable.prototype = {
	        setOnEvents: function (action, phases) {
	            if (action === 'drop') {
	                if (isFunction(phases.ondrop)          ) { this.ondrop           = phases.ondrop          ; }
	                if (isFunction(phases.ondropactivate)  ) { this.ondropactivate   = phases.ondropactivate  ; }
	                if (isFunction(phases.ondropdeactivate)) { this.ondropdeactivate = phases.ondropdeactivate; }
	                if (isFunction(phases.ondragenter)     ) { this.ondragenter      = phases.ondragenter     ; }
	                if (isFunction(phases.ondragleave)     ) { this.ondragleave      = phases.ondragleave     ; }
	                if (isFunction(phases.ondropmove)      ) { this.ondropmove       = phases.ondropmove      ; }
	            }
	            else {
	                action = 'on' + action;

	                if (isFunction(phases.onstart)       ) { this[action + 'start'         ] = phases.onstart         ; }
	                if (isFunction(phases.onmove)        ) { this[action + 'move'          ] = phases.onmove          ; }
	                if (isFunction(phases.onend)         ) { this[action + 'end'           ] = phases.onend           ; }
	                if (isFunction(phases.oninertiastart)) { this[action + 'inertiastart'  ] = phases.oninertiastart  ; }
	            }

	            return this;
	        },

	        /*\
	         * Interactable.draggable
	         [ method ]
	         *
	         * Gets or sets whether drag actions can be performed on the
	         * Interactable
	         *
	         = (boolean) Indicates if this can be the target of drag events
	         | var isDraggable = interact('ul li').draggable();
	         * or
	         - options (boolean | object) #optional true/false or An object with event listeners to be fired on drag events (object makes the Interactable draggable)
	         = (object) This Interactable
	         | interact(element).draggable({
	         |     onstart: function (event) {},
	         |     onmove : function (event) {},
	         |     onend  : function (event) {},
	         |
	         |     // the axis in which the first movement must be
	         |     // for the drag sequence to start
	         |     // 'xy' by default - any direction
	         |     axis: 'x' || 'y' || 'xy',
	         |
	         |     // max number of drags that can happen concurrently
	         |     // with elements of this Interactable. Infinity by default
	         |     max: Infinity,
	         |
	         |     // max number of drags that can target the same element+Interactable
	         |     // 1 by default
	         |     maxPerElement: 2
	         | });
	        \*/
	        draggable: function (options) {
	            if (isObject(options)) {
	                this.options.drag.enabled = options.enabled === false? false: true;
	                this.setPerAction('drag', options);
	                this.setOnEvents('drag', options);

	                if (/^x$|^y$|^xy$/.test(options.axis)) {
	                    this.options.drag.axis = options.axis;
	                }
	                else if (options.axis === null) {
	                    delete this.options.drag.axis;
	                }

	                return this;
	            }

	            if (isBool(options)) {
	                this.options.drag.enabled = options;

	                return this;
	            }

	            return this.options.drag;
	        },

	        setPerAction: function (action, options) {
	            // for all the default per-action options
	            for (var option in options) {
	                // if this option exists for this action
	                if (option in defaultOptions[action]) {
	                    // if the option in the options arg is an object value
	                    if (isObject(options[option])) {
	                        // duplicate the object
	                        this.options[action][option] = extend(this.options[action][option] || {}, options[option]);

	                        if (isObject(defaultOptions.perAction[option]) && 'enabled' in defaultOptions.perAction[option]) {
	                            this.options[action][option].enabled = options[option].enabled === false? false : true;
	                        }
	                    }
	                    else if (isBool(options[option]) && isObject(defaultOptions.perAction[option])) {
	                        this.options[action][option].enabled = options[option];
	                    }
	                    else if (options[option] !== undefined) {
	                        // or if it's not undefined, do a plain assignment
	                        this.options[action][option] = options[option];
	                    }
	                }
	            }
	        },

	        /*\
	         * Interactable.dropzone
	         [ method ]
	         *
	         * Returns or sets whether elements can be dropped onto this
	         * Interactable to trigger drop events
	         *
	         * Dropzones can receive the following events:
	         *  - `dropactivate` and `dropdeactivate` when an acceptable drag starts and ends
	         *  - `dragenter` and `dragleave` when a draggable enters and leaves the dropzone
	         *  - `dragmove` when a draggable that has entered the dropzone is moved
	         *  - `drop` when a draggable is dropped into this dropzone
	         *
	         *  Use the `accept` option to allow only elements that match the given CSS selector or element.
	         *
	         *  Use the `overlap` option to set how drops are checked for. The allowed values are:
	         *   - `'pointer'`, the pointer must be over the dropzone (default)
	         *   - `'center'`, the draggable element's center must be over the dropzone
	         *   - a number from 0-1 which is the `(intersection area) / (draggable area)`.
	         *       e.g. `0.5` for drop to happen when half of the area of the
	         *       draggable is over the dropzone
	         *
	         - options (boolean | object | null) #optional The new value to be set.
	         | interact('.drop').dropzone({
	         |   accept: '.can-drop' || document.getElementById('single-drop'),
	         |   overlap: 'pointer' || 'center' || zeroToOne
	         | }
	         = (boolean | object) The current setting or this Interactable
	        \*/
	        dropzone: function (options) {
	            if (isObject(options)) {
	                this.options.drop.enabled = options.enabled === false? false: true;
	                this.setOnEvents('drop', options);

	                if (/^(pointer|center)$/.test(options.overlap)) {
	                    this.options.drop.overlap = options.overlap;
	                }
	                else if (isNumber(options.overlap)) {
	                    this.options.drop.overlap = Math.max(Math.min(1, options.overlap), 0);
	                }
	                if ('accept' in options) {
	                  this.options.drop.accept = options.accept;
	                }
	                if ('checker' in options) {
	                  this.options.drop.checker = options.checker;
	                }

	                return this;
	            }

	            if (isBool(options)) {
	                this.options.drop.enabled = options;

	                return this;
	            }

	            return this.options.drop;
	        },

	        dropCheck: function (dragEvent, event, draggable, draggableElement, dropElement, rect) {
	            var dropped = false;

	            // if the dropzone has no rect (eg. display: none)
	            // call the custom dropChecker or just return false
	            if (!(rect = rect || this.getRect(dropElement))) {
	                return (this.options.drop.checker
	                    ? this.options.drop.checker(dragEvent, event, dropped, this, dropElement, draggable, draggableElement)
	                    : false);
	            }

	            var dropOverlap = this.options.drop.overlap;

	            if (dropOverlap === 'pointer') {
	                var page = getPageXY(dragEvent),
	                    origin = getOriginXY(draggable, draggableElement),
	                    horizontal,
	                    vertical;

	                page.x += origin.x;
	                page.y += origin.y;

	                horizontal = (page.x > rect.left) && (page.x < rect.right);
	                vertical   = (page.y > rect.top ) && (page.y < rect.bottom);

	                dropped = horizontal && vertical;
	            }

	            var dragRect = draggable.getRect(draggableElement);

	            if (dropOverlap === 'center') {
	                var cx = dragRect.left + dragRect.width  / 2,
	                    cy = dragRect.top  + dragRect.height / 2;

	                dropped = cx >= rect.left && cx <= rect.right && cy >= rect.top && cy <= rect.bottom;
	            }

	            if (isNumber(dropOverlap)) {
	                var overlapArea  = (Math.max(0, Math.min(rect.right , dragRect.right ) - Math.max(rect.left, dragRect.left))
	                                  * Math.max(0, Math.min(rect.bottom, dragRect.bottom) - Math.max(rect.top , dragRect.top ))),
	                    overlapRatio = overlapArea / (dragRect.width * dragRect.height);

	                dropped = overlapRatio >= dropOverlap;
	            }

	            if (this.options.drop.checker) {
	                dropped = this.options.drop.checker(dragEvent, event, dropped, this, dropElement, draggable, draggableElement);
	            }

	            return dropped;
	        },

	        /*\
	         * Interactable.dropChecker
	         [ method ]
	         *
	         * DEPRECATED. Use interactable.dropzone({ checker: function... }) instead.
	         *
	         * Gets or sets the function used to check if a dragged element is
	         * over this Interactable.
	         *
	         - checker (function) #optional The function that will be called when checking for a drop
	         = (Function | Interactable) The checker function or this Interactable
	         *
	         * The checker function takes the following arguments:
	         *
	         - dragEvent (InteractEvent) The related dragmove or dragend event
	         - event (TouchEvent | PointerEvent | MouseEvent) The user move/up/end Event related to the dragEvent
	         - dropped (boolean) The value from the default drop checker
	         - dropzone (Interactable) The dropzone interactable
	         - dropElement (Element) The dropzone element
	         - draggable (Interactable) The Interactable being dragged
	         - draggableElement (Element) The actual element that's being dragged
	         *
	         > Usage:
	         | interact(target)
	         | .dropChecker(function(dragEvent,         // related dragmove or dragend event
	         |                       event,             // TouchEvent/PointerEvent/MouseEvent
	         |                       dropped,           // bool result of the default checker
	         |                       dropzone,          // dropzone Interactable
	         |                       dropElement,       // dropzone elemnt
	         |                       draggable,         // draggable Interactable
	         |                       draggableElement) {// draggable element
	         |
	         |   return dropped && event.target.hasAttribute('allow-drop');
	         | }
	        \*/
	        dropChecker: function (checker) {
	            if (isFunction(checker)) {
	                this.options.drop.checker = checker;

	                return this;
	            }
	            if (checker === null) {
	                delete this.options.getRect;

	                return this;
	            }

	            return this.options.drop.checker;
	        },

	        /*\
	         * Interactable.accept
	         [ method ]
	         *
	         * Deprecated. add an `accept` property to the options object passed to
	         * @Interactable.dropzone instead.
	         *
	         * Gets or sets the Element or CSS selector match that this
	         * Interactable accepts if it is a dropzone.
	         *
	         - newValue (Element | string | null) #optional
	         * If it is an Element, then only that element can be dropped into this dropzone.
	         * If it is a string, the element being dragged must match it as a selector.
	         * If it is null, the accept options is cleared - it accepts any element.
	         *
	         = (string | Element | null | Interactable) The current accept option if given `undefined` or this Interactable
	        \*/
	        accept: function (newValue) {
	            if (isElement(newValue)) {
	                this.options.drop.accept = newValue;

	                return this;
	            }

	            // test if it is a valid CSS selector
	            if (trySelector(newValue)) {
	                this.options.drop.accept = newValue;

	                return this;
	            }

	            if (newValue === null) {
	                delete this.options.drop.accept;

	                return this;
	            }

	            return this.options.drop.accept;
	        },

	        /*\
	         * Interactable.resizable
	         [ method ]
	         *
	         * Gets or sets whether resize actions can be performed on the
	         * Interactable
	         *
	         = (boolean) Indicates if this can be the target of resize elements
	         | var isResizeable = interact('input[type=text]').resizable();
	         * or
	         - options (boolean | object) #optional true/false or An object with event listeners to be fired on resize events (object makes the Interactable resizable)
	         = (object) This Interactable
	         | interact(element).resizable({
	         |     onstart: function (event) {},
	         |     onmove : function (event) {},
	         |     onend  : function (event) {},
	         |
	         |     edges: {
	         |       top   : true,       // Use pointer coords to check for resize.
	         |       left  : false,      // Disable resizing from left edge.
	         |       bottom: '.resize-s',// Resize if pointer target matches selector
	         |       right : handleEl    // Resize if pointer target is the given Element
	         |     },
	         |
	         |     // Width and height can be adjusted independently. When `true`, width and
	         |     // height are adjusted at a 1:1 ratio.
	         |     square: false,
	         |
	         |     // Width and height can be adjusted independently. When `true`, width and
	         |     // height maintain the aspect ratio they had when resizing started.
	         |     preserveAspectRatio: false,
	         |
	         |     // a value of 'none' will limit the resize rect to a minimum of 0x0
	         |     // 'negate' will allow the rect to have negative width/height
	         |     // 'reposition' will keep the width/height positive by swapping
	         |     // the top and bottom edges and/or swapping the left and right edges
	         |     invert: 'none' || 'negate' || 'reposition'
	         |
	         |     // limit multiple resizes.
	         |     // See the explanation in the @Interactable.draggable example
	         |     max: Infinity,
	         |     maxPerElement: 1,
	         | });
	        \*/
	        resizable: function (options) {
	            if (isObject(options)) {
	                this.options.resize.enabled = options.enabled === false? false: true;
	                this.setPerAction('resize', options);
	                this.setOnEvents('resize', options);

	                if (/^x$|^y$|^xy$/.test(options.axis)) {
	                    this.options.resize.axis = options.axis;
	                }
	                else if (options.axis === null) {
	                    this.options.resize.axis = defaultOptions.resize.axis;
	                }

	                if (isBool(options.preserveAspectRatio)) {
	                    this.options.resize.preserveAspectRatio = options.preserveAspectRatio;
	                }
	                else if (isBool(options.square)) {
	                    this.options.resize.square = options.square;
	                }

	                return this;
	            }
	            if (isBool(options)) {
	                this.options.resize.enabled = options;

	                return this;
	            }
	            return this.options.resize;
	        },

	        /*\
	         * Interactable.squareResize
	         [ method ]
	         *
	         * Deprecated. Add a `square: true || false` property to @Interactable.resizable instead
	         *
	         * Gets or sets whether resizing is forced 1:1 aspect
	         *
	         = (boolean) Current setting
	         *
	         * or
	         *
	         - newValue (boolean) #optional
	         = (object) this Interactable
	        \*/
	        squareResize: function (newValue) {
	            if (isBool(newValue)) {
	                this.options.resize.square = newValue;

	                return this;
	            }

	            if (newValue === null) {
	                delete this.options.resize.square;

	                return this;
	            }

	            return this.options.resize.square;
	        },

	        /*\
	         * Interactable.gesturable
	         [ method ]
	         *
	         * Gets or sets whether multitouch gestures can be performed on the
	         * Interactable's element
	         *
	         = (boolean) Indicates if this can be the target of gesture events
	         | var isGestureable = interact(element).gesturable();
	         * or
	         - options (boolean | object) #optional true/false or An object with event listeners to be fired on gesture events (makes the Interactable gesturable)
	         = (object) this Interactable
	         | interact(element).gesturable({
	         |     onstart: function (event) {},
	         |     onmove : function (event) {},
	         |     onend  : function (event) {},
	         |
	         |     // limit multiple gestures.
	         |     // See the explanation in @Interactable.draggable example
	         |     max: Infinity,
	         |     maxPerElement: 1,
	         | });
	        \*/
	        gesturable: function (options) {
	            if (isObject(options)) {
	                this.options.gesture.enabled = options.enabled === false? false: true;
	                this.setPerAction('gesture', options);
	                this.setOnEvents('gesture', options);

	                return this;
	            }

	            if (isBool(options)) {
	                this.options.gesture.enabled = options;

	                return this;
	            }

	            return this.options.gesture;
	        },

	        /*\
	         * Interactable.autoScroll
	         [ method ]
	         **
	         * Deprecated. Add an `autoscroll` property to the options object
	         * passed to @Interactable.draggable or @Interactable.resizable instead.
	         *
	         * Returns or sets whether dragging and resizing near the edges of the
	         * window/container trigger autoScroll for this Interactable
	         *
	         = (object) Object with autoScroll properties
	         *
	         * or
	         *
	         - options (object | boolean) #optional
	         * options can be:
	         * - an object with margin, distance and interval properties,
	         * - true or false to enable or disable autoScroll or
	         = (Interactable) this Interactable
	        \*/
	        autoScroll: function (options) {
	            if (isObject(options)) {
	                options = extend({ actions: ['drag', 'resize']}, options);
	            }
	            else if (isBool(options)) {
	                options = { actions: ['drag', 'resize'], enabled: options };
	            }

	            return this.setOptions('autoScroll', options);
	        },

	        /*\
	         * Interactable.snap
	         [ method ]
	         **
	         * Deprecated. Add a `snap` property to the options object passed
	         * to @Interactable.draggable or @Interactable.resizable instead.
	         *
	         * Returns or sets if and how action coordinates are snapped. By
	         * default, snapping is relative to the pointer coordinates. You can
	         * change this by setting the
	         * [`elementOrigin`](https://github.com/taye/interact.js/pull/72).
	         **
	         = (boolean | object) `false` if snap is disabled; object with snap properties if snap is enabled
	         **
	         * or
	         **
	         - options (object | boolean | null) #optional
	         = (Interactable) this Interactable
	         > Usage
	         | interact(document.querySelector('#thing')).snap({
	         |     targets: [
	         |         // snap to this specific point
	         |         {
	         |             x: 100,
	         |             y: 100,
	         |             range: 25
	         |         },
	         |         // give this function the x and y page coords and snap to the object returned
	         |         function (x, y) {
	         |             return {
	         |                 x: x,
	         |                 y: (75 + 50 * Math.sin(x * 0.04)),
	         |                 range: 40
	         |             };
	         |         },
	         |         // create a function that snaps to a grid
	         |         interact.createSnapGrid({
	         |             x: 50,
	         |             y: 50,
	         |             range: 10,              // optional
	         |             offset: { x: 5, y: 10 } // optional
	         |         })
	         |     ],
	         |     // do not snap during normal movement.
	         |     // Instead, trigger only one snapped move event
	         |     // immediately before the end event.
	         |     endOnly: true,
	         |
	         |     relativePoints: [
	         |         { x: 0, y: 0 },  // snap relative to the top left of the element
	         |         { x: 1, y: 1 },  // and also to the bottom right
	         |     ],  
	         |
	         |     // offset the snap target coordinates
	         |     // can be an object with x/y or 'startCoords'
	         |     offset: { x: 50, y: 50 }
	         |   }
	         | });
	        \*/
	        snap: function (options) {
	            var ret = this.setOptions('snap', options);

	            if (ret === this) { return this; }

	            return ret.drag;
	        },

	        setOptions: function (option, options) {
	            var actions = options && isArray(options.actions)
	                    ? options.actions
	                    : ['drag'];

	            var i;

	            if (isObject(options) || isBool(options)) {
	                for (i = 0; i < actions.length; i++) {
	                    var action = /resize/.test(actions[i])? 'resize' : actions[i];

	                    if (!isObject(this.options[action])) { continue; }

	                    var thisOption = this.options[action][option];

	                    if (isObject(options)) {
	                        extend(thisOption, options);
	                        thisOption.enabled = options.enabled === false? false: true;

	                        if (option === 'snap') {
	                            if (thisOption.mode === 'grid') {
	                                thisOption.targets = [
	                                    interact.createSnapGrid(extend({
	                                        offset: thisOption.gridOffset || { x: 0, y: 0 }
	                                    }, thisOption.grid || {}))
	                                ];
	                            }
	                            else if (thisOption.mode === 'anchor') {
	                                thisOption.targets = thisOption.anchors;
	                            }
	                            else if (thisOption.mode === 'path') {
	                                thisOption.targets = thisOption.paths;
	                            }

	                            if ('elementOrigin' in options) {
	                                thisOption.relativePoints = [options.elementOrigin];
	                            }
	                        }
	                    }
	                    else if (isBool(options)) {
	                        thisOption.enabled = options;
	                    }
	                }

	                return this;
	            }

	            var ret = {},
	                allActions = ['drag', 'resize', 'gesture'];

	            for (i = 0; i < allActions.length; i++) {
	                if (option in defaultOptions[allActions[i]]) {
	                    ret[allActions[i]] = this.options[allActions[i]][option];
	                }
	            }

	            return ret;
	        },


	        /*\
	         * Interactable.inertia
	         [ method ]
	         **
	         * Deprecated. Add an `inertia` property to the options object passed
	         * to @Interactable.draggable or @Interactable.resizable instead.
	         *
	         * Returns or sets if and how events continue to run after the pointer is released
	         **
	         = (boolean | object) `false` if inertia is disabled; `object` with inertia properties if inertia is enabled
	         **
	         * or
	         **
	         - options (object | boolean | null) #optional
	         = (Interactable) this Interactable
	         > Usage
	         | // enable and use default settings
	         | interact(element).inertia(true);
	         |
	         | // enable and use custom settings
	         | interact(element).inertia({
	         |     // value greater than 0
	         |     // high values slow the object down more quickly
	         |     resistance     : 16,
	         |
	         |     // the minimum launch speed (pixels per second) that results in inertia start
	         |     minSpeed       : 200,
	         |
	         |     // inertia will stop when the object slows down to this speed
	         |     endSpeed       : 20,
	         |
	         |     // boolean; should actions be resumed when the pointer goes down during inertia
	         |     allowResume    : true,
	         |
	         |     // boolean; should the jump when resuming from inertia be ignored in event.dx/dy
	         |     zeroResumeDelta: false,
	         |
	         |     // if snap/restrict are set to be endOnly and inertia is enabled, releasing
	         |     // the pointer without triggering inertia will animate from the release
	         |     // point to the snaped/restricted point in the given amount of time (ms)
	         |     smoothEndDuration: 300,
	         |
	         |     // an array of action types that can have inertia (no gesture)
	         |     actions        : ['drag', 'resize']
	         | });
	         |
	         | // reset custom settings and use all defaults
	         | interact(element).inertia(null);
	        \*/
	        inertia: function (options) {
	            var ret = this.setOptions('inertia', options);

	            if (ret === this) { return this; }

	            return ret.drag;
	        },

	        getAction: function (pointer, event, interaction, element) {
	            var action = this.defaultActionChecker(pointer, interaction, element);

	            if (this.options.actionChecker) {
	                return this.options.actionChecker(pointer, event, action, this, element, interaction);
	            }

	            return action;
	        },

	        defaultActionChecker: defaultActionChecker,

	        /*\
	         * Interactable.actionChecker
	         [ method ]
	         *
	         * Gets or sets the function used to check action to be performed on
	         * pointerDown
	         *
	         - checker (function | null) #optional A function which takes a pointer event, defaultAction string, interactable, element and interaction as parameters and returns an object with name property 'drag' 'resize' or 'gesture' and optionally an `edges` object with boolean 'top', 'left', 'bottom' and right props.
	         = (Function | Interactable) The checker function or this Interactable
	         *
	         | interact('.resize-drag')
	         |   .resizable(true)
	         |   .draggable(true)
	         |   .actionChecker(function (pointer, event, action, interactable, element, interaction) {
	         |
	         |   if (interact.matchesSelector(event.target, '.drag-handle') {
	         |     // force drag with handle target
	         |     action.name = drag;
	         |   }
	         |   else {
	         |     // resize from the top and right edges
	         |     action.name  = 'resize';
	         |     action.edges = { top: true, right: true };
	         |   }
	         |
	         |   return action;
	         | });
	        \*/
	        actionChecker: function (checker) {
	            if (isFunction(checker)) {
	                this.options.actionChecker = checker;

	                return this;
	            }

	            if (checker === null) {
	                delete this.options.actionChecker;

	                return this;
	            }

	            return this.options.actionChecker;
	        },

	        /*\
	         * Interactable.getRect
	         [ method ]
	         *
	         * The default function to get an Interactables bounding rect. Can be
	         * overridden using @Interactable.rectChecker.
	         *
	         - element (Element) #optional The element to measure.
	         = (object) The object's bounding rectangle.
	         o {
	         o     top   : 0,
	         o     left  : 0,
	         o     bottom: 0,
	         o     right : 0,
	         o     width : 0,
	         o     height: 0
	         o }
	        \*/
	        getRect: function rectCheck (element) {
	            element = element || this._element;

	            if (this.selector && !(isElement(element))) {
	                element = this._context.querySelector(this.selector);
	            }

	            return getElementRect(element);
	        },

	        /*\
	         * Interactable.rectChecker
	         [ method ]
	         *
	         * Returns or sets the function used to calculate the interactable's
	         * element's rectangle
	         *
	         - checker (function) #optional A function which returns this Interactable's bounding rectangle. See @Interactable.getRect
	         = (function | object) The checker function or this Interactable
	        \*/
	        rectChecker: function (checker) {
	            if (isFunction(checker)) {
	                this.getRect = checker;

	                return this;
	            }

	            if (checker === null) {
	                delete this.options.getRect;

	                return this;
	            }

	            return this.getRect;
	        },

	        /*\
	         * Interactable.styleCursor
	         [ method ]
	         *
	         * Returns or sets whether the action that would be performed when the
	         * mouse on the element are checked on `mousemove` so that the cursor
	         * may be styled appropriately
	         *
	         - newValue (boolean) #optional
	         = (boolean | Interactable) The current setting or this Interactable
	        \*/
	        styleCursor: function (newValue) {
	            if (isBool(newValue)) {
	                this.options.styleCursor = newValue;

	                return this;
	            }

	            if (newValue === null) {
	                delete this.options.styleCursor;

	                return this;
	            }

	            return this.options.styleCursor;
	        },

	        /*\
	         * Interactable.preventDefault
	         [ method ]
	         *
	         * Returns or sets whether to prevent the browser's default behaviour
	         * in response to pointer events. Can be set to:
	         *  - `'always'` to always prevent
	         *  - `'never'` to never prevent
	         *  - `'auto'` to let interact.js try to determine what would be best
	         *
	         - newValue (string) #optional `true`, `false` or `'auto'`
	         = (string | Interactable) The current setting or this Interactable
	        \*/
	        preventDefault: function (newValue) {
	            if (/^(always|never|auto)$/.test(newValue)) {
	                this.options.preventDefault = newValue;
	                return this;
	            }

	            if (isBool(newValue)) {
	                this.options.preventDefault = newValue? 'always' : 'never';
	                return this;
	            }

	            return this.options.preventDefault;
	        },

	        /*\
	         * Interactable.origin
	         [ method ]
	         *
	         * Gets or sets the origin of the Interactable's element.  The x and y
	         * of the origin will be subtracted from action event coordinates.
	         *
	         - origin (object | string) #optional An object eg. { x: 0, y: 0 } or string 'parent', 'self' or any CSS selector
	         * OR
	         - origin (Element) #optional An HTML or SVG Element whose rect will be used
	         **
	         = (object) The current origin or this Interactable
	        \*/
	        origin: function (newValue) {
	            if (trySelector(newValue)) {
	                this.options.origin = newValue;
	                return this;
	            }
	            else if (isObject(newValue)) {
	                this.options.origin = newValue;
	                return this;
	            }

	            return this.options.origin;
	        },

	        /*\
	         * Interactable.deltaSource
	         [ method ]
	         *
	         * Returns or sets the mouse coordinate types used to calculate the
	         * movement of the pointer.
	         *
	         - newValue (string) #optional Use 'client' if you will be scrolling while interacting; Use 'page' if you want autoScroll to work
	         = (string | object) The current deltaSource or this Interactable
	        \*/
	        deltaSource: function (newValue) {
	            if (newValue === 'page' || newValue === 'client') {
	                this.options.deltaSource = newValue;

	                return this;
	            }

	            return this.options.deltaSource;
	        },

	        /*\
	         * Interactable.restrict
	         [ method ]
	         **
	         * Deprecated. Add a `restrict` property to the options object passed to
	         * @Interactable.draggable, @Interactable.resizable or @Interactable.gesturable instead.
	         *
	         * Returns or sets the rectangles within which actions on this
	         * interactable (after snap calculations) are restricted. By default,
	         * restricting is relative to the pointer coordinates. You can change
	         * this by setting the
	         * [`elementRect`](https://github.com/taye/interact.js/pull/72).
	         **
	         - options (object) #optional an object with keys drag, resize, and/or gesture whose values are rects, Elements, CSS selectors, or 'parent' or 'self'
	         = (object) The current restrictions object or this Interactable
	         **
	         | interact(element).restrict({
	         |     // the rect will be `interact.getElementRect(element.parentNode)`
	         |     drag: element.parentNode,
	         |
	         |     // x and y are relative to the the interactable's origin
	         |     resize: { x: 100, y: 100, width: 200, height: 200 }
	         | })
	         |
	         | interact('.draggable').restrict({
	         |     // the rect will be the selected element's parent
	         |     drag: 'parent',
	         |
	         |     // do not restrict during normal movement.
	         |     // Instead, trigger only one restricted move event
	         |     // immediately before the end event.
	         |     endOnly: true,
	         |
	         |     // https://github.com/taye/interact.js/pull/72#issue-41813493
	         |     elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
	         | });
	        \*/
	        restrict: function (options) {
	            if (!isObject(options)) {
	                return this.setOptions('restrict', options);
	            }

	            var actions = ['drag', 'resize', 'gesture'],
	                ret;

	            for (var i = 0; i < actions.length; i++) {
	                var action = actions[i];

	                if (action in options) {
	                    var perAction = extend({
	                            actions: [action],
	                            restriction: options[action]
	                        }, options);

	                    ret = this.setOptions('restrict', perAction);
	                }
	            }

	            return ret;
	        },

	        /*\
	         * Interactable.context
	         [ method ]
	         *
	         * Gets the selector context Node of the Interactable. The default is `window.document`.
	         *
	         = (Node) The context Node of this Interactable
	         **
	        \*/
	        context: function () {
	            return this._context;
	        },

	        _context: document,

	        /*\
	         * Interactable.ignoreFrom
	         [ method ]
	         *
	         * If the target of the `mousedown`, `pointerdown` or `touchstart`
	         * event or any of it's parents match the given CSS selector or
	         * Element, no drag/resize/gesture is started.
	         *
	         - newValue (string | Element | null) #optional a CSS selector string, an Element or `null` to not ignore any elements
	         = (string | Element | object) The current ignoreFrom value or this Interactable
	         **
	         | interact(element, { ignoreFrom: document.getElementById('no-action') });
	         | // or
	         | interact(element).ignoreFrom('input, textarea, a');
	        \*/
	        ignoreFrom: function (newValue) {
	            if (trySelector(newValue)) {            // CSS selector to match event.target
	                this.options.ignoreFrom = newValue;
	                return this;
	            }

	            if (isElement(newValue)) {              // specific element
	                this.options.ignoreFrom = newValue;
	                return this;
	            }

	            return this.options.ignoreFrom;
	        },

	        /*\
	         * Interactable.allowFrom
	         [ method ]
	         *
	         * A drag/resize/gesture is started only If the target of the
	         * `mousedown`, `pointerdown` or `touchstart` event or any of it's
	         * parents match the given CSS selector or Element.
	         *
	         - newValue (string | Element | null) #optional a CSS selector string, an Element or `null` to allow from any element
	         = (string | Element | object) The current allowFrom value or this Interactable
	         **
	         | interact(element, { allowFrom: document.getElementById('drag-handle') });
	         | // or
	         | interact(element).allowFrom('.handle');
	        \*/
	        allowFrom: function (newValue) {
	            if (trySelector(newValue)) {            // CSS selector to match event.target
	                this.options.allowFrom = newValue;
	                return this;
	            }

	            if (isElement(newValue)) {              // specific element
	                this.options.allowFrom = newValue;
	                return this;
	            }

	            return this.options.allowFrom;
	        },

	        /*\
	         * Interactable.element
	         [ method ]
	         *
	         * If this is not a selector Interactable, it returns the element this
	         * interactable represents
	         *
	         = (Element) HTML / SVG Element
	        \*/
	        element: function () {
	            return this._element;
	        },

	        /*\
	         * Interactable.fire
	         [ method ]
	         *
	         * Calls listeners for the given InteractEvent type bound globally
	         * and directly to this Interactable
	         *
	         - iEvent (InteractEvent) The InteractEvent object to be fired on this Interactable
	         = (Interactable) this Interactable
	        \*/
	        fire: function (iEvent) {
	            if (!(iEvent && iEvent.type) || !contains(eventTypes, iEvent.type)) {
	                return this;
	            }

	            var listeners,
	                i,
	                len,
	                onEvent = 'on' + iEvent.type,
	                funcName = '';

	            // Interactable#on() listeners
	            if (iEvent.type in this._iEvents) {
	                listeners = this._iEvents[iEvent.type];

	                for (i = 0, len = listeners.length; i < len && !iEvent.immediatePropagationStopped; i++) {
	                    funcName = listeners[i].name;
	                    listeners[i](iEvent);
	                }
	            }

	            // interactable.onevent listener
	            if (isFunction(this[onEvent])) {
	                funcName = this[onEvent].name;
	                this[onEvent](iEvent);
	            }

	            // interact.on() listeners
	            if (iEvent.type in globalEvents && (listeners = globalEvents[iEvent.type]))  {

	                for (i = 0, len = listeners.length; i < len && !iEvent.immediatePropagationStopped; i++) {
	                    funcName = listeners[i].name;
	                    listeners[i](iEvent);
	                }
	            }

	            return this;
	        },

	        /*\
	         * Interactable.on
	         [ method ]
	         *
	         * Binds a listener for an InteractEvent or DOM event.
	         *
	         - eventType  (string | array | object) The types of events to listen for
	         - listener   (function) The function to be called on the given event(s)
	         - useCapture (boolean) #optional useCapture flag for addEventListener
	         = (object) This Interactable
	        \*/
	        on: function (eventType, listener, useCapture) {
	            var i;

	            if (isString(eventType) && eventType.search(' ') !== -1) {
	                eventType = eventType.trim().split(/ +/);
	            }

	            if (isArray(eventType)) {
	                for (i = 0; i < eventType.length; i++) {
	                    this.on(eventType[i], listener, useCapture);
	                }

	                return this;
	            }

	            if (isObject(eventType)) {
	                for (var prop in eventType) {
	                    this.on(prop, eventType[prop], listener);
	                }

	                return this;
	            }

	            if (eventType === 'wheel') {
	                eventType = wheelEvent;
	            }

	            // convert to boolean
	            useCapture = useCapture? true: false;

	            if (contains(eventTypes, eventType)) {
	                // if this type of event was never bound to this Interactable
	                if (!(eventType in this._iEvents)) {
	                    this._iEvents[eventType] = [listener];
	                }
	                else {
	                    this._iEvents[eventType].push(listener);
	                }
	            }
	            // delegated event for selector
	            else if (this.selector) {
	                if (!delegatedEvents[eventType]) {
	                    delegatedEvents[eventType] = {
	                        selectors: [],
	                        contexts : [],
	                        listeners: []
	                    };

	                    // add delegate listener functions
	                    for (i = 0; i < documents.length; i++) {
	                        events.add(documents[i], eventType, delegateListener);
	                        events.add(documents[i], eventType, delegateUseCapture, true);
	                    }
	                }

	                var delegated = delegatedEvents[eventType],
	                    index;

	                for (index = delegated.selectors.length - 1; index >= 0; index--) {
	                    if (delegated.selectors[index] === this.selector
	                        && delegated.contexts[index] === this._context) {
	                        break;
	                    }
	                }

	                if (index === -1) {
	                    index = delegated.selectors.length;

	                    delegated.selectors.push(this.selector);
	                    delegated.contexts .push(this._context);
	                    delegated.listeners.push([]);
	                }

	                // keep listener and useCapture flag
	                delegated.listeners[index].push([listener, useCapture]);
	            }
	            else {
	                events.add(this._element, eventType, listener, useCapture);
	            }

	            return this;
	        },

	        /*\
	         * Interactable.off
	         [ method ]
	         *
	         * Removes an InteractEvent or DOM event listener
	         *
	         - eventType  (string | array | object) The types of events that were listened for
	         - listener   (function) The listener function to be removed
	         - useCapture (boolean) #optional useCapture flag for removeEventListener
	         = (object) This Interactable
	        \*/
	        off: function (eventType, listener, useCapture) {
	            var i;

	            if (isString(eventType) && eventType.search(' ') !== -1) {
	                eventType = eventType.trim().split(/ +/);
	            }

	            if (isArray(eventType)) {
	                for (i = 0; i < eventType.length; i++) {
	                    this.off(eventType[i], listener, useCapture);
	                }

	                return this;
	            }

	            if (isObject(eventType)) {
	                for (var prop in eventType) {
	                    this.off(prop, eventType[prop], listener);
	                }

	                return this;
	            }

	            var eventList,
	                index = -1;

	            // convert to boolean
	            useCapture = useCapture? true: false;

	            if (eventType === 'wheel') {
	                eventType = wheelEvent;
	            }

	            // if it is an action event type
	            if (contains(eventTypes, eventType)) {
	                eventList = this._iEvents[eventType];

	                if (eventList && (index = indexOf(eventList, listener)) !== -1) {
	                    this._iEvents[eventType].splice(index, 1);
	                }
	            }
	            // delegated event
	            else if (this.selector) {
	                var delegated = delegatedEvents[eventType],
	                    matchFound = false;

	                if (!delegated) { return this; }

	                // count from last index of delegated to 0
	                for (index = delegated.selectors.length - 1; index >= 0; index--) {
	                    // look for matching selector and context Node
	                    if (delegated.selectors[index] === this.selector
	                        && delegated.contexts[index] === this._context) {

	                        var listeners = delegated.listeners[index];

	                        // each item of the listeners array is an array: [function, useCaptureFlag]
	                        for (i = listeners.length - 1; i >= 0; i--) {
	                            var fn = listeners[i][0],
	                                useCap = listeners[i][1];

	                            // check if the listener functions and useCapture flags match
	                            if (fn === listener && useCap === useCapture) {
	                                // remove the listener from the array of listeners
	                                listeners.splice(i, 1);

	                                // if all listeners for this interactable have been removed
	                                // remove the interactable from the delegated arrays
	                                if (!listeners.length) {
	                                    delegated.selectors.splice(index, 1);
	                                    delegated.contexts .splice(index, 1);
	                                    delegated.listeners.splice(index, 1);

	                                    // remove delegate function from context
	                                    events.remove(this._context, eventType, delegateListener);
	                                    events.remove(this._context, eventType, delegateUseCapture, true);

	                                    // remove the arrays if they are empty
	                                    if (!delegated.selectors.length) {
	                                        delegatedEvents[eventType] = null;
	                                    }
	                                }

	                                // only remove one listener
	                                matchFound = true;
	                                break;
	                            }
	                        }

	                        if (matchFound) { break; }
	                    }
	                }
	            }
	            // remove listener from this Interatable's element
	            else {
	                events.remove(this._element, eventType, listener, useCapture);
	            }

	            return this;
	        },

	        /*\
	         * Interactable.set
	         [ method ]
	         *
	         * Reset the options of this Interactable
	         - options (object) The new settings to apply
	         = (object) This Interactable
	        \*/
	        set: function (options) {
	            if (!isObject(options)) {
	                options = {};
	            }

	            this.options = extend({}, defaultOptions.base);

	            var i,
	                actions = ['drag', 'drop', 'resize', 'gesture'],
	                methods = ['draggable', 'dropzone', 'resizable', 'gesturable'],
	                perActions = extend(extend({}, defaultOptions.perAction), options[action] || {});

	            for (i = 0; i < actions.length; i++) {
	                var action = actions[i];

	                this.options[action] = extend({}, defaultOptions[action]);

	                this.setPerAction(action, perActions);

	                this[methods[i]](options[action]);
	            }

	            var settings = [
	                    'accept', 'actionChecker', 'allowFrom', 'deltaSource',
	                    'dropChecker', 'ignoreFrom', 'origin', 'preventDefault',
	                    'rectChecker', 'styleCursor'
	                ];

	            for (i = 0, len = settings.length; i < len; i++) {
	                var setting = settings[i];

	                this.options[setting] = defaultOptions.base[setting];

	                if (setting in options) {
	                    this[setting](options[setting]);
	                }
	            }

	            return this;
	        },

	        /*\
	         * Interactable.unset
	         [ method ]
	         *
	         * Remove this interactable from the list of interactables and remove
	         * it's drag, drop, resize and gesture capabilities
	         *
	         = (object) @interact
	        \*/
	        unset: function () {
	            events.remove(this._element, 'all');

	            if (!isString(this.selector)) {
	                events.remove(this, 'all');
	                if (this.options.styleCursor) {
	                    this._element.style.cursor = '';
	                }
	            }
	            else {
	                // remove delegated events
	                for (var type in delegatedEvents) {
	                    var delegated = delegatedEvents[type];

	                    for (var i = 0; i < delegated.selectors.length; i++) {
	                        if (delegated.selectors[i] === this.selector
	                            && delegated.contexts[i] === this._context) {

	                            delegated.selectors.splice(i, 1);
	                            delegated.contexts .splice(i, 1);
	                            delegated.listeners.splice(i, 1);

	                            // remove the arrays if they are empty
	                            if (!delegated.selectors.length) {
	                                delegatedEvents[type] = null;
	                            }
	                        }

	                        events.remove(this._context, type, delegateListener);
	                        events.remove(this._context, type, delegateUseCapture, true);

	                        break;
	                    }
	                }
	            }

	            this.dropzone(false);

	            interactables.splice(indexOf(interactables, this), 1);

	            return interact;
	        }
	    };

	    function warnOnce (method, message) {
	        var warned = false;

	        return function () {
	            if (!warned) {
	                window.console.warn(message);
	                warned = true;
	            }

	            return method.apply(this, arguments);
	        };
	    }

	    Interactable.prototype.snap = warnOnce(Interactable.prototype.snap,
	         'Interactable#snap is deprecated. See the new documentation for snapping at http://interactjs.io/docs/snapping');
	    Interactable.prototype.restrict = warnOnce(Interactable.prototype.restrict,
	         'Interactable#restrict is deprecated. See the new documentation for resticting at http://interactjs.io/docs/restriction');
	    Interactable.prototype.inertia = warnOnce(Interactable.prototype.inertia,
	         'Interactable#inertia is deprecated. See the new documentation for inertia at http://interactjs.io/docs/inertia');
	    Interactable.prototype.autoScroll = warnOnce(Interactable.prototype.autoScroll,
	         'Interactable#autoScroll is deprecated. See the new documentation for autoScroll at http://interactjs.io/docs/#autoscroll');
	    Interactable.prototype.squareResize = warnOnce(Interactable.prototype.squareResize,
	         'Interactable#squareResize is deprecated. See http://interactjs.io/docs/#resize-square');

	    Interactable.prototype.accept = warnOnce(Interactable.prototype.accept,
	         'Interactable#accept is deprecated. use Interactable#dropzone({ accept: target }) instead');
	    Interactable.prototype.dropChecker = warnOnce(Interactable.prototype.dropChecker,
	         'Interactable#dropChecker is deprecated. use Interactable#dropzone({ dropChecker: checkerFunction }) instead');
	    Interactable.prototype.context = warnOnce(Interactable.prototype.context,
	         'Interactable#context as a method is deprecated. It will soon be a DOM Node instead');

	    /*\
	     * interact.isSet
	     [ method ]
	     *
	     * Check if an element has been set
	     - element (Element) The Element being searched for
	     = (boolean) Indicates if the element or CSS selector was previously passed to interact
	    \*/
	    interact.isSet = function(element, options) {
	        return interactables.indexOfElement(element, options && options.context) !== -1;
	    };

	    /*\
	     * interact.on
	     [ method ]
	     *
	     * Adds a global listener for an InteractEvent or adds a DOM event to
	     * `document`
	     *
	     - type       (string | array | object) The types of events to listen for
	     - listener   (function) The function to be called on the given event(s)
	     - useCapture (boolean) #optional useCapture flag for addEventListener
	     = (object) interact
	    \*/
	    interact.on = function (type, listener, useCapture) {
	        if (isString(type) && type.search(' ') !== -1) {
	            type = type.trim().split(/ +/);
	        }

	        if (isArray(type)) {
	            for (var i = 0; i < type.length; i++) {
	                interact.on(type[i], listener, useCapture);
	            }

	            return interact;
	        }

	        if (isObject(type)) {
	            for (var prop in type) {
	                interact.on(prop, type[prop], listener);
	            }

	            return interact;
	        }

	        // if it is an InteractEvent type, add listener to globalEvents
	        if (contains(eventTypes, type)) {
	            // if this type of event was never bound
	            if (!globalEvents[type]) {
	                globalEvents[type] = [listener];
	            }
	            else {
	                globalEvents[type].push(listener);
	            }
	        }
	        // If non InteractEvent type, addEventListener to document
	        else {
	            events.add(document, type, listener, useCapture);
	        }

	        return interact;
	    };

	    /*\
	     * interact.off
	     [ method ]
	     *
	     * Removes a global InteractEvent listener or DOM event from `document`
	     *
	     - type       (string | array | object) The types of events that were listened for
	     - listener   (function) The listener function to be removed
	     - useCapture (boolean) #optional useCapture flag for removeEventListener
	     = (object) interact
	     \*/
	    interact.off = function (type, listener, useCapture) {
	        if (isString(type) && type.search(' ') !== -1) {
	            type = type.trim().split(/ +/);
	        }

	        if (isArray(type)) {
	            for (var i = 0; i < type.length; i++) {
	                interact.off(type[i], listener, useCapture);
	            }

	            return interact;
	        }

	        if (isObject(type)) {
	            for (var prop in type) {
	                interact.off(prop, type[prop], listener);
	            }

	            return interact;
	        }

	        if (!contains(eventTypes, type)) {
	            events.remove(document, type, listener, useCapture);
	        }
	        else {
	            var index;

	            if (type in globalEvents
	                && (index = indexOf(globalEvents[type], listener)) !== -1) {
	                globalEvents[type].splice(index, 1);
	            }
	        }

	        return interact;
	    };

	    /*\
	     * interact.enableDragging
	     [ method ]
	     *
	     * Deprecated.
	     *
	     * Returns or sets whether dragging is enabled for any Interactables
	     *
	     - newValue (boolean) #optional `true` to allow the action; `false` to disable action for all Interactables
	     = (boolean | object) The current setting or interact
	    \*/
	    interact.enableDragging = warnOnce(function (newValue) {
	        if (newValue !== null && newValue !== undefined) {
	            actionIsEnabled.drag = newValue;

	            return interact;
	        }
	        return actionIsEnabled.drag;
	    }, 'interact.enableDragging is deprecated and will soon be removed.');

	    /*\
	     * interact.enableResizing
	     [ method ]
	     *
	     * Deprecated.
	     *
	     * Returns or sets whether resizing is enabled for any Interactables
	     *
	     - newValue (boolean) #optional `true` to allow the action; `false` to disable action for all Interactables
	     = (boolean | object) The current setting or interact
	    \*/
	    interact.enableResizing = warnOnce(function (newValue) {
	        if (newValue !== null && newValue !== undefined) {
	            actionIsEnabled.resize = newValue;

	            return interact;
	        }
	        return actionIsEnabled.resize;
	    }, 'interact.enableResizing is deprecated and will soon be removed.');

	    /*\
	     * interact.enableGesturing
	     [ method ]
	     *
	     * Deprecated.
	     *
	     * Returns or sets whether gesturing is enabled for any Interactables
	     *
	     - newValue (boolean) #optional `true` to allow the action; `false` to disable action for all Interactables
	     = (boolean | object) The current setting or interact
	    \*/
	    interact.enableGesturing = warnOnce(function (newValue) {
	        if (newValue !== null && newValue !== undefined) {
	            actionIsEnabled.gesture = newValue;

	            return interact;
	        }
	        return actionIsEnabled.gesture;
	    }, 'interact.enableGesturing is deprecated and will soon be removed.');

	    interact.eventTypes = eventTypes;

	    /*\
	     * interact.debug
	     [ method ]
	     *
	     * Returns debugging data
	     = (object) An object with properties that outline the current state and expose internal functions and variables
	    \*/
	    interact.debug = function () {
	        var interaction = interactions[0] || new Interaction();

	        return {
	            interactions          : interactions,
	            target                : interaction.target,
	            dragging              : interaction.dragging,
	            resizing              : interaction.resizing,
	            gesturing             : interaction.gesturing,
	            prepared              : interaction.prepared,
	            matches               : interaction.matches,
	            matchElements         : interaction.matchElements,

	            prevCoords            : interaction.prevCoords,
	            startCoords           : interaction.startCoords,

	            pointerIds            : interaction.pointerIds,
	            pointers              : interaction.pointers,
	            addPointer            : listeners.addPointer,
	            removePointer         : listeners.removePointer,
	            recordPointer        : listeners.recordPointer,

	            snap                  : interaction.snapStatus,
	            restrict              : interaction.restrictStatus,
	            inertia               : interaction.inertiaStatus,

	            downTime              : interaction.downTimes[0],
	            downEvent             : interaction.downEvent,
	            downPointer           : interaction.downPointer,
	            prevEvent             : interaction.prevEvent,

	            Interactable          : Interactable,
	            interactables         : interactables,
	            pointerIsDown         : interaction.pointerIsDown,
	            defaultOptions        : defaultOptions,
	            defaultActionChecker  : defaultActionChecker,

	            actionCursors         : actionCursors,
	            dragMove              : listeners.dragMove,
	            resizeMove            : listeners.resizeMove,
	            gestureMove           : listeners.gestureMove,
	            pointerUp             : listeners.pointerUp,
	            pointerDown           : listeners.pointerDown,
	            pointerMove           : listeners.pointerMove,
	            pointerHover          : listeners.pointerHover,

	            eventTypes            : eventTypes,

	            events                : events,
	            globalEvents          : globalEvents,
	            delegatedEvents       : delegatedEvents,

	            prefixedPropREs       : prefixedPropREs
	        };
	    };

	    // expose the functions used to calculate multi-touch properties
	    interact.getPointerAverage = pointerAverage;
	    interact.getTouchBBox     = touchBBox;
	    interact.getTouchDistance = touchDistance;
	    interact.getTouchAngle    = touchAngle;

	    interact.getElementRect         = getElementRect;
	    interact.getElementClientRect   = getElementClientRect;
	    interact.matchesSelector        = matchesSelector;
	    interact.closest                = closest;

	    /*\
	     * interact.margin
	     [ method ]
	     *
	     * Deprecated. Use `interact(target).resizable({ margin: number });` instead.
	     * Returns or sets the margin for autocheck resizing used in
	     * @Interactable.getAction. That is the distance from the bottom and right
	     * edges of an element clicking in which will start resizing
	     *
	     - newValue (number) #optional
	     = (number | interact) The current margin value or interact
	    \*/
	    interact.margin = warnOnce(function (newvalue) {
	        if (isNumber(newvalue)) {
	            margin = newvalue;

	            return interact;
	        }
	        return margin;
	    },
	    'interact.margin is deprecated. Use interact(target).resizable({ margin: number }); instead.') ;

	    /*\
	     * interact.supportsTouch
	     [ method ]
	     *
	     = (boolean) Whether or not the browser supports touch input
	    \*/
	    interact.supportsTouch = function () {
	        return supportsTouch;
	    };

	    /*\
	     * interact.supportsPointerEvent
	     [ method ]
	     *
	     = (boolean) Whether or not the browser supports PointerEvents
	    \*/
	    interact.supportsPointerEvent = function () {
	        return supportsPointerEvent;
	    };

	    /*\
	     * interact.stop
	     [ method ]
	     *
	     * Cancels all interactions (end events are not fired)
	     *
	     - event (Event) An event on which to call preventDefault()
	     = (object) interact
	    \*/
	    interact.stop = function (event) {
	        for (var i = interactions.length - 1; i >= 0; i--) {
	            interactions[i].stop(event);
	        }

	        return interact;
	    };

	    /*\
	     * interact.dynamicDrop
	     [ method ]
	     *
	     * Returns or sets whether the dimensions of dropzone elements are
	     * calculated on every dragmove or only on dragstart for the default
	     * dropChecker
	     *
	     - newValue (boolean) #optional True to check on each move. False to check only before start
	     = (boolean | interact) The current setting or interact
	    \*/
	    interact.dynamicDrop = function (newValue) {
	        if (isBool(newValue)) {
	            //if (dragging && dynamicDrop !== newValue && !newValue) {
	                //calcRects(dropzones);
	            //}

	            dynamicDrop = newValue;

	            return interact;
	        }
	        return dynamicDrop;
	    };

	    /*\
	     * interact.pointerMoveTolerance
	     [ method ]
	     * Returns or sets the distance the pointer must be moved before an action
	     * sequence occurs. This also affects tolerance for tap events.
	     *
	     - newValue (number) #optional The movement from the start position must be greater than this value
	     = (number | Interactable) The current setting or interact
	    \*/
	    interact.pointerMoveTolerance = function (newValue) {
	        if (isNumber(newValue)) {
	            pointerMoveTolerance = newValue;

	            return this;
	        }

	        return pointerMoveTolerance;
	    };

	    /*\
	     * interact.maxInteractions
	     [ method ]
	     **
	     * Returns or sets the maximum number of concurrent interactions allowed.
	     * By default only 1 interaction is allowed at a time (for backwards
	     * compatibility). To allow multiple interactions on the same Interactables
	     * and elements, you need to enable it in the draggable, resizable and
	     * gesturable `'max'` and `'maxPerElement'` options.
	     **
	     - newValue (number) #optional Any number. newValue <= 0 means no interactions.
	    \*/
	    interact.maxInteractions = function (newValue) {
	        if (isNumber(newValue)) {
	            maxInteractions = newValue;

	            return this;
	        }

	        return maxInteractions;
	    };

	    interact.createSnapGrid = function (grid) {
	        return function (x, y) {
	            var offsetX = 0,
	                offsetY = 0;

	            if (isObject(grid.offset)) {
	                offsetX = grid.offset.x;
	                offsetY = grid.offset.y;
	            }

	            var gridx = Math.round((x - offsetX) / grid.x),
	                gridy = Math.round((y - offsetY) / grid.y),

	                newX = gridx * grid.x + offsetX,
	                newY = gridy * grid.y + offsetY;

	            return {
	                x: newX,
	                y: newY,
	                range: grid.range
	            };
	        };
	    };

	    function endAllInteractions (event) {
	        for (var i = 0; i < interactions.length; i++) {
	            interactions[i].pointerEnd(event, event);
	        }
	    }

	    function listenToDocument (doc) {
	        if (contains(documents, doc)) { return; }

	        var win = doc.defaultView || doc.parentWindow;

	        // add delegate event listener
	        for (var eventType in delegatedEvents) {
	            events.add(doc, eventType, delegateListener);
	            events.add(doc, eventType, delegateUseCapture, true);
	        }

	        if (PointerEvent) {
	            if (PointerEvent === win.MSPointerEvent) {
	                pEventTypes = {
	                    up: 'MSPointerUp', down: 'MSPointerDown', over: 'mouseover',
	                    out: 'mouseout', move: 'MSPointerMove', cancel: 'MSPointerCancel' };
	            }
	            else {
	                pEventTypes = {
	                    up: 'pointerup', down: 'pointerdown', over: 'pointerover',
	                    out: 'pointerout', move: 'pointermove', cancel: 'pointercancel' };
	            }

	            events.add(doc, pEventTypes.down  , listeners.selectorDown );
	            events.add(doc, pEventTypes.move  , listeners.pointerMove  );
	            events.add(doc, pEventTypes.over  , listeners.pointerOver  );
	            events.add(doc, pEventTypes.out   , listeners.pointerOut   );
	            events.add(doc, pEventTypes.up    , listeners.pointerUp    );
	            events.add(doc, pEventTypes.cancel, listeners.pointerCancel);

	            // autoscroll
	            events.add(doc, pEventTypes.move, listeners.autoScrollMove);
	        }
	        else {
	            events.add(doc, 'mousedown', listeners.selectorDown);
	            events.add(doc, 'mousemove', listeners.pointerMove );
	            events.add(doc, 'mouseup'  , listeners.pointerUp   );
	            events.add(doc, 'mouseover', listeners.pointerOver );
	            events.add(doc, 'mouseout' , listeners.pointerOut  );

	            events.add(doc, 'touchstart' , listeners.selectorDown );
	            events.add(doc, 'touchmove'  , listeners.pointerMove  );
	            events.add(doc, 'touchend'   , listeners.pointerUp    );
	            events.add(doc, 'touchcancel', listeners.pointerCancel);

	            // autoscroll
	            events.add(doc, 'mousemove', listeners.autoScrollMove);
	            events.add(doc, 'touchmove', listeners.autoScrollMove);
	        }

	        events.add(win, 'blur', endAllInteractions);

	        try {
	            if (win.frameElement) {
	                var parentDoc = win.frameElement.ownerDocument,
	                    parentWindow = parentDoc.defaultView;

	                events.add(parentDoc   , 'mouseup'      , listeners.pointerEnd);
	                events.add(parentDoc   , 'touchend'     , listeners.pointerEnd);
	                events.add(parentDoc   , 'touchcancel'  , listeners.pointerEnd);
	                events.add(parentDoc   , 'pointerup'    , listeners.pointerEnd);
	                events.add(parentDoc   , 'MSPointerUp'  , listeners.pointerEnd);
	                events.add(parentWindow, 'blur'         , endAllInteractions );
	            }
	        }
	        catch (error) {
	            interact.windowParentError = error;
	        }

	        // prevent native HTML5 drag on interact.js target elements
	        events.add(doc, 'dragstart', function (event) {
	            for (var i = 0; i < interactions.length; i++) {
	                var interaction = interactions[i];

	                if (interaction.element
	                    && (interaction.element === event.target
	                        || nodeContains(interaction.element, event.target))) {

	                    interaction.checkAndPreventDefault(event, interaction.target, interaction.element);
	                    return;
	                }
	            }
	        });

	        if (events.useAttachEvent) {
	            // For IE's lack of Event#preventDefault
	            events.add(doc, 'selectstart', function (event) {
	                var interaction = interactions[0];

	                if (interaction.currentAction()) {
	                    interaction.checkAndPreventDefault(event);
	                }
	            });

	            // For IE's bad dblclick event sequence
	            events.add(doc, 'dblclick', doOnInteractions('ie8Dblclick'));
	        }

	        documents.push(doc);
	    }

	    listenToDocument(document);

	    function indexOf (array, target) {
	        for (var i = 0, len = array.length; i < len; i++) {
	            if (array[i] === target) {
	                return i;
	            }
	        }

	        return -1;
	    }

	    function contains (array, target) {
	        return indexOf(array, target) !== -1;
	    }

	    function matchesSelector (element, selector, nodeList) {
	        if (ie8MatchesSelector) {
	            return ie8MatchesSelector(element, selector, nodeList);
	        }

	        // remove /deep/ from selectors if shadowDOM polyfill is used
	        if (window !== realWindow) {
	            selector = selector.replace(/\/deep\//g, ' ');
	        }

	        return element[prefixedMatchesSelector](selector);
	    }

	    function matchesUpTo (element, selector, limit) {
	        while (isElement(element)) {
	            if (matchesSelector(element, selector)) {
	                return true;
	            }

	            element = parentElement(element);

	            if (element === limit) {
	                return matchesSelector(element, selector);
	            }
	        }

	        return false;
	    }

	    // For IE8's lack of an Element#matchesSelector
	    // taken from http://tanalin.com/en/blog/2012/12/matches-selector-ie8/ and modified
	    if (!(prefixedMatchesSelector in Element.prototype) || !isFunction(Element.prototype[prefixedMatchesSelector])) {
	        ie8MatchesSelector = function (element, selector, elems) {
	            elems = elems || element.parentNode.querySelectorAll(selector);

	            for (var i = 0, len = elems.length; i < len; i++) {
	                if (elems[i] === element) {
	                    return true;
	                }
	            }

	            return false;
	        };
	    }

	    // requestAnimationFrame polyfill
	    (function() {
	        var lastTime = 0,
	            vendors = ['ms', 'moz', 'webkit', 'o'];

	        for(var x = 0; x < vendors.length && !realWindow.requestAnimationFrame; ++x) {
	            reqFrame = realWindow[vendors[x]+'RequestAnimationFrame'];
	            cancelFrame = realWindow[vendors[x]+'CancelAnimationFrame'] || realWindow[vendors[x]+'CancelRequestAnimationFrame'];
	        }

	        if (!reqFrame) {
	            reqFrame = function(callback) {
	                var currTime = new Date().getTime(),
	                    timeToCall = Math.max(0, 16 - (currTime - lastTime)),
	                    id = setTimeout(function() { callback(currTime + timeToCall); },
	                  timeToCall);
	                lastTime = currTime + timeToCall;
	                return id;
	            };
	        }

	        if (!cancelFrame) {
	            cancelFrame = function(id) {
	                clearTimeout(id);
	            };
	        }
	    }());

	    /* global exports: true, module, define */

	    // http://documentcloud.github.io/underscore/docs/underscore.html#section-11
	    if (true) {
	        if (typeof module !== 'undefined' && module.exports) {
	            exports = module.exports = interact;
	        }
	        exports.interact = interact;
	    }
	    // AMD
	    else if (typeof define === 'function' && define.amd) {
	        define('interact', function() {
	            return interact;
	        });
	    }
	    else {
	        realWindow.interact = interact;
	    }

	} (typeof window === 'undefined'? undefined : window));


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
	 * THIS FILE IS AUTO GENERATED FROM 'lib/parse.kep'
	 * DO NOT EDIT
	*/
	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(10), __webpack_require__(11)], __WEBPACK_AMD_DEFINE_RESULT__ = function(require, exports, stream, seshat) {
	    "use strict";
	    var Tail, trampoline, ParserError, ParseError, MultipleError, UnknownError, UnexpectError, ExpectError,
	            ParserState, Position, Parser, label, late, rec, unparser, always, of, never, bind, chain, map, ap,
	            extract, getParserState, setParserState, modifyParserState, getState, setState, modifyState,
	            getInput, setInput, getPosition, setPosition, fail, attempt, look, lookahead, next, sequences,
	            sequencea, sequence, empty, either, concat, choices, choicea, choice, optional, expected, not,
	            eager, binds, cons, append, enumerations, enumerationa, enumeration, many, many1, manyTill, memo,
	            token, anyToken, eof, exec, parseState, parseStream, parse, runState, runStream, run, testState,
	            testStream, test, NIL = stream["NIL"],
	        first = stream["first"],
	        isEmpty = stream["isEmpty"],
	        rest = stream["rest"],
	        reduceRight = stream["reduceRight"],
	        foldr = stream["foldr"],
	        identity = (function(x) {
	            return x;
	        }),
	        args = (function() {
	            var args0 = arguments;
	            return args0;
	        });
	    (Tail = (function(p, state, m, cok, cerr, eok, eerr) {
	        var self = this;
	        (self.p = p);
	        (self.state = state);
	        (self.m = m);
	        (self.cok = cok);
	        (self.cerr = cerr);
	        (self.eok = eok);
	        (self.eerr = eerr);
	    }));
	    (trampoline = (function(f) {
	        var value = f;
	        while ((value instanceof Tail)) {
	            (value = value.p(value.state, value.m, value.cok, value.cerr, value.eok, value.eerr));
	        }
	        return value;
	    }));
	    var Memoer = (function(memoer, frames) {
	        var self = this;
	        (self.memoer = memoer);
	        (self.frames = frames);
	    });
	    (Memoer.empty = new(Memoer)(seshat.create((function(x, y) {
	        return x.compare(y);
	    }), (function(x, y) {
	        return ((x.id === y.id) && ((x.state === y.state) || (x.state && x.state.eq(y.state))));
	    })), NIL));
	    (Memoer.pushWindow = (function(m, lower) {
	        return new(Memoer)(m.memoer, stream.cons(lower, m.frames));
	    }));
	    (Memoer.popWindow = (function(m) {
	        var frames = m["frames"],
	            r = rest(frames);
	        return new(Memoer)((isEmpty(r) ? seshat.prune(m.memoer, first(frames)) : m.memoer), r);
	    }));
	    (Memoer.prune = (function(m, position) {
	        return (isEmpty(m.frames) ? new(Memoer)(seshat.prune(m.memoer, position), m.frames) : m);
	    }));
	    (Memoer.lookup = (function(m, pos, id) {
	        return seshat.lookup(m.memoer, pos, id);
	    }));
	    (Memoer.update = (function(m, pos, id, val) {
	        return new(Memoer)(seshat.update(m.memoer, pos, id, val), m.frames);
	    }));
	    (Position = (function(i) {
	        var self = this;
	        (self.index = i);
	    }));
	    (Position.initial = new(Position)(0));
	    (Position.prototype.toString = (function() {
	        var self = this;
	        return ("" + self.index);
	    }));
	    (Position.prototype.increment = (function(_, _0) {
	        var self = this;
	        return new(Position)((self.index + 1));
	    }));
	    (Position.prototype.compare = (function(pos) {
	        var self = this;
	        return (self.index - pos.index);
	    }));
	    (ParserState = (function(input, position, userState) {
	        var self = this;
	        (self.input = input);
	        (self.position = position);
	        (self.userState = userState);
	    }));
	    (ParserState.prototype.eq = (function(other) {
	        var self = this;
	        return ((other && (self.input === other.input)) && (self.userState === other.userState));
	    }));
	    (ParserState.prototype.isEmpty = (function() {
	        var self = this;
	        return isEmpty(self.input);
	    }));
	    (ParserState.prototype.first = (function() {
	        var self = this;
	        return first(self.input);
	    }));
	    (ParserState.prototype.next = (function(tok) {
	        var self = this;
	        if ((!self._next)) {
	            var r = rest(self.input),
	                s = new(ParserState)(r, self.position.increment(tok, r), self.userState);
	            (self._next = new(Parser)((function(_, m, cok) {
	                return cok(tok, s, m);
	            })));
	        }
	        return self._next;
	    }));
	    (ParserState.prototype.setInput = (function(input) {
	        var self = this;
	        return new(ParserState)(input, self.position, self.userState);
	    }));
	    (ParserState.prototype.setPosition = (function(position) {
	        var self = this;
	        return new(ParserState)(self.input, position, self.userState);
	    }));
	    (ParserState.prototype.setUserState = (function(userState) {
	        var self = this;
	        return new(ParserState)(self.input, self.position, userState);
	    }));
	    (ParserError = (function(msg) {
	        var self = this;
	        (self.message = msg);
	    }));
	    (ParserError.prototype = new(Error)());
	    (ParserError.prototype.constructor = ParserError);
	    (ParserError.prototype.name = "ParserError");
	    (ParseError = (function(position, msg) {
	        var self = this;
	        (self.position = position);
	        (self._msg = (msg || ""));
	    }));
	    (ParseError.prototype = new(Error)());
	    (ParseError.prototype.constructor = ParseError);
	    (ParseError.prototype.name = "ParseError");
	    (ParseError.prototype.toString = (function() {
	        var self = this;
	        return self.message;
	    }));
	    Object.defineProperties(ParseError.prototype, ({
	        message: ({
	            configurable: true,
	            get: (function() {
	                var self = this;
	                return ((("At " + self.position) + " ") + self.errorMessage);
	            })
	        }),
	        errorMessage: ({
	            configurable: true,
	            get: (function() {
	                var self = this;
	                return self._msg;
	            })
	        })
	    }));
	    (MultipleError = (function(position, errors) {
	        var self = this;
	        (self.position = position);
	        (self.errors = (errors || []));
	    }));
	    (MultipleError.prototype = new(ParseError)());
	    (MultipleError.prototype.constructor = MultipleError);
	    (MultipleError.prototype.name = "MultipleError");
	    Object.defineProperty(MultipleError.prototype, "errorMessage", ({
	        get: (function() {
	            var self = this;
	            return (("[" + self.errors.map((function(x) {
	                    return x.message;
	                }))
	                .join(", ")) + "]");
	        })
	    }));
	    var ChoiceError = (function(position, pErr, qErr) {
	        var self = this;
	        (self.position = position);
	        (self._pErr = pErr);
	        (self._qErr = qErr);
	    });
	    (ChoiceError.prototype = new(MultipleError)());
	    (ChoiceError.prototype.constructor = MultipleError);
	    (ChoiceError.prototype.name = "ChoiceError");
	    Object.defineProperty(ChoiceError.prototype, "errors", ({
	        get: (function() {
	            var self = this;
	            return [self._pErr].concat(self._qErr.errors);
	        })
	    }));
	    (UnknownError = (function(position) {
	        var self = this;
	        (self.position = position);
	    }));
	    (UnknownError.prototype = new(ParseError)());
	    (UnknownError.prototype.constructor = UnknownError);
	    (UnknownError.prototype.name = "UnknownError");
	    Object.defineProperty(UnknownError.prototype, "errorMessage", ({
	        value: "unknown error"
	    }));
	    (UnexpectError = (function(position, unexpected) {
	        var self = this;
	        (self.position = position);
	        (self.unexpected = unexpected);
	    }));
	    (UnexpectError.prototype = new(ParseError)());
	    (UnexpectError.prototype.constructor = UnexpectError);
	    (UnexpectError.prototype.name = "UnexpectError");
	    Object.defineProperty(UnexpectError.prototype, "errorMessage", ({
	        get: (function() {
	            var self = this;
	            return ("unexpected: " + self.unexpected);
	        })
	    }));
	    (ExpectError = (function(position, expected, found) {
	        var self = this;
	        (self.position = position);
	        (self.expected = expected);
	        (self.found = found);
	    }));
	    (ExpectError.prototype = new(ParseError)());
	    (ExpectError.prototype.constructor = ExpectError);
	    (ExpectError.prototype.name = "ExpectError");
	    Object.defineProperty(ExpectError.prototype, "errorMessage", ({
	        get: (function() {
	            var self = this;
	            return (("expected: " + self.expected) + (self.found ? (" found: " + self.found) : ""));
	        })
	    }));
	    (Parser = (function(n) {
	        var self = this;
	        (self.run = n);
	    }));
	    (unparser = (function(p, state, m, cok, cerr, eok, eerr) {
	        return new(Tail)(p.run, state, m, cok, cerr, eok, eerr);
	    }));
	    (label = (function(name, p) {
	        return (p.run.hasOwnProperty("displayName") ? label(name, new(Parser)((function(state, m, cok,
	            cerr, eok, eerr) {
	            return new(Tail)(p.run, state, m, cok, cerr, eok, eerr);
	        }))) : new(Parser)(Object.defineProperty(p.run, "displayName", ({
	            value: name,
	            writable: false
	        }))));
	    }));
	    (late = (function(def) {
	        var value;
	        return new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	            (value = (value || def()));
	            var p = value;
	            return new(Tail)(p.run, state, m, cok, cerr, eok, eerr);
	        }));
	    }));
	    (rec = (function(def) {
	        var value = def(late((function() {
	            return value;
	        })));
	        return value;
	    }));
	    (Parser.prototype.of = (function(x) {
	        return new(Parser)((function(state, m, _, _0, eok, _1) {
	            return eok(x, state, m);
	        }));
	    }));
	    (Parser.of = Parser.prototype.of);
	    (of = Parser.of);
	    (always = of);
	    (never = (function(x) {
	        return new(Parser)((function(state, m, _, _0, _1, eerr) {
	            return eerr(x, state, m);
	        }));
	    }));
	    (Parser.chain = (function(p, f) {
	        return new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	            var cok0 = (function(x, state0, m0) {
	                var p0 = f(x);
	                return new(Tail)(p0.run, state0, m0, cok, cerr, cok, cerr);
	            }),
	                eok0 = (function(x, state0, m0) {
	                    var p0 = f(x);
	                    return new(Tail)(p0.run, state0, m0, cok, cerr, eok, eerr);
	                });
	            return new(Tail)(p.run, state, m, cok0, cerr, eok0, eerr);
	        }));
	    }));
	    (chain = Parser.chain);
	    (bind = chain);
	    (Parser.prototype.chain = (function(f) {
	        var self = this;
	        return chain(self, f);
	    }));
	    (Parser.prototype.map = (function(f) {
	        var self = this;
	        return chain(self, (function(z) {
	            return of(f(z));
	        }));
	    }));
	    (Parser.map = (function(f, p) {
	        return p.map(f);
	    }));
	    (map = Parser.map);
	    (Parser.ap = (function(f, m) {
	        return chain(f, (function(f0) {
	            return m.map(f0);
	        }));
	    }));
	    (ap = Parser.ap);
	    (Parser.prototype.ap = (function(m2) {
	        var self = this;
	        return ap(self, m2);
	    }));
	    (modifyParserState = (function(f) {
	        return new(Parser)((function(state, m, _, _0, eok, _1) {
	            var newState = f(state);
	            return eok(newState, newState, m);
	        }));
	    }));
	    var p = new(Parser)((function(state, m, _, _0, eok, _1) {
	        return eok(state, state, m);
	    }));
	    (getParserState = (p.run.hasOwnProperty("displayName") ? label("Get Parser State", new(Parser)((function(
	        state, m, cok, cerr, eok, eerr) {
	        return new(Tail)(p.run, state, m, cok, cerr, eok, eerr);
	    }))) : new(Parser)(Object.defineProperty(p.run, "displayName", ({
	        value: "Get Parser State",
	        writable: false
	    })))));
	    (setParserState = (function(z) {
	        return new(Parser)((function(state, m, _, _0, eok, _1) {
	            return eok(z, z, m);
	        }));
	    }));
	    (extract = (function(f) {
	        return new(Parser)((function(state, m, _, _0, eok, _1) {
	            return eok(f(state), state, m);
	        }));
	    }));
	    (modifyState = (function(f) {
	        return new(Parser)((function(state, m, _, _0, eok, _1) {
	            var newState = state.setUserState(f(state.userState));
	            return eok(newState, newState, m);
	        }));
	    }));
	    var p0 = new(Parser)((function(state, m, _, _0, eok, _1) {
	        return eok(state.userState, state, m);
	    }));
	    (getState = (p0.run.hasOwnProperty("displayName") ? label("Get State", new(Parser)((function(state, m, cok,
	        cerr, eok, eerr) {
	        return new(Tail)(p0.run, state, m, cok, cerr, eok, eerr);
	    }))) : new(Parser)(Object.defineProperty(p0.run, "displayName", ({
	        value: "Get State",
	        writable: false
	    })))));
	    (setState = (function(z) {
	        return new(Parser)((function(state, m, _, _0, eok, _1) {
	            var newState = state.setUserState(z);
	            return eok(newState, newState, m);
	        }));
	    }));
	    var p1 = new(Parser)((function(state, m, _, _0, eok, _1) {
	        return eok(state.position, state, m);
	    }));
	    (getPosition = (p1.run.hasOwnProperty("displayName") ? label("Get Position", new(Parser)((function(state, m,
	        cok, cerr, eok, eerr) {
	        return new(Tail)(p1.run, state, m, cok, cerr, eok, eerr);
	    }))) : new(Parser)(Object.defineProperty(p1.run, "displayName", ({
	        value: "Get Position",
	        writable: false
	    })))));
	    (setPosition = (function(position) {
	        return new(Parser)((function(state, m, _, _0, eok, _1) {
	            var newState = state.setPosition(position);
	            return eok(newState, newState, m);
	        }));
	    }));
	    var p2 = new(Parser)((function(state, m, _, _0, eok, _1) {
	        return eok(state.input, state, m);
	    }));
	    (getInput = (p2.run.hasOwnProperty("displayName") ? label("Get Input", new(Parser)((function(state, m, cok,
	        cerr, eok, eerr) {
	        return new(Tail)(p2.run, state, m, cok, cerr, eok, eerr);
	    }))) : new(Parser)(Object.defineProperty(p2.run, "displayName", ({
	        value: "Get Input",
	        writable: false
	    })))));
	    (setInput = (function(input) {
	        return new(Parser)((function(state, m, _, _0, eok, _1) {
	            var newState = state.setInput(input);
	            return eok(newState, newState, m);
	        }));
	    }));
	    (fail = (function(msg) {
	        var e = (msg ? ParseError : UnknownError);
	        return chain(getPosition, (function(z) {
	            var x = new(e)(z, msg);
	            return new(Parser)((function(state, m, _, _0, _1, eerr) {
	                return eerr(x, state, m);
	            }));
	        }));
	    }));
	    (attempt = (function(p3) {
	        return new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	            var peerr = (function(x, s, m0) {
	                return eerr(x, s, Memoer.popWindow(m0));
	            }),
	                m0 = Memoer.pushWindow(m, state.position),
	                cok0 = (function(x, s, m1) {
	                    return cok(x, s, Memoer.popWindow(m1));
	                }),
	                eok0 = (function(x, s, m1) {
	                    return eok(x, s, Memoer.popWindow(m1));
	                });
	            return new(Tail)(p3.run, state, m0, cok0, peerr, eok0, peerr);
	        }));
	    }));
	    (look = (function(p3) {
	        return chain(getParserState, (function(v1) {
	            return chain(p3, (function(v2) {
	                return next(setParserState(v1), of(v2));
	            }));
	        }));
	    }));
	    (lookahead = (function(p3) {
	        return chain(getInput, (function(v1) {
	            return chain(getPosition, (function(v2) {
	                return chain(p3, (function(x) {
	                    return sequence(new(Parser)((function(state, m, _, _0, eok,
	                        _1) {
	                        var newState = state.setPosition(v2);
	                        return eok(newState, newState, m);
	                    })), setInput(v1), of(x));
	                }));
	            }));
	        }));
	    }));
	    (next = (function(p3, q) {
	        return chain(p3, (function() {
	            return q;
	        }));
	    }));
	    (sequences = reduceRight.bind(null, (function(x, y) {
	        return chain(y, (function() {
	            return x;
	        }));
	    })));
	    var x = stream.from;
	    (sequencea = (function(z) {
	        return sequences(x(z));
	    }));
	    (sequence = (function() {
	        var args0 = arguments;
	        return sequencea(args.apply(null, args0));
	    }));
	    var e = (undefined ? ParseError : UnknownError);
	    (Parser.prototype.empty = chain(getPosition, (function(z) {
	        var x0 = new(e)(z, undefined);
	        return new(Parser)((function(state, m, _, _0, _1, eerr) {
	            return eerr(x0, state, m);
	        }));
	    })));
	    (Parser.empty = Parser.prototype.empty);
	    (empty = Parser.empty);
	    (Parser.concat = (function(p3, q) {
	        return new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	            var position = state["position"],
	                peerr = (function(errFromP, _, mFromP) {
	                    var qeerr = (function(errFromQ, _0, mFromQ) {
	                        return eerr(new(MultipleError)(position, [errFromP, errFromQ]),
	                            state, mFromQ);
	                    });
	                    return new(Tail)(q.run, state, mFromP, cok, cerr, eok, qeerr);
	                });
	            return new(Tail)(p3.run, state, m, cok, cerr, eok, peerr);
	        }));
	    }));
	    (concat = Parser.concat);
	    (either = concat);
	    (Parser.prototype.concat = (function(p3) {
	        var self = this;
	        return concat(self, p3);
	    }));
	    var x0;
	    (choices = foldr.bind(null, (function(x0, y) {
	        return new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	            var position = state["position"],
	                peerr = (function(errFromP, _, mFromP) {
	                    var qeerr = (function(errFromQ, _0, mFromQ) {
	                        return eerr(new(ChoiceError)(position, errFromP, errFromQ),
	                            state, mFromQ);
	                    });
	                    return new(Tail)(x0.run, state, mFromP, cok, cerr, eok, qeerr);
	                });
	            return new(Tail)(y.run, state, m, cok, cerr, eok, peerr);
	        }));
	    }), ((x0 = new(MultipleError)(null, [])), new(Parser)((function(state, m, _, _0, _1, eerr) {
	        return eerr(x0, state, m);
	    })))));
	    var x1 = stream.from;
	    (choicea = (function(z) {
	        return choices(x1(z));
	    }));
	    (choice = (function() {
	        var args0 = arguments;
	        return choicea(args.apply(null, args0));
	    }));
	    (optional = (function(x2, p3) {
	        return (p3 ? concat(p3, of(x2)) : concat(x2, of(null)));
	    }));
	    (expected = (function(expect, p3) {
	        return new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	            var eerr0 = (function(x2, state0, m0) {
	                return eerr(new(ExpectError)(state0.position, expect), state0, m0);
	            });
	            return new(Tail)(p3.run, state, m, cok, cerr, eok, eerr0);
	        }));
	    }));
	    (not = (function(p3) {
	        var p4 = concat(chain(new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	            var peerr = (function(x2, s, m0) {
	                return eerr(x2, s, Memoer.popWindow(m0));
	            }),
	                m0 = Memoer.pushWindow(m, state.position),
	                cok0 = (function(x2, s, m1) {
	                    return cok(x2, s, Memoer.popWindow(m1));
	                }),
	                eok0 = (function(x2, s, m1) {
	                    return eok(x2, s, Memoer.popWindow(m1));
	                });
	            return new(Tail)(p3.run, state, m0, cok0, peerr, eok0, peerr);
	        })), (function(x2) {
	            return chain(getPosition, (function(z) {
	                var x3 = new(UnexpectError)(z, x2);
	                return new(Parser)((function(state, m, _, _0, _1, eerr) {
	                    return eerr(x3, state, m);
	                }));
	            }));
	        })), of(null));
	        return new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	            var peerr = (function(x2, s, m0) {
	                return eerr(x2, s, Memoer.popWindow(m0));
	            }),
	                m0 = Memoer.pushWindow(m, state.position),
	                cok0 = (function(x2, s, m1) {
	                    return cok(x2, s, Memoer.popWindow(m1));
	                }),
	                eok0 = (function(x2, s, m1) {
	                    return eok(x2, s, Memoer.popWindow(m1));
	                });
	            return new(Tail)(p4.run, state, m0, cok0, peerr, eok0, peerr);
	        }));
	    }));
	    (eager = map.bind(null, stream.toArray));
	    (binds = (function(p3, f) {
	        return chain(eager(p3), (function(x2) {
	            return f.apply(undefined, x2);
	        }));
	    }));
	    var f = stream.cons;
	    (cons = (function(p10, p20) {
	        return chain(p10, (function(x2) {
	            return map((function(y) {
	                return f(x2, y);
	            }), p20);
	        }));
	    }));
	    var f0 = stream.append;
	    (append = (function(p10, p20) {
	        return chain(p10, (function(x2) {
	            return map((function(y) {
	                return f0(x2, y);
	            }), p20);
	        }));
	    }));
	    (enumerations = foldr.bind(null, (function(x2, y) {
	        return cons(y, x2);
	    }), of(NIL)));
	    var x2 = stream.from;
	    (enumerationa = (function(z) {
	        return enumerations(x2(z));
	    }));
	    (enumeration = (function() {
	        var args0 = arguments;
	        return enumerationa(args.apply(null, args0));
	    }));
	    var err = new(ParserError)("Many parser applied to parser that accepts an empty string"),
	        manyError = (function() {
	            throw err;
	        });
	    (many = (function(p3) {
	        var safeP = new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	            return new(Tail)(p3.run, state, m, cok, cerr, manyError, eerr);
	        }));
	        return rec((function(self) {
	            var p4 = cons(safeP, self);
	            return (p4 ? concat(p4, of(NIL)) : concat(NIL, of(null)));
	        }));
	    }));
	    (many1 = (function(p3) {
	        return cons(p3, many(p3));
	    }));
	    (manyTill = (function(p3, end) {
	        return rec((function(self) {
	            var p4, p5;
	            return concat(((p4 = chain(getInput, (function(v1) {
	                    return chain(getPosition, (function(v2) {
	                        return chain(end, (function(x3) {
	                            return sequence(new(Parser)((
	                                function(state, m, _,
	                                    _0, eok, _1) {
	                                    var newState =
	                                        state.setPosition(
	                                            v2);
	                                    return eok(newState,
	                                        newState, m);
	                                })), setInput(v1), of(x3));
	                        }));
	                    }));
	                }))), new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	                    var peerr = (function(x3, s, m0) {
	                        return eerr(x3, s, Memoer.popWindow(m0));
	                    }),
	                        m0 = Memoer.pushWindow(m, state.position),
	                        cok0 = (function(x3, s, m1) {
	                            return cok(x3, s, Memoer.popWindow(m1));
	                        }),
	                        eok0 = (function(x3, s, m1) {
	                            return eok(x3, s, Memoer.popWindow(m1));
	                        });
	                    return new(Tail)(p4.run, state, m0, cok0, peerr, eok0, peerr);
	                })))
	                .map((function(_) {
	                    return NIL;
	                })), ((p5 = cons(p3, self)), (p5 ? concat(p5, of(NIL)) : concat(NIL, of(null))))
	            );
	        }));
	    }));
	    (memo = (function(p3) {
	        return new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	            var position = state["position"],
	                key = ({
	                    id: p3,
	                    state: state
	                }),
	                entry = Memoer.lookup(m, position, key);
	            if (entry) {
	                var type = entry[0],
	                    x3 = entry[1],
	                    s = entry[2];
	                switch (type) {
	                    case "cok":
	                        return cok(x3, s, m);
	                    case "ceerr":
	                        return cerr(x3, s, m);
	                    case "eok":
	                        return eok(x3, s, m);
	                    case "eerr":
	                        return eerr(x3, s, m);
	                }
	            }
	            var cok0 = (function(x4, pstate, pm) {
	                return cok(x4, pstate, Memoer.update(pm, position, key, ["cok", x4, pstate]));
	            }),
	                cerr0 = (function(x4, pstate, pm) {
	                    return cerr(x4, pstate, Memoer.update(pm, position, key, ["cerr", x4,
	                        pstate
	                    ]));
	                }),
	                eok0 = (function(x4, pstate, pm) {
	                    return eok(x4, pstate, Memoer.update(pm, position, key, ["eok", x4,
	                        pstate
	                    ]));
	                }),
	                eerr0 = (function(x4, pstate, pm) {
	                    return eerr(x4, pstate, Memoer.update(pm, position, key, ["eerr", x4,
	                        pstate
	                    ]));
	                });
	            return new(Tail)(p3.run, state, m, cok0, cerr0, eok0, eerr0);
	        }));
	    }));
	    var defaultErr = (function(pos, tok) {
	        return new(UnexpectError)(pos, ((tok === null) ? "end of input" : tok));
	    });
	    (token = (function(consume, onErr) {
	        var errorHandler = (onErr || defaultErr);
	        return new(Parser)((function(s, m, cok, cerr, eok, eerr) {
	            var tok, pcok, p3;
	            return (s.isEmpty() ? eerr(errorHandler(s.position, null), s, m) : ((tok = s.first()), (
	                consume(tok) ? ((pcok = (function(x3, s0, m0) {
	                    return cok(x3, s0, Memoer.prune(m0, s0.position));
	                })), (p3 = s.next(tok)), new(Tail)(p3.run, s, m, pcok, cerr, pcok,
	                    cerr)) : eerr(errorHandler(s.position, tok), s, m))));
	        }));
	    }));
	    var p3 = token((function() {
	        return true;
	    }));
	    (anyToken = (p3.run.hasOwnProperty("displayName") ? label("Any Token", new(Parser)((function(state, m, cok,
	        cerr, eok, eerr) {
	        return new(Tail)(p3.run, state, m, cok, cerr, eok, eerr);
	    }))) : new(Parser)(Object.defineProperty(p3.run, "displayName", ({
	        value: "Any Token",
	        writable: false
	    })))));
	    var p4 = concat(chain(new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	        var peerr = (function(x3, s, m0) {
	            return eerr(x3, s, Memoer.popWindow(m0));
	        }),
	            m0 = Memoer.pushWindow(m, state.position),
	            cok0 = (function(x3, s, m1) {
	                return cok(x3, s, Memoer.popWindow(m1));
	            }),
	            eok0 = (function(x3, s, m1) {
	                return eok(x3, s, Memoer.popWindow(m1));
	            });
	        return new(Tail)(anyToken.run, state, m0, cok0, peerr, eok0, peerr);
	    })), (function(x3) {
	        return chain(getPosition, (function(z) {
	            var x4 = new(UnexpectError)(z, x3);
	            return new(Parser)((function(state, m, _, _0, _1, eerr) {
	                return eerr(x4, state, m);
	            }));
	        }));
	    })), of(null)),
	        p5 = new(Parser)((function(state, m, cok, cerr, eok, eerr) {
	            var peerr = (function(x3, s, m0) {
	                return eerr(x3, s, Memoer.popWindow(m0));
	            }),
	                m0 = Memoer.pushWindow(m, state.position),
	                cok0 = (function(x3, s, m1) {
	                    return cok(x3, s, Memoer.popWindow(m1));
	                }),
	                eok0 = (function(x3, s, m1) {
	                    return eok(x3, s, Memoer.popWindow(m1));
	                });
	            return new(Tail)(p4.run, state, m0, cok0, peerr, eok0, peerr);
	        }));
	    (eof = (p5.run.hasOwnProperty("displayName") ? label("EOF", new(Parser)((function(state, m, cok, cerr, eok,
	        eerr) {
	        return new(Tail)(p5.run, state, m, cok, cerr, eok, eerr);
	    }))) : new(Parser)(Object.defineProperty(p5.run, "displayName", ({
	        value: "EOF",
	        writable: false
	    })))));
	    (exec = (function() {
	        var args0 = arguments;
	        return trampoline(unparser.apply(null, args0));
	    }));
	    (parseState = (function(p6, state, ok, err0) {
	        return exec(p6, state, Memoer.empty, ok, err0, ok, err0);
	    }));
	    (parseStream = (function(p6, s, ud, ok, err0) {
	        var state = new(ParserState)(s, Position.initial, ud);
	        return exec(p6, state, Memoer.empty, ok, err0, ok, err0);
	    }));
	    (parse = (function(p6, input, ud, ok, err0) {
	        var s = stream.from(input),
	            state = new(ParserState)(s, Position.initial, ud);
	        return exec(p6, state, Memoer.empty, ok, err0, ok, err0);
	    }));
	    var err0 = (function(x3) {
	        throw x3;
	    });
	    (runState = (function(p6, state) {
	        return exec(p6, state, Memoer.empty, identity, err0, identity, err0);
	    }));
	    (runStream = (function(p6, s, ud) {
	        return runState(p6, new(ParserState)(s, Position.initial, ud));
	    }));
	    (run = (function(p6, input, ud) {
	        var s = stream.from(input);
	        return runState(p6, new(ParserState)(s, Position.initial, ud));
	    }));
	    var ok = (function() {
	        return true;
	    }),
	        err1 = (function() {
	            return false;
	        });
	    (testState = (function(p6, state) {
	        return exec(p6, state, Memoer.empty, ok, err1, ok, err1);
	    }));
	    (testStream = (function(p6, s, ud) {
	        return testState(p6, new(ParserState)(s, Position.initial, ud));
	    }));
	    (test = (function(p6, input, ud) {
	        var s = stream.from(input);
	        return testState(p6, new(ParserState)(s, Position.initial, ud));
	    }));
	    (exports["Tail"] = Tail);
	    (exports["trampoline"] = trampoline);
	    (exports["ParserError"] = ParserError);
	    (exports["ParseError"] = ParseError);
	    (exports["MultipleError"] = MultipleError);
	    (exports["UnknownError"] = UnknownError);
	    (exports["UnexpectError"] = UnexpectError);
	    (exports["ExpectError"] = ExpectError);
	    (exports["ParserState"] = ParserState);
	    (exports["Position"] = Position);
	    (exports["Parser"] = Parser);
	    (exports["label"] = label);
	    (exports["late"] = late);
	    (exports["rec"] = rec);
	    (exports["unparser"] = unparser);
	    (exports["always"] = always);
	    (exports["of"] = of);
	    (exports["never"] = never);
	    (exports["bind"] = bind);
	    (exports["chain"] = chain);
	    (exports["map"] = map);
	    (exports["ap"] = ap);
	    (exports["extract"] = extract);
	    (exports["getParserState"] = getParserState);
	    (exports["setParserState"] = setParserState);
	    (exports["modifyParserState"] = modifyParserState);
	    (exports["getState"] = getState);
	    (exports["setState"] = setState);
	    (exports["modifyState"] = modifyState);
	    (exports["getInput"] = getInput);
	    (exports["setInput"] = setInput);
	    (exports["getPosition"] = getPosition);
	    (exports["setPosition"] = setPosition);
	    (exports["fail"] = fail);
	    (exports["attempt"] = attempt);
	    (exports["look"] = look);
	    (exports["lookahead"] = lookahead);
	    (exports["next"] = next);
	    (exports["sequences"] = sequences);
	    (exports["sequencea"] = sequencea);
	    (exports["sequence"] = sequence);
	    (exports["empty"] = empty);
	    (exports["either"] = either);
	    (exports["concat"] = concat);
	    (exports["choices"] = choices);
	    (exports["choicea"] = choicea);
	    (exports["choice"] = choice);
	    (exports["optional"] = optional);
	    (exports["expected"] = expected);
	    (exports["not"] = not);
	    (exports["eager"] = eager);
	    (exports["binds"] = binds);
	    (exports["cons"] = cons);
	    (exports["append"] = append);
	    (exports["enumerations"] = enumerations);
	    (exports["enumerationa"] = enumerationa);
	    (exports["enumeration"] = enumeration);
	    (exports["many"] = many);
	    (exports["many1"] = many1);
	    (exports["manyTill"] = manyTill);
	    (exports["memo"] = memo);
	    (exports["token"] = token);
	    (exports["anyToken"] = anyToken);
	    (exports["eof"] = eof);
	    (exports["exec"] = exec);
	    (exports["parseState"] = parseState);
	    (exports["parseStream"] = parseStream);
	    (exports["parse"] = parse);
	    (exports["runState"] = runState);
	    (exports["runStream"] = runStream);
	    (exports["run"] = run);
	    (exports["testState"] = testState);
	    (exports["testStream"] = testStream);
	    (exports["test"] = test);
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
	 * THIS FILE IS AUTO GENERATED from 'lib/stream.kep'
	 * DO NOT EDIT
	*/
	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = function(require, exports) {
	    "use strict";
	    var end, NIL, stream, memoStream, cons, append, appendz, concat, from, first, rest, isEmpty, isStream,
	            forEach, reverse, foldl, foldr, reduce, reduceRight, toArray, zip, indexed, map, filter, bind,
	            arrayReduce = Function.prototype.call.bind(Array.prototype.reduce),
	        constant = (function(x) {
	            return (function() {
	                return x;
	            });
	        }),
	        flip = (function(f) {
	            return (function(x, y) {
	                return f(y, x);
	            });
	        }),
	        memo = (function(f) {
	            var value;
	            return (function(x) {
	                if ((value === undefined))(value = f(x));
	                return value;
	            });
	        });
	    (end = null);
	    (NIL = end);
	    (stream = (function(val, f) {
	        return ({
	            "first": val,
	            "rest": f
	        });
	    }));
	    (memoStream = (function(val, f) {
	        return stream(val, memo(f));
	    }));
	    (first = (function(s) {
	        return s.first;
	    }));
	    (rest = (function(s) {
	        return s.rest();
	    }));
	    (isEmpty = (function(s) {
	        return (s === NIL);
	    }));
	    (isStream = (function(s) {
	        return (((s && s.hasOwnProperty("first")) && s.hasOwnProperty("rest")) || (s === NIL));
	    }));
	    (cons = (function(val, s) {
	        return stream(val, constant(s));
	    }));
	    (appendz = (function(s1, f) {
	        return (isEmpty(s1) ? f() : memoStream(first(s1), appendz.bind(null, rest(s1), f)));
	    }));
	    var reducer = (function(s1, s2) {
	        return appendz(s1, constant(s2));
	    });
	    (append = (function() {
	        var streams = arguments;
	        return arrayReduce(streams, reducer, end);
	    }));
	    (concat = (function(s) {
	        return (isEmpty(s) ? s : appendz(first(s), concat.bind(null, rest(s))));
	    }));
	    var fromImpl = (function(arr, i, len) {
	        return ((i >= len) ? end : memoStream(arr[i], fromImpl.bind(null, arr, (i + 1), len)));
	    });
	    (from = (function(arr) {
	        var length = arr["length"];
	        return fromImpl(arr, 0, length);
	    }));
	    (zip = (function(l1, l2) {
	        return ((isEmpty(l1) || isEmpty(l2)) ? end : memoStream([first(l1), first(l2)], zip.bind(null,
	            rest(l1), rest(l2))));
	    }));
	    var count = (function(n) {
	        return stream(n, count.bind(null, (n + 1)));
	    });
	    (indexed = zip.bind(null, count(0)));
	    (forEach = (function(f, s) {
	        for (var head = s;
	            (!isEmpty(head));
	            (head = rest(head))) f(first(head));
	    }));
	    (foldl = (function(f, z, s) {
	        var r = z;
	        forEach((function(x) {
	            (r = f(r, x));
	        }), s);
	        return r;
	    }));
	    (reverse = foldl.bind(null, flip(cons), end));
	    (foldr = (function(f, z, s) {
	        return foldl(f, z, reverse(s));
	    }));
	    (reduce = (function(f, s) {
	        return foldl(f, first(s), rest(s));
	    }));
	    (reduceRight = (function(f, s) {
	        return reduce(f, reverse(s));
	    }));
	    var builder = (function(p, c) {
	        p.push(c);
	        return p;
	    });
	    (toArray = (function(s) {
	        return foldl(builder, [], s);
	    }));
	    (map = (function(f, s) {
	        return (isEmpty(s) ? s : memoStream(f(first(s)), map.bind(null, f, rest(s))));
	    }));
	    (filter = (function(pred, s) {
	        var head = s;
	        for (;
	            (!isEmpty(head));
	            (head = rest(head))) {
	            var x = first(head);
	            if (pred(x)) return memoStream(x, filter.bind(null, pred, rest(head)));
	        }
	        return head;
	    }));
	    (bind = (function(f, g) {
	        return (function() {
	            return f(g.apply(null, arguments));
	        });
	    })(concat, map));
	    (exports.end = end);
	    (exports.NIL = NIL);
	    (exports.stream = stream);
	    (exports.memoStream = memoStream);
	    (exports.cons = cons);
	    (exports.append = append);
	    (exports.appendz = appendz);
	    (exports.concat = concat);
	    (exports.from = from);
	    (exports.first = first);
	    (exports.rest = rest);
	    (exports.isEmpty = isEmpty);
	    (exports.isStream = isStream);
	    (exports.forEach = forEach);
	    (exports.reverse = reverse);
	    (exports.foldl = foldl);
	    (exports.foldr = foldr);
	    (exports.reduce = reduce);
	    (exports.reduceRight = reduceRight);
	    (exports.toArray = toArray);
	    (exports.zip = zip);
	    (exports.indexed = indexed);
	    (exports.map = map);
	    (exports.filter = filter);
	    (exports.bind = bind);
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
	 * THIS FILE IS AUTO GENERATED from 'lib/seshet.kep'
	 * DO NOT EDIT
	*/
	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = function(require, exports) {
	    "use strict";
	    var create, lookup, update, prune;
	    var max = (function(x, y) {
	        return ((x > y) ? x : y);
	    });
	    var heightFromChild = (function(child) {
	        return (child ? (1 + child.height) : 0);
	    });
	    var height = (function(root) {
	        return (!root ? 0 : max(heightFromChild(root.left), heightFromChild(root.right)));
	    });
	    var bf = (function(node) {
	        return (!node ? 0 : (heightFromChild(node.left) - heightFromChild(node.right)));
	    });
	    var Cell = (function(id, val, delegate) {
	        (this.id = id);
	        (this.val = val);
	        (this.delegate = delegate);
	    });
	    (Cell.lookup = (function(base, eq, id) {
	        for (var cell = base; cell;
	            (cell = cell.delegate))
	            if (eq(cell.id, id)) return cell.val;
	        return null;
	    }));
	    var Node = (function(key, cell, l, r, height) {
	        (this.key = key);
	        (this.cell = cell);
	        (this.left = l);
	        (this.right = r);
	        (this.height = height);
	    });
	    (Node.setChildren = (function(node, l, r) {
	        return new(Node)(node.key, node.cell, l, r, ((l || r) ? (1 + max(height(l), height(r))) : 0));
	    }));
	    (Node.setLeft = (function(node, l) {
	        return Node.setChildren(node, l, node.right);
	    }));
	    (Node.setRight = (function(node, r) {
	        return Node.setChildren(node, node.left, r);
	    }));
	    (Node.lookup = (function(root, compare, eq, key, id) {
	        for (var node = root; node;) {
	            var diff = compare(key, node.key);
	            if ((diff === 0)) return Cell.lookup(node.cell, eq, id);
	            (node = ((diff < 0) ? node.left : node.right));
	        }
	        return null;
	    }));
	    (Node.put = (function(node, id, val) {
	        return new(Node)(node.key, new(Cell)(id, val, node.cell), node.left, node.right, node.height);
	    }));
	    var rr = (function(node) {
	        return (!node ? node : Node.setLeft(node.right, Node.setRight(node, node.right.left)));
	    });
	    var ll = (function(node) {
	        return (!node ? node : Node.setRight(node.left, Node.setLeft(node, node.left.right)));
	    });
	    var lr = (function(node) {
	        return ll(Node.setLeft(node, rr(node.left)));
	    });
	    var rl = (function(node) {
	        return rr(Node.setRight(node, ll(node.right)));
	    });
	    var rot = (function(node) {
	        var d = bf(node);
	        if ((d > 1)) return ((bf(node.left) <= -1) ? lr(node) : ll(node));
	        else if ((d < -1)) return ((bf(node.right) >= 1) ? rl(node) : rr(node));
	        return node;
	    });
	    (Node.update = (function(root, compare, key, id, val) {
	        if (!root) return new(Node)(key, new(Cell)(id, val, null), null, null, 0);
	        var diff = compare(key, root.key);
	        if ((diff === 0)) return Node.put(root, id, val);
	        return rot(((diff < 0) ? Node.setLeft(root, Node.update(root.left, compare, key, id, val)) :
	            Node.setRight(root, Node.update(root.right, compare, key, id, val))));
	    }));
	    (Node.rebalance = (function(root) {
	        return ((Math.abs(bf(root)) <= 1) ? root : rot(Node.setChildren(root, Node.rebalance(root.left),
	            Node.rebalance(root.right))));
	    }));
	    (Node.prune = (function(root, compare, lower, upper) {
	        if (!root) return root;
	        if ((lower !== undefined)) {
	            var dl = compare(root.key, lower);
	            if ((dl < 0)) return Node.prune(root.right, compare, lower, upper);
	            else if ((dl === 0)) return Node.setChildren(root, null, Node.prune(root.right, compare,
	                undefined, upper));
	        }
	        if (((upper !== undefined) && (compare(root.key, upper) >= 0))) return Node.prune(root.left,
	            compare, lower, upper);
	        return Node.setChildren(root, Node.prune(root.left, compare, lower, upper), Node.prune(root.right,
	            compare, lower, upper));
	    }));
	    var Memoer = (function(compare, eq, root) {
	        (this.compare = compare);
	        (this.eq = eq);
	        (this.root = root);
	    });
	    (Memoer.setRoot = (function(m, root) {
	        return new(Memoer)(m.compare, m.eq, root);
	    }));
	    (create = (function() {
	            var equals = (function(x, y) {
	                return (x === y);
	            });
	            return (function(compare, eq) {
	                return new(Memoer)(compare, (eq || equals), null);
	            });
	        })
	        .call(this));
	    (lookup = (function(m, key, id) {
	        return Node.lookup(m.root, m.compare, m.eq, key, id);
	    }));
	    (update = (function(m, key, id, val) {
	        return Memoer.setRoot(m, Node.update(m.root, m.compare, key, id, val));
	    }));
	    (prune = (function(m, lower, upper) {
	        return Memoer.setRoot(m, Node.rebalance(Node.prune(m.root, m.compare, lower, upper)));
	    }));
	    (exports.create = create);
	    (exports.lookup = lookup);
	    (exports.update = update);
	    (exports.prune = prune);
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
	 * THIS FILE IS AUTO GENERATED FROM 'lib/text.kep'
	 * DO NOT EDIT
	*/
	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(9)], __WEBPACK_AMD_DEFINE_RESULT__ = function(require, exports, __o) {
	    "use strict";
	    var character, oneOf, noneOf, string, trie, match, anyChar, letter, space, digit, always = __o["always"],
	        attempt = __o["attempt"],
	        bind = __o["bind"],
	        optional = __o["optional"],
	        ExpectError = __o["ExpectError"],
	        next = __o["next"],
	        label = __o["label"],
	        token = __o["token"],
	        join = Function.prototype.call.bind(Array.prototype.join),
	        map = Function.prototype.call.bind(Array.prototype.map),
	        reduce = Function.prototype.call.bind(Array.prototype.reduce),
	        reduceRight = Function.prototype.call.bind(Array.prototype.reduceRight),
	        StringError = (function(position, string, index, expected, found) {
	            var self = this;
	            ExpectError.call(self, position, expected, found);
	            (self.string = string);
	            (self.index = index);
	        });
	    (StringError.prototype = new(ExpectError)());
	    (StringError.prototype.constructor = StringError);
	    Object.defineProperty(StringError.prototype, "errorMessage", ({
	        "get": (function() {
	            var self = this;
	            return ((((((("In string: '" + self.string) + "' at index: ") + self.index) +
	                ", Expected: ") + self.expected) + " Found: ") + (self.found ? self.found :
	                "end of input"));
	        })
	    }));
	    var unbox = (function(y) {
	        return ("" + y);
	    }),
	        _character = (function(c, err) {
	            var x;
	            return token(((x = ("" + c)), (function(r) {
	                return (x === ("" + r));
	            })), err);
	        });
	    (character = (function(c) {
	        return _character(c, (function(pos, tok) {
	            return new(ExpectError)(pos, c, ((tok === null) ? "end of input" : tok));
	        }));
	    }));
	    (oneOf = (function(chars) {
	        var chars0 = map(chars, unbox),
	            msg;
	        return token((function(x) {
	            return (chars0.indexOf(("" + x)) >= 0);
	        }), ((msg = join(chars0, " or ")), (function(pos, tok) {
	            return new(ExpectError)(pos, msg, ((tok === null) ? "end of input" : tok));
	        })));
	    }));
	    (noneOf = (function(chars) {
	        var chars0 = map(chars, unbox),
	            msg;
	        return token((function(z) {
	            var x = (chars0.indexOf(("" + z)) >= 0);
	            return (!x);
	        }), ((msg = ("none of:" + join(chars0, " or "))), (function(pos, tok) {
	            return new(ExpectError)(pos, msg, ((tok === null) ? "end of input" : tok));
	        })));
	    }));
	    var reducer = (function(p, c, i, s) {
	        return next(_character(c, (function(pos, tok) {
	            return new(StringError)(pos, s, i, c, tok);
	        })), p);
	    });
	    (string = (function(s) {
	        return attempt(reduceRight(s, reducer, always(("" + s))));
	    }));
	    var wordReduce = (function(parent, l) {
	        (parent[l] = (parent[l] || ({})));
	        return parent[l];
	    }),
	        wordsReduce = (function(trie, word) {
	            var node = reduce(word, wordReduce, trie);
	            (node[""] = word);
	            return trie;
	        }),
	        _trie = (function(trie) {
	            var chars, msg, keys = Object.keys(trie),
	                paths = reduce(keys, (function(p, c) {
	                    if (c) {
	                        (p[c] = _trie(trie[c]));
	                    }
	                    return p;
	                }), ({})),
	                select = attempt(bind(((chars = map(keys, unbox)), token((function(x) {
	                    return (chars.indexOf(("" + x)) >= 0);
	                }), ((msg = join(chars, " or ")), (function(pos, tok) {
	                    return new(ExpectError)(pos, msg, ((tok === null) ?
	                        "end of input" : tok));
	                })))), (function(y) {
	                    return paths[y];
	                })));
	            return (trie.hasOwnProperty("") ? optional(trie[""], select) : select);
	        });
	    (trie = (function(z) {
	        var z0 = reduce(z, wordsReduce, ({})),
	            chars, msg, keys, paths, select;
	        return attempt(((keys = Object.keys(z0)), (paths = reduce(keys, (function(p, c) {
	            if (c) {
	                (p[c] = _trie(z0[c]));
	            }
	            return p;
	        }), ({}))), (select = attempt(bind(((chars = map(keys, unbox)), token((function(x) {
	            return (chars.indexOf(("" + x)) >= 0);
	        }), ((msg = join(chars, " or ")), (function(pos, tok) {
	            return new(ExpectError)(pos, msg, ((tok === null) ?
	                "end of input" : tok));
	        })))), (function(y) {
	            return paths[y];
	        })))), (z0.hasOwnProperty("") ? optional(z0[""], select) : select)));
	    }));
	    (match = (function(pattern, expected) {
	        return token(RegExp.prototype.test.bind(pattern), (function(pos, tok) {
	            return new(ExpectError)(pos, expected, ((tok === null) ? "end of input" : tok));
	        }));
	    }));
	    var pattern;
	    (anyChar = label("Any Character", ((pattern = /^.$/), token(RegExp.prototype.test.bind(pattern), (function(
	        pos, tok) {
	        return new(ExpectError)(pos, "any character", ((tok === null) ? "end of input" :
	            tok));
	    })))));
	    var pattern0;
	    (letter = label("Any Letter", ((pattern0 = /^[a-z]$/i), token(RegExp.prototype.test.bind(pattern0), (
	        function(pos, tok) {
	            return new(ExpectError)(pos, "any letter character", ((tok === null) ?
	                "end of input" : tok));
	        })))));
	    var pattern1;
	    (space = label("Any Whitespace", ((pattern1 = /^\s$/i), token(RegExp.prototype.test.bind(pattern1), (
	        function(pos, tok) {
	            return new(ExpectError)(pos, "any space character", ((tok === null) ?
	                "end of input" : tok));
	        })))));
	    var pattern2;
	    (digit = label("Any Digit", ((pattern2 = /^[0-9]$/i), token(RegExp.prototype.test.bind(pattern2), (function(
	        pos, tok) {
	        return new(ExpectError)(pos, "any digit character", ((tok === null) ?
	            "end of input" : tok));
	    })))));
	    (exports["character"] = character);
	    (exports["oneOf"] = oneOf);
	    (exports["noneOf"] = noneOf);
	    (exports["string"] = string);
	    (exports["trie"] = trie);
	    (exports["match"] = match);
	    (exports["anyChar"] = anyChar);
	    (exports["letter"] = letter);
	    (exports["space"] = space);
	    (exports["digit"] = digit);
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
	 * THIS FILE IS AUTO GENERATED FROM 'lib/lang.kep'
	 * DO NOT EDIT
	*/
	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(10), __webpack_require__(14), __webpack_require__(9)], __WEBPACK_AMD_DEFINE_RESULT__ = function(require, exports, __o, __o0,
	    __o1) {
	    "use strict";
	    var times, atMostTimes, betweenTimes, then, between, sepBy1, sepBy, sepEndBy1, sepEndBy, endBy1, endBy,
	            chainl1, chainl, chainr1, chainr, NIL = __o["NIL"],
	        repeat = __o0["repeat"],
	        append = __o1["append"],
	        always = __o1["always"],
	        bind = __o1["bind"],
	        cons = __o1["cons"],
	        either = __o1["either"],
	        enumerations = __o1["enumerations"],
	        late = __o1["late"],
	        many = __o1["many"],
	        many1 = __o1["many1"],
	        next = __o1["next"],
	        optional = __o1["optional"],
	        rec = __o1["rec"],
	        _end = always(NIL),
	        _optionalValueParser = optional.bind(null, NIL);
	    (times = (function() {
	        var args = arguments;
	        return enumerations(repeat.apply(null, args));
	    }));
	    (atMostTimes = (function(n, p) {
	        return ((n <= 0) ? _end : _optionalValueParser(cons(p, late((function() {
	            return atMostTimes((n - 1), p);
	        })))));
	    }));
	    (betweenTimes = (function(min, max, p) {
	        var args, n;
	        return append(((args = [min, p]), enumerations(repeat.apply(null, args))), ((n = (max - min)), (
	            (n <= 0) ? _end : _optionalValueParser(cons(p, late((function() {
	                return atMostTimes((n - 1), p);
	            })))))));
	    }));
	    (then = (function(p, q) {
	        return bind(p, (function(x) {
	            return next(q, always(x));
	        }));
	    }));
	    (between = (function(open, close, p) {
	        return next(open, bind(p, (function(x) {
	            return next(close, always(x));
	        })));
	    }));
	    (sepBy1 = (function(sep, p) {
	        return cons(p, many(next(sep, p)));
	    }));
	    (sepBy = (function() {
	        var args = arguments;
	        return _optionalValueParser(sepBy1.apply(null, args));
	    }));
	    (sepEndBy1 = (function(sep, p) {
	        return rec((function(self) {
	            return cons(p, _optionalValueParser(next(sep, _optionalValueParser(self))));
	        }));
	    }));
	    (sepEndBy = (function(sep, p) {
	        return either(rec((function(self) {
	            return cons(p, _optionalValueParser(next(sep, _optionalValueParser(self))));
	        })), next(optional(sep), _end));
	    }));
	    (endBy1 = (function(sep, p) {
	        return many1(bind(p, (function(x) {
	            return next(sep, always(x));
	        })));
	    }));
	    (endBy = (function(sep, p) {
	        return many(bind(p, (function(x) {
	            return next(sep, always(x));
	        })));
	    }));
	    (chainl1 = (function(op, p) {
	        return bind(p, (function chain(x) {
	            return optional(x, bind(op, (function(f) {
	                return bind(p, (function(y) {
	                    return chain(f(x, y));
	                }));
	            })));
	        }));
	    }));
	    (chainl = (function(op, x, p) {
	        return optional(x, bind(p, (function chain(x0) {
	            return optional(x0, bind(op, (function(f) {
	                return bind(p, (function(y) {
	                    return chain(f(x0, y));
	                }));
	            })));
	        })));
	    }));
	    (chainr1 = (function(op, p) {
	        return rec((function(self) {
	            return bind(p, (function(x) {
	                return optional(x, bind(op, (function(f) {
	                    return self.map((function(y) {
	                        return f(x, y);
	                    }));
	                })));
	            }));
	        }));
	    }));
	    (chainr = (function(op, x, p) {
	        return optional(x, rec((function(self) {
	            return bind(p, (function(x0) {
	                return optional(x0, bind(op, (function(f) {
	                    return self.map((function(y) {
	                        return f(x0, y);
	                    }));
	                })));
	            }));
	        })));
	    }));
	    (exports["times"] = times);
	    (exports["atMostTimes"] = atMostTimes);
	    (exports["betweenTimes"] = betweenTimes);
	    (exports["then"] = then);
	    (exports["between"] = between);
	    (exports["sepBy1"] = sepBy1);
	    (exports["sepBy"] = sepBy);
	    (exports["sepEndBy1"] = sepEndBy1);
	    (exports["sepEndBy"] = sepEndBy);
	    (exports["endBy1"] = endBy1);
	    (exports["endBy"] = endBy);
	    (exports["chainl1"] = chainl1);
	    (exports["chainl"] = chainl);
	    (exports["chainr1"] = chainr1);
	    (exports["chainr"] = chainr);
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
	 * THIS FILE IS AUTO GENERATED from 'lib/gen.kep'
	 * DO NOT EDIT
	*/
	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(10)], __WEBPACK_AMD_DEFINE_RESULT__ = function(require, exports, __o) {
	    "use strict";
	    var NIL = __o["NIL"],
	        memoStream = __o["memoStream"],
	        repeat, range;
	    (repeat = (function(times, x) {
	        return ((times <= 0) ? NIL : memoStream(x, repeat.bind(null, (times - 1), x)));
	    }));
	    var rangeImpl = (function(lower, upper, step) {
	        return (((step > 0) ? (upper <= lower) : (upper >= lower)) ? NIL : memoStream(lower, rangeImpl.bind(
	            null, (lower + step), upper, step)));
	    });
	    (range = (function(lower, upper, step) {
	        var rangeLower = (isNaN(lower) ? Infinity : (+lower)),
	            rangeStep = (isNaN(step) ? 1 : (+step));
	        return (isNaN(upper) ? rangeImpl(0, rangeLower, rangeStep) : rangeImpl(rangeLower, upper,
	            rangeStep));
	    }));
	    (exports.repeat = repeat);
	    (exports.range = range);
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
	 * THIS FILE IS AUTO GENERATED FROM 'lib/incremental.kep'
	 * DO NOT EDIT
	*/
	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(9), __webpack_require__(10)], __WEBPACK_AMD_DEFINE_RESULT__ = function(require, exports, __o, stream) {
	    "use strict";
	    var provide, provideString, finish, parseIncState, parseInc, runIncState, runInc, runManyState,
	            runManyStream, runMany, bind = __o["bind"],
	        getParserState = __o["getParserState"],
	        next = __o["next"],
	        optional = __o["optional"],
	        parseState = __o["parseState"],
	        Parser = __o["Parser"],
	        ParserState = __o["ParserState"],
	        Position = __o["Position"],
	        runState = __o["runState"],
	        trampoline = __o["trampoline"],
	        streamFrom = stream["from"],
	        isEmpty = stream["isEmpty"],
	        NIL = stream["NIL"],
	        memoStream = stream["memoStream"],
	        Request = (function(chunk, k) {
	            var self = this;
	            (self.chunk = chunk);
	            (self.k = k);
	        }),
	        Session = (function(done, k, chunks) {
	            var self = this;
	            (self.done = done);
	            (self.k = k);
	            (self.chunks = chunks);
	        });
	    (Session.prototype.addChunk = (function(c) {
	        var self = this;
	        return new(Session)(self.done, self.k, self.chunks.concat(c));
	    }));
	    (Session.prototype.hasChunk = (function(c) {
	        var self = this;
	        return (c < self.chunks.length);
	    }));
	    (Session.prototype.getChunk = (function(c) {
	        var self = this;
	        return self.chunks[c];
	    }));
	    var IncrementalState = (function(chunk, state) {
	        var self = this;
	        (self.chunk = chunk);
	        (self.state = state);
	    });
	    Object.defineProperties(IncrementalState.prototype, ({
	        input: ({
	            get: (function() {
	                var self = this;
	                return self.state.input;
	            })
	        }),
	        position: ({
	            get: (function() {
	                var self = this;
	                return self.state.position;
	            })
	        }),
	        userState: ({
	            get: (function() {
	                var self = this;
	                return self.state.userState;
	            })
	        })
	    }));
	    (IncrementalState.prototype.eq = (function(other) {
	        var self = this;
	        return ((other && (other.chunk === self.chunk)) && self.state.eq(other.state));
	    }));
	    (IncrementalState.prototype.isEmpty = (function() {
	        var self = this;
	        return self.state.isEmpty();
	    }));
	    (IncrementalState.prototype.first = (function() {
	        var self = this;
	        return self.state.first();
	    }));
	    (IncrementalState.prototype.next = (function(x) {
	        var self = this;
	        if ((!self._next)) {
	            var chunk = self.chunk;
	            (self._next = bind(next(self.state.next(x), getParserState), (function(innerState) {
	                var state;
	                return (innerState.isEmpty() ? new(Parser)((function(_, m, cok) {
	                    return new(Request)((chunk + 1), (function(i) {
	                        return cok(x, new(IncrementalState)((chunk + 1),
	                            innerState.setInput(i)), m);
	                    }));
	                })) : ((state = new(IncrementalState)(chunk, innerState)), new(Parser)(
	                    (function(_, m, cok) {
	                        return cok(x, state, m);
	                    }))));
	            })));
	        }
	        return self._next;
	    }));
	    (IncrementalState.prototype.setInput = (function(input) {
	        var self = this;
	        return new(IncrementalState)(self.chunk, self.state.setInput(input));
	    }));
	    (IncrementalState.prototype.setPosition = (function(position) {
	        var self = this;
	        return new(IncrementalState)(self.chunk, self.state.setPosition(position));
	    }));
	    (IncrementalState.prototype.setUserState = (function(userState) {
	        var self = this;
	        return new(IncrementalState)(self.chunk, self.state.setUserState(userState));
	    }));
	    var forceProvide = (function(c, r) {
	        if (r.done) return r;
	        var r2 = r.addChunk(c),
	            result = trampoline(r2.k(c));
	        while (((result instanceof Request) && r2.hasChunk(result.chunk))) {
	            (result = trampoline(result.k(r2.getChunk(result.chunk))));
	        }
	        return ((result instanceof Request) ? new(Session)(false, result.k, r2.chunks) : result);
	    });
	    (provide = (function(c, r) {
	        return (isEmpty(c) ? r : forceProvide(c, r));
	    }));
	    (provideString = (function(input, r) {
	        return provide(streamFrom(input), r);
	    }));
	    var x = forceProvide.bind(null, NIL);
	    (finish = (function(z) {
	        var r = x(z);
	        return r.k();
	    }));
	    (parseIncState = (function(p, state, ok, err) {
	        var pok = (function(x0, s) {
	            return new(Session)(true, ok.bind(null, x0, s));
	        }),
	            perr = (function(x0, s) {
	                return new(Session)(true, err.bind(null, x0, s));
	            });
	        return provide(state.input, new(Session)(false, (function(i) {
	            return parseState(p, new(IncrementalState)(0, state.setInput(i)), pok, perr);
	        }), []));
	    }));
	    (parseInc = (function(p, ud, ok, err) {
	        return parseIncState(p, new(ParserState)(NIL, Position.initial, ud), ok, err);
	    }));
	    var ok = (function(x0) {
	        return x0;
	    }),
	        err = (function(x0) {
	            throw x0;
	        });
	    (runIncState = (function(p, state) {
	        return parseIncState(p, state, ok, err);
	    }));
	    (runInc = (function(p, ud) {
	        return runIncState(p, new(ParserState)(NIL, Position.initial, ud));
	    }));
	    (runManyState = (function(p, state) {
	        var manyP = optional(NIL, bind(p, (function(x0) {
	            return new(Parser)((function(state0, m, _, _0, eok, _1) {
	                return eok(memoStream(x0, runState.bind(null, manyP, state0, m)),
	                    state0, m);
	            }));
	        })));
	        return runState(manyP, state);
	    }));
	    (runManyStream = (function(p, s, ud) {
	        return runManyState(p, new(ParserState)(s, Position.initial, ud));
	    }));
	    (runMany = (function(p, input, ud) {
	        return runManyStream(p, streamFrom(input), ud);
	    }));
	    (exports["provide"] = provide);
	    (exports["provideString"] = provideString);
	    (exports["finish"] = finish);
	    (exports["parseIncState"] = parseIncState);
	    (exports["parseInc"] = parseInc);
	    (exports["runIncState"] = runIncState);
	    (exports["runInc"] = runInc);
	    (exports["runManyState"] = runManyState);
	    (exports["runManyStream"] = runManyStream);
	    (exports["runMany"] = runMany);
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(17)(__webpack_require__(18))

/***/ },
/* 17 */
/***/ function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	module.exports = function(src) {
		if (typeof execScript !== "undefined")
			execScript(src);
		else
			eval.call(null, src);
	}


/***/ },
/* 18 */
/***/ function(module, exports) {

	module.exports = "var OUTPUT_CHANNELS=2;\nvar vowels={\n\ta : {frequencies: []},\n\te :\t{frequencies: [270,2300,3000]},\n\ti : {frequencies: []},\n\to : {frequencies: []},\n\tu : {frequencies: []}\n};\n\nfunction Graph(msg,ac,sampleBank){\n\tthis.ac = ac;\n\tvar last,temp;\n\tthis.when = msg.when;\n\t// get basic buffer player, including speed change and sample reversal\n\tthis.source = last = ac.createBufferSource();\n\tif(typeof msg.begin != 'number') msg.begin = 0;\n\tif(typeof msg.end != 'number') msg.end = 1;\n\tthis.begin = msg.begin;\n\tthis.end = msg.end;\n\tvar buffer = sampleBank.getBuffer(msg.sample_name,msg.sample_n);\n\tif(isNaN(parseInt(msg.speed))) msg.speed = 1;\n\tif(msg.speed>=0 && buffer != null) this.source.buffer = buffer;\n\tif(msg.speed<0 && buffer != null) {this.source.buffer=buffer; last = reverse(last,ac)};\n\tthis.source.playbackRate.value=Math.abs(msg.speed);\n\tif(buffer != null) this.start();\n\telse { // buffer not available but may be available soon\n\t\tvar closure = this;\n\t\tvar reattemptDelay = (msg.when-ac.currentTime-0.2)*1000;\n\t\tsetTimeout(function(){\n\t\t\tvar buffer = sampleBank.getBuffer(msg.sample_name,msg.sample_n);\n\t\t\tif(buffer != null) {\n\t\t\t\tclosure.source.buffer = buffer;\n\t\t\t\tclosure.start();\n\t\t\t}\n\t\t  else console.log(\"unable to access sample \" + msg.sample_name + \":\" + msg.sample_n + \" on second attempt\");\n\t\t},reattemptDelay);\n\t}\n\n\t// accelerate\n\tif(isNaN(parseInt(msg.accelerate))) msg.accelerate = 0;\n\tif(msg.accelerate!=0){\n\t\tthis.source.playbackRate.exponentialRampToValueAtTime(msg.accelerate, this.source.buffer.duration);\n\t}\n\n\t// // Distortion\n\t// //if(isNaN(parseInt(msg.shape))) msg.shape = 0;\n\tlast = shape(last, msg.shape, ac);\n\n\t//Lowpass filtering @what level/function to set frequency and resonant gain at?\n\tlast = lowPassFilter(last, msg.cutoff, msg.resonance, ac);\n\n\t//higpass filtering @what to do with resonance, and what level/function to set frequency at?\n\tlast = highPassFilter(last, msg.hcutoff, msg.hresonance, ac)\n\n\t//Band Pass Filtering @where to set frequency ranges?\n\tlast = bandPassFilter(last, msg.bandf, msg.bandq, ac)\n\n\tlast = vowel(last, msg.vowel, ac);\n\n\t// Delay\n\tlast = delay(last,msg.delay,msg.delaytime,msg.delayfeedback);\n\n\t//Samle_loop\n\t/* if (msg.sample_loop>0){\n\t\tlast.loop=true;\n\t\tconsole.log(last.loop)\n\t\tlast.connect(ac.destination)\n\t\tthis.source.start(0)\n\t\treturn\n\t} */\n\n\n\t/*For testing otherCoarse (leave here for now)\n\tlast = otherCoarse(last, msg.coarse, ac);\n\tvar crushNode=last;*/\n\n\t//Coarse\n\t//Safety and msg parsing\n\tif(isNaN(parseInt(msg.coarse))) msg.coarse = 1;\n\tmsg.coarse=Math.abs(msg.coarse);\n\n\tvar coarseNode;\n\t//If coarse is valid, coarseNode becomes last with coarseNode effect\n\t//otherwise, coarseNode becomes last \t\t\n\tif(msg.coarse>1){\n\t\t//New script processor node\t\n\t\tcoarseNode = coarse(msg.coarse, ac)\n\t\tlast.connect(coarseNode);\n\t}\n\telse coarseNode = last;\n\t//When last is done playing, disconnects last from coarseNode\n\t//this is necessary or the processor node's (coarseNode's) handler\n\t//function will infinitely be invoked\n\tlast.onended=function(){\n\t    if (last !== coarseNode) { last.disconnect(coarseNode) }\n\t\tcoarseNode.disconnect()\n\t}\n\n\n\n\t//Crush\n\tif(isNaN(parseInt(msg.crush))) msg.crush = null;\n\telse msg.crush=Math.abs(msg.crush)\n\tvar crushNode;\n\n\tif(msg.crush!=null && msg.crush>0){\n\t\tconsole.log(\"hello\")\n\t\tcrushNode=crush(msg.crush, ac)\n\t\tcoarseNode.connect(crushNode);\n\t}\n\telse crushNode = coarseNode;\n\n\tcoarseNode.onended = function(){\n\t    if (coarseNode !== crushNode) {coarseNode.disconnect(crushNode)}\n\t\tcrushNode.disconnect();\n\t}\n\n\n\n\n\n\t//Gain\n\tif(isNaN(parseInt(msg.gain))) msg.gain = 1;\n\tvar gain = ac.createGain();\n\t// @this should be as per Dirt's mapping...\n\tgain.gain.value = Math.abs(msg.gain);\n\tcrushNode.connect(gain);\n\tvar last1 = gain;\n\n\n\t//Panning (currently stereo)\n\tif(isNaN(parseInt(msg.pan))) msg.pan = 0.5;\n\tvar gain1 = ac.createGain();\n\tvar gain2 = ac.createGain();\n\tgain1.gain.value=1-msg.pan;\n\tgain2.gain.value=msg.pan;\n\t// @should do equal power panning or something like that instead, i.e. +3 dB as becomes centre-panned\n\tlast1.connect(gain1);\n\tlast1.connect(gain2);\n\tvar channelMerger = ac.createChannelMerger(2);\n\tgain1.connect(channelMerger,0,0);\n\tgain2.connect(channelMerger,0,1);\n\tchannelMerger.connect(ac.destination);\n\n}\n\n\n//Coarse\n//Returns a scriptNode that will create a fake-resampling effect when .connected to another node.\n//Note: is important to disconnect what is returned by this function once a sample has finished playing\n//otherwise the onaudioprocess function will be called infinitely.\nfunction coarse (coarse, ac){\n\tvar scriptNode = ac.createScriptProcessor();\n\tscriptNode.onaudioprocess = function(audioProcessingEvent){\n\t\tvar inputBuffer = audioProcessingEvent.inputBuffer;\t\n\t\tvar outputBuffer = audioProcessingEvent.outputBuffer;\n\t\tfor (var channel=0;channel<outputBuffer.numberOfChannels; channel++){\n\t\t\tvar inputData = inputBuffer.getChannelData(channel);\n\t\t\tvar outputData = outputBuffer.getChannelData(channel);\n\t\t\tfor(var frame=0; frame<inputBuffer.length; frame++){\n\n\t\t\t\t if(frame%coarse==0) outputData[frame]=inputData[frame];\n\t\t\t\telse outputData[frame]=outputData[frame-1];\t\t\t\t\n\t\t\t}\n\t\t}\n\t\tconsole.log(\"hello\")\n\t\t//return outputBuffer\n\t}//end scriptNode audio processing handler \n\t\n\treturn scriptNode;\n}\n\n\n\n//@ is the scriptNode handler continuously called even after it has finished playing?\n//how to stop that?\nfunction reverse (input, ac){\n\n\t\n\tvar scriptNode = ac.createScriptProcessor();\n\n\tinput.connect(scriptNode);\n\n\tscriptNode.onaudioprocess = function(audioProcessingEvent){\n\t\tvar inputBuffer = audioProcessingEvent.inputBuffer;\n\t\n\t\tvar outputBuffer = audioProcessingEvent.outputBuffer;\n\n\t\tfor (var channel=0;channel<outputBuffer.numberOfChannels; channel++){\n\t\t\tvar inputData = inputBuffer.getChannelData(channel);\n\t\t\tvar outputData = outputBuffer.getChannelData(channel);\n\t\t\tfor(var frame=0; frame<inputBuffer.length; frame++){\n\t\t\t\toutputData[frame]= inputData[inputBuffer.length-frame];\n\t\t\t}//Frames\n\t\t}//Channels\n\t\t//return outputBuffer\n\t\tconsole.log(\"input: \");\n\t\tconsole.log(inputData)\n\t\tconsole.log(\"output: \")\n\t\tconsole.log(outputData);\n\t}//end scriptNode audio processing handler \n\n\treturn scriptNode;\n}\n\n\n//@ is the scriptNode handler continuously called even after it has finished playing?\n//how to stop that?\nfunction crush (crush, ac){\n\n\tvar scriptNode = ac.createScriptProcessor();\n\n\t//scriptNode Processing Handler\n\tscriptNode.onaudioprocess = function(audioProcessingEvent){\n\t\tvar inputBuffer = audioProcessingEvent.inputBuffer;\n\t\tvar outputBuffer = audioProcessingEvent.outputBuffer;\n\t\tfor (var channel=0;channel<outputBuffer.numberOfChannels; channel++){\n\t\t\tvar inputData = inputBuffer.getChannelData(channel);\n\t\t\tvar outputData = outputBuffer.getChannelData(channel);\n\t\t\tfor(var frame=0; frame<inputBuffer.length; frame++){\n\n\t\t\t\toutputData[frame]=Math.round(inputData[frame]*Math.pow(2,(crush-1)))/Math.pow(2,(crush-1));\n\t\t\t}//Frames\n\t\t}//Channels\n\t}//end scriptNode audio processing handler \n\treturn scriptNode;\n\n}//End Crush\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nGraph.prototype.start = function() {\n\tconsole.log(\"start\")\n\tthis.source.start(this.when,this.begin*this.source.buffer.duration,this.end*this.source.buffer.duration);\n}\n\n\nfunction delay(input,outputGain,delayTime,delayFeedback) {\n\tif(isNaN(parseInt(outputGain))) outputGain = 0;\n\toutputGain = Math.abs(outputGain);\n\tif(outputGain!=0){\n\t\tvar delayNode = ac.createDelay();\n\t\tif(isNaN(parseInt(delayTime))) {\n\t\t\tconsole.log(\"warning: delaytime not a number, using default of 1\");\n\t\t\tdelayTime = 1;\n\t\t}\n\t\tdelayNode.delayTime.value = delayTime;\n\t\tvar feedBackGain = ac.createGain();\n\t\tif(isNaN(parseInt(delayFeedback))) {\n\t\t\tconsole.log(\"warning: delayfeedback not a number, using default of 0.5\");\n\t\t\tdelayFeedback = 0.5;\n\t\t}\n\t\tfeedBackGain.gain.value= delayFeedback;\n\t\tvar delayGain = ac.createGain();\n\t\tdelayGain.gain.value = outputGain;\n\t\tinput.connect(delayNode);\n\t\tdelayNode.connect(feedBackGain);\n\t\tdelayNode.connect(delayGain);\n\t\tfeedbackGain.connect(delayNode);\n\t\treturn delayGain;\n\t} else return input;\n}\n\n//Borrowed from documentation @may want to redo/make differently\nfunction makeDistortionCurve(amount) {\n  var k = typeof amount === 'number' ? amount : 50,\n    n_samples = 44100,\n    curve = new Float32Array(n_samples),\n    deg = Math.PI / 180,\n    i = 0,\n    x;\n  for ( ; i < n_samples; ++i ) {\n    x = i * 2 / n_samples - 1;\n    curve[i] = ( 3 + k ) * x * 20 * deg / ( Math.PI + k * Math.abs(x) );\n  }\n  return curve;\n};\n\nfunction bandPassFilter(input, bandf, bandq, ac){\n\t//Bandpass Filter\n\tif(bandf>0 && bandf<1 && bandq>0){\n\t\t\tfilterNode = ac.createBiquadFilter();\n\t\t\tfilterNode.type = 'bandpass';\n\t\t\tfilterNode.frequency.value = bandf*10000;\n\t\t\tfilterNode.Q.value = bandq;\n\n\t\t\tinput.connect(filterNode);\n\t\t\treturn filterNode;\n\t}\n\telse return input;\n}\n\nfunction highPassFilter (input, hcutoff, hresonance, ac){\n\t\n\tif(hresonance>0 && hresonance<1 && hcutoff>0 && hcutoff<1){\n\t\t\t//Filtering\n\t\t\tfilterNode = ac.createBiquadFilter();\n\t\t\tfilterNode.type = 'highpass';\n\t\t\tfilterNode.frequency.value = hcutoff*10000;\n\t\t\tfilterNode.Q.value = 0.1;\n\t\t\tinput.connect(filterNode);\n\t\t\tinput = filterNode;\n\n\t\t\t//Resonance\n\t\t\tfilterNode = ac.createBiquadFilter();\n\t\t\tfilterNode.type = 'peaking';\n\t\t\tfilterNode.frequency.value = hcutoff*10000+100;\n\t\t\tfilterNode.Q.value=70;\n\t\t\tfilterNode.gain.value = hresonance*10;\n\t\t\tinput.connect(filterNode);\n\n\t\t\tinput.connect(filterNode);\n\t\t\treturn filterNode;\n\t}\n\telse return input;\n}\n\n\nfunction lowPassFilter(input, cutoff, resonance, ac){\n\n\tif(resonance>0 && resonance<=1 && cutoff>0 && cutoff<=1){\n\n\t\t\tvar filterNode = ac.createBiquadFilter();\n\t\t\tfilterNode.type = 'lowpass';\n\t\t\tfilterNode.frequency.value = cutoff*14000;\n\t\t\tfilterNode.Q.value = 0.1;\n\n\t\t\tinput.connect(filterNode);\n\t\t\tinput = filterNode;\n\n\t\t\tfilterNode = ac.createBiquadFilter();\n\t\t\tfilterNode.type = 'peaking';\n\t\t\tfilterNode.frequency.value = cutoff*1400+100;\n\t\t\tfilterNode.Q.value=70;\n\t\t\tfilterNode.gain.value = resonance*10;\n\t\t\tinput.connect(filterNode);\n\t\t\treturn filterNode;\n\t}\n\telse return input\n\n}\n\nfunction shape(input, shape, ac){\n\tif (isNaN(parseInt(shape)))return input;\n\n\tif(shape!=0) {\n\t\t//Distortion limited to [0,1]\n\t\tif (Math.abs(shape)>1) shape=1;\n\t\telse shape=Math.abs(shape);\n\t\tvar distortionNode = ac.createWaveShaper();\n\n\t\t//@Change makeDistortion Curve?\n\t\tdistortionNode.curve = makeDistortionCurve(shape*300);\n\t\tdistortionNode.oversample = '2x';\n\n\t\t//Connect Distortion to last, and pass on 'last'\n\t\tinput.connect(distortionNode);\n\t\treturn distortionNode;\n\t}\n\telse \n\t\treturn input;\n}\n\n//Returns a new BufferSourceNode containing a buffer with the reversed frames of the\n//parameter 'buffer'\n//@add add more for multi-channel samples\nfunction reverseBuffer(buffer,ac){\n\tvar frames = buffer.length;\n\tvar pcmData = new Float32Array(frames);\n\tvar newBuffer = ac.createBuffer(buffer.numberOfChannels,buffer.length,ac.sampleRate);\n\t// var source = ac.createBufferSource();\n\tvar newChannelData = new Float32Array(frames);\n\n\tbuffer.copyFromChannel(pcmData,0,0);\n\tfor (var i =0;i<frames;i++){\n\t\tnewChannelData[i]=pcmData[frames-i];\n\t}\n\t//First element of newChannelData will be set to NaN - causes clipping on first frame\n\t//set to send element to get rid of clipping\n\tnewChannelData[0]=newChannelData[1];\n\tnewBuffer.copyToChannel(newChannelData,0,0);\n\n\treturn newBuffer;\n\n//\tsource.buffer = newBuffer;\n//\treturn source;\n}\n\n//Returns BufferSourceNode with a sped up buffer. Volume of samples given a high speed parameter (>~4) is\n//better preserved using this function rather than simply altering the playbackRate of the buffer.\nfunction speed(buffer,rate,ac){\n\tvar frames = buffer.length;\n\tvar pcmData = new Float32Array(frames);\n\tvar newBuffer = ac.createBuffer(buffer.numberOfChannels,Math.trunc(buffer.length/rate),ac.sampleRate);\n\tvar newsource = ac.createBufferSource();\n\tvar newChannelData = new Float32Array(newBuffer.length);\n\tbuffer.copyFromChannel(pcmData,0,0);\n\n\tfor(var i=0; i<newBuffer.length; i++){\n\t\tnewChannelData[i]=pcmData[i*rate];\n\t}\n\n\n\tnewBuffer.copyToChannel(newChannelData,0,0);\n\tnewsource.buffer = newBuffer;\n\treturn newsource;\n\n}\n\n//Vowel effect\n//@ get frequencies from superdirt\nfunction vowel (input, vowel, ac){\n\tif (vowel=='a'||vowel=='e'||vowel=='i'||vowel=='o'||vowel=='u'){\n\t\t\tvar frequencies;\n\t\t\tvar q = [5/20,20/20,50/20];\n\t\t\tvar gain= ac.createGain();\n\n\t\t\tswitch (vowel){\n\t\t\t\tcase('a'):\n\t\t\t\t\tfrequencies= [730,1090,2440];\n\t\t\t\t\tbreak\n\t\t\t\tcase('e'):\n\t\t\t\t\tfrequencies = [270,2290,3010];\n\t\t\t\t\tbreak\n\t\t\t\tcase('i'):\n\t\t\t\t\tfrequencies = [390, 1990,2550];\n\t\t\t\t\tbreak;\n\t\t\t\tcase('o'):\n\t\t\t\t\tfrequencies = [360,1390, 1090];\n\t\t\t\t\tbreak;\n\t\t\t\tcase('u'):\n\t\t\t\t\tfrequencies = [520,1190, 2390];\n\t\t\t}\n\t\t\tfor(var i=0; i<3; i++){\n\t\t\t\tfilterNode = ac.createBiquadFilter()\n\t\t\t\tfilterNode.type = 'bandpass'\n\t\t\t\tfilterNode.Q.value=1;\n\t\t\t\tfilterNode.frequency.value=frequencies[i];\n\n\t\t\t\tinput.connect(filterNode);\n\t\t\t\t//temp.connect(gain);\n\t\t\t\tinput=filterNode;\n\t\t\t}\n\t\t\tvar makeupGain = ac.createGain();\n\t\t\tmakeupGain.gain.value=20;\n\t\t\tfilterNode.connect(makeupGain);\n\t\t\treturn makeupGain;\n\t}\n\telse return input\n}\n\n\n\n\n\n\n/* Some older functions for effects, may want to refer back to them eventually\n\n\n\n//@ is the scriptNode handler continuously called even after it has finished playing?\n//how to stop that?\nfunction otherCoarse (input, coarse, ac){\n\t//Safety and msg parsing\n\tif(isNaN(parseInt(coarse))) coarse = 1;\n\tcoarse=Math.abs(coarse);\n\n\tvar coarseNode;\n\t//If coarse is valid, coarseNode becomes last with coarseNode effect\n\t//otherwise, coarseNode becomes last \t\t\n\tif(coarse>1){\n\tvar scriptNode = ac.createScriptProcessor();\n\tscriptNode.onaudioprocess = function(audioProcessingEvent){\n\t\tvar inputBuffer = audioProcessingEvent.inputBuffer;\n\t\n\t\tvar outputBuffer = audioProcessingEvent.outputBuffer;\n\n\t\tfor (var channel=0;channel<outputBuffer.numberOfChannels; channel++){\n\t\t\tvar inputData = inputBuffer.getChannelData(channel);\n\t\t\tvar outputData = outputBuffer.getChannelData(channel);\n\t\t\tfor(var frame=0; frame<inputBuffer.length; frame++){\n\t\t\t\tif(frame%coarse==0) outputData[frame]=inputData[frame];\n\t\t\t\telse outputData[frame]=outputData[frame-1];\t\n\t\t\t}//Frames\n\t\t}//Channels\n\t\tconsole.log(\"infinite loop in otherCoarse, scriptNode handler\")\n\t}//end scriptNode audio processing handler \n\n\tinput.connect(scriptNode);\n\tinput.onended= function(){input.disconnect(scriptNode)};\n\treturn scriptNode;\n\t}\n\telse \n\t\treturn input\n}\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n//Used for coarse effect\nfunction decimate(buffer,rate,ac){\n\tvar frames = buffer.length;\n\tvar pcmData = new Float32Array(frames);\n\tvar newBuffer = ac.createBuffer(buffer.numberOfChannels,buffer.length,ac.sampleRate);\n\tvar newsource = ac.createBufferSource();\n\tvar newChannelData = new Float32Array(newBuffer.length);\n\tbuffer.copyFromChannel(pcmData,0,0);\n\n\tfor(var i=0; i<newBuffer.length; i++){\n\t\tif(i%rate==0){\n\t\tnewChannelData[i]=pcmData[i];\n\t\t}\n\t\telse{\n\t\tnewChannelData[i]=newChannelData[i-1];\n\t\t}\n\n\t}\n\tnewBuffer.copyToChannel(newChannelData,0,0);\n\tnewsource.buffer = newBuffer;\n\treturn newsource;\n }\n\n// returns a buffer source with a buffer with bit resolution degraded\n// by 'crush'\nfunction crush(buffer, crush, ac){\n\tvar frames = buffer.length;\n\tvar pcmData = new Float32Array(frames);\n\tvar newBuffer = ac.createBuffer(buffer.numberOfChannels,buffer.length,ac.sampleRate);\n\tvar source = ac.createBufferSource();\n\tvar newChannelData = new Float32Array(frames);\n\tbuffer.copyFromChannel(pcmData,0,0);\n\n\tfor (var i =0;i<frames;i++){\n\t\tnewChannelData[i]=Math.round(pcmData[i]*Math.pow(2,(crush-1)))/Math.pow(2,(crush-1));\n\t}\n\tconsole.log(newChannelData)\n\tnewBuffer.copyToChannel(newChannelData,0,0);\n\tsource.buffer = newBuffer;\n\n\treturn source;\n}\n\n\n\n*/\n\n"

/***/ },
/* 19 */
/***/ function(module, exports) {

	
	SampleBank = function(sampleMapUrl,urlPrefix,audioContext) {
	  this.sampleMapUrl = sampleMapUrl;
	  this.urlPrefix = urlPrefix;
	  this.samples = {};
	  if(audioContext == null) throw Error("audioContext argument to SampleBank constructor was null");
	  this.ac = audioContext;

	  var request = new XMLHttpRequest();
	  request.open('GET',this.sampleMapUrl,true);
	  request.responseType = "json";
	  var closure = this;
	  request.onload = function() {
	    if(request.readyState != 4) throw Error("readyState != 4 in callback of sampleMap load");
	    if(request.status != 200) throw Error("status != 200 in callback of sampleMap load");
	    if(request.response == null) throw Error("JSON response null in callback of sampleMap load");
	    closure.sampleMap = request.response;
	    console.log("sampleMap loaded from " + closure.sampleMapUrl);
	  };
	  request.send();
	}

	// loads the set of samples with the same name (but different number)
	SampleBank.prototype.loadAllNamed = function(name) {
	  if(this.sampleMap == null) throw Error("SampleBank.loadAllNamed: sampleMap is null");
	  if(this.sampleMap[name] == null) throw Error("SampleBank.loadAllNamed: no sampleMap for " + name);
	  for(var n=0;n<this.sampleMap[name].length;n++) this.load(name,n);
	}

	// loads an individual sample
	SampleBank.prototype.load = function(name,number) {
	  if(this.sampleMap == null) throw Error("SampleBank.load: sampleMap is null");
	  if(this.sampleMap[name] == null) throw Error("SampleBank.load: no sampleMap for " + name);
	  if(number >= this.sampleMap[name].length) throw Error("SampleBank.load: number > number of samples");
	  var filename = this.sampleMap[name][number];
	  if(this.samples[filename] != null) {
	    if(this.samples[filename].status == 'ready') {
	        console.log('warning: already loaded sample ' + name);
	        return;
	    }
	    if(this.samples[filename].status == 'loading') {
	        console.log('warning: loading already in progress for sample ' + name);
	        return;
	    }
	  }
	  this.samples[filename] = { status: 'loading' };
	  var url = this.urlPrefix + "/" + filename;
	  var request = new XMLHttpRequest();
	  try {
	    request.open('GET',url,true);
	    request.responseType = 'arraybuffer';
	    var closure = this; // a closure is necessary for...
	    request.onload = function() {
	      closure.ac.decodeAudioData(request.response, function(x) {
	        console.log("sample " + url + "loaded");
	        closure.samples[filename].buffer = x; // ...the decoded data to be kept in the object
	        closure.samples[filename].status = 'ready';
	      },
	      function(err) {
	        console.log("error decoding sample " + url);
	        closure.samples[filename].status = 'error';
	      });
	    };
	    request.send();
	  }
	  catch(e) {
	    console.log("exception during loading of sample " + name + ":" + e);
	  }
	}

	SampleBank.prototype.getBuffer = function(name,number) {
	  if(this.sampleMap == null) throw Error("SampleBank.getBuffer: sampleMap is null");
	  if(this.sampleMap[name] == null) throw Error("SampleBank.getBuffer: no sampleMap for " + name);
	  number = number % this.sampleMap[name].length;

	  var filename = this.sampleMap[name][number];
	  if(this.samples[filename] == null) {
	    this.load(name,number);
	    console.log("loading sample " + filename + "for the first time");
	    return null;
	  }
	  if(this.samples[filename].status == 'error') {
	    console.log("SampleBank.getBuffer: sample " + name + " has status error");
	    return null;
	  }
	  if(this.samples[filename].status == 'loading') {
	    console.log("SampleBank.getBuffer: sample" + name + " is still loading");
	  }
	  return this.samples[filename].buffer;
	}


/***/ },
/* 20 */
/***/ function(module, exports) {

	WebDirt = function() {
	  window.AudioContext = window.AudioContext || window.webkitAudioContext;
	  try {
	    this.ac = new AudioContext();
	    console.log("WebDirt audio context created");
	  }
	  catch(e) {
	    alert('Web Audio API is not supported in this browser');
	  }
	  this.sampleBank = new SampleBank('sampleMap.json','samples',this.ac);
	}

	WebDirt.prototype.queue = function(msg) {
		if(msg.when==null) throw Error ("Sample given no 'when' parameter");
		var graph = new Graph(msg,this.ac,this.sampleBank);
	}

	WebDirt.prototype.subscribeToTidalSocket = function(withLog) {
	  if(withLog == null)withLog = false;
	  window.WebSocket = window.WebSocket || window.MozWebSocket;
	  var url = 'ws://127.0.0.1:7771'; // hard-coded to local server for now...
	  console.log("attempting websocket connection to " + url);
	  ws = new WebSocket(url);
	  ws.onopen = function () { console.log("websocket connection to tidalSocket opened"); };
	  ws.onerror = function () { console.log("ERROR opening websocket connection to tidalSocket"); };
	  var closure = this;
	  ws.onmessage = function (m) {
	    if(m.data == null) throw Error("null data in tidalSocket onmessage handler");
	    var msg = JSON.parse(m.data);
	    if(msg.address == null) throw Error("received message from tidalSocket with no address: " + m.data);
	    else if(msg.address != "/play") throw Error("address of message from tidalSocket wasn't /play: " + m.data);
	    else if(msg.args.constructor !== Array) throw Error("ERROR: message from tidalSocket where args doesn't exist or isn't an array: " + m.data);
	    else if(msg.args.length != 30) throw Error("ERROR: message from tidalSocket with " + msg.args.length + " args instead of 30: " + m.data);
	    else {
	      var x = {};
	      var diff = Date.now()/1000 - closure.ac.currentTime;
	      x.when = msg.args[0] + (msg.args[1]/1000000) - diff;
	      x.sample_name = msg.args[3];
	      //4 not used.
	      x.begin = msg.args[5];
	      x.end = msg.args[6];
	      x.speed = msg.args[7];
	      x.pan = msg.args[8];
	      //9 not used.
	      x.vowel=msg.args[10];
	      x.cutoff = msg.args[11];
	      x.resonance = msg.args[12];
	      x.accelerate = msg.args[13];
	      x.shape = msg.args[14];
	      //15 not used.
	      x.gain = msg.args[16]
	      x.cut = msg.args[17];
	      x.delay = msg.args[18];
	      x.delaytime = msg.args[19];
	      x.delayfeedback = msg.args[20]
	      x.crush = msg.args[21];
	      x.coarse = msg.args[22];
	      x.hcutoff = msg.args[23];
	      x.hresonance = msg.args[24];
	      x.bandf = msg.args[25];
	      x.bandq = msg.args[26];
	      x.unit_name = msg.args[27];
	      x.sample_loop = msg.args[28];
	      x.sample_n = msg.args[29];


	      closure.queue(x);
	      if(withLog)console.log(msg);
	    }
	  };
	}


/***/ },
/* 21 */
/***/ function(module, exports) {

	(function () {
	    'use strict';
	    function CustomEvent (event, params) {
	        params = params || { bubbles: false, cancelable: false, detail: undefined };
	        var evt;
	        try {
	            evt = document.createEvent('CustomEvent');
	            evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
	        } catch (error) {
	            // fallback for browsers that don't support createEvent('CustomEvent')
	            evt = document.createEvent('Event');
	            for (var param in params) {
	                evt[param] = params[param];
	            }
	            evt.initEvent(event, params.bubbles, params.cancelable);
	        }
	        return evt;
	    }

	    if(typeof window.CustomEvent !== 'undefined') {
	        CustomEvent.prototype = window.CustomEvent.prototype;
	    }

	    window.CustomEvent = CustomEvent;
	})();

/***/ },
/* 22 */
/***/ function(module, exports) {

	function Pattern(arc) {
	  if (typeof arc === "function") {
	    this.arc = arc;
	  }
	  else {
	    this.arc = pure(arc).arc;
	  }
	}

	var patternMethods = {
	  withQueryArc: function withQueryArc(fn) {
	    var pattern = this;
	    return new Pattern(function(s, e) {
	      return pattern.arc.apply(pattern, fn([s, e]));
	    });
	  },
	  withQueryTime: function withQueryTime(fn) {
	    return this.withQueryArc(Event.mapArc.bind(null, fn));
	  },
	  withResultArc: function withResultArc(fn) {
	    var pattern = this;
	    return new Pattern(function(s,e) {
	      return Event.mapArcs(fn, pattern.arc(s, e));
	    });
	  },
	  withResultTime: function withResultTime(fn) {
	    return this.withResultArc(Event.mapArc.bind(null, fn));
	  },
	  overlay: function overlay(p) {
	    var self = this;
	    return new Pattern(function(s, e) {
	      return self.arc(s, e).concat(p.arc(s, e));
	    });
	  },
	  toString: function toString() {
	    var events = this.arc(0,1);
	    return events.map(function(e) {
	      return "(" + e[2] + " " + Event.showArc(e[0]) + ")";
	    }).join(" ");
	  },
	  splitAtSam: function splitAtSam() {
	    var self = this,
	        trimArc = function(sprime, s, e) {
	          return [
	            Fraction(Math.max(sprime, s)),
	            Fraction(Math.min(sprime + 1, e))
	          ];
	        },
	        p = new Pattern(function (s,e) {
	          return Event.mapSnds(
	            trimArc(Event.sam(s)),
	            self.arc(s,e)
	          );
	        });
	    
	    return this.splitQueries(p);
	  },
	  splitQueries: function splitQueries() {
	    var self = this;
	    return new Pattern(function(s,e) {
	      return Event.arcCycles(s, e).reduce(function(events, a) {
	        return events.concat(self.arc(a[0], a[1]));
	      }, []);
	    });
	  },
	  density: function density(time) {
	    switch (time) {
	    case 0:
	      return silence();
	    case 1:
	      return this;
	    default:
	      return (this.withQueryTime(function(x) {
	        return Fraction(x * time);
	      })).withResultTime(function(x) {
	        return Fraction(x / time);
	      });
	    }
	  },
	  filterStartInRange: function filterStartInRange() {
	    var self = this;
	    return new Pattern(function(s,e) {
	      return self.arc(s,e).filter(function(event) {
	        return (Event.onset(event) < s);
	      });
	    });
	  },
	  filterOnsets: function filterOnsets() {
	    var self = this;
	    return new Pattern(function(s,e) {
	      return self.arc(s,e).filter(function(event) {
	        return (Event.onset(event) >= Event.start(event));
	      });
	    });
	  },
	  filterOnsetsInRange: function filterOnsetsInRange() {
	    return this.filterOnsets(this.filterStartInRange);
	  },
	  // does not produce a Pattern
	  seqToRelOnsets: function seqToRelOnsets(s, e) {
	    return this.filterOnsetsInRange().arc(s, e).map(function(event) {
	      var sprime = Event.onset(event),
	          x = event[2];
	      return [ (sprime - s) / (e - s), x ];
	    });
	  }
	  
	};

	Object.keys(patternMethods).forEach(function(key) {
	  Pattern.prototype[key] = patternMethods[key];
	});

	function silence() {
	  return new Pattern(function() { return []; });
	}

	function pure(value) {
	  return new Pattern(function(s, e) {
	    var events = [], i;

	    for (i = Math.floor(s); i < Math.ceil(e); i++) {
	      events.push([
	        [Fraction(i, 1), Fraction(i + 1, 1)],
	        [Fraction(i, 1), Fraction(i + 1, 1)],
	        value
	      ]);
	    }
	    
	    return events;
	  });
	}

	function slowcat(patterns) {
	  if (patterns.length) {
	    var
	        ps = patterns.map(function(p) {
	          return p.splitAtSam();
	        }),
	        l = ps.length,
	        f = function(s, e) {
	          var r = Math.floor(s),
	              n = r % l,
	              offset = r - ((r - n) / l),
	              aprime = [
	                Fraction(s - offset),
	                Fraction(e - offset)
	              ],
	              p = ps[n];          
	      
	          return p.withResultTime(function(x) {
	            return Fraction(x + offset);
	          }).arc(aprime[0], aprime[1]);
	        },
	        pattern = new Pattern(f);
	    
	    return pattern.splitQueries();
	  }    
	  else {
	    return silence();
	  }
	}

	function cat(patterns) {
	  return slowcat(patterns).density(patterns.length);
	}

	Pattern.cat = cat;
	Pattern.slowcat = slowcat;
	Pattern.pure = pure;
	Pattern.silence = silence;
	module.exports = Pattern;


/***/ },
/* 23 */
/***/ function(module, exports) {

	var E;

	E = Event = {
	  start: function eventStart(event) {
	    return event[1][0]; // [ [1/2, 1] ,[ ___ 1/2 ___ , 1], "bd" ]
	  },
	  onset: function eventOnset(event) {
	    return event[0][0]; // [ [ ___ 1/2 ___ , 1] ,[1/2, 1], "bd" ]
	  },
	  offset: function eventOffset(event) {
	    return event[0][1]; // [ [ 1/2 , ___ 1 ___ ] ,[1/2, 1], "bd" ]    
	  },
	  sam :function sam(time) {
	    return Math.floor(time);
	  },
	  nextSam: function nextSam(time) {
	    return Fraction(1 + E.sam(time));
	  }  ,
	  cyclePos: function cyclePos(time) {
	    return Fraction(time - E.sam(time));
	  },

	  arcCycles: function arcCycles(s, e) {
	    if (Fraction(s).compare(Fraction(e)) >= 0) {
	      // s is larger than or the same as e
	      return [];
	    }
	    else if (E.sam(s) == E.sam(e)) {
	      return [[s,e]];
	    }
	    else {
	      return [
	        [s, E.nextSam(s)]
	      ].concat(
	        E.arcCycles(E.nextSam(s), e)
	      );
	    }
	  },

	  mapArc: function mapArc(fn, x) {
	    return [fn(x[0]), fn(x[1])];
	  },

	  mapFst: function mapFst(fn, x) {
	    return [fn(x[0]), x[1], x[2]];
	  },

	  mapSnd:function mapSnd(fn, x) {
	    return [x[0], fn(x[1]), x[2]];
	  },

	  mapThd: function mapThd(fn, x) {
	    return [x[0], x[1], fn(x[2])];
	  },

	  mapFsts: function mapFsts(fn, events) {
	    return events.map(E.mapFst.bind(null, fn));
	  },

	  mapSnds: function mapSnds(fn, events) {
	    return events.map(E.mapSnd.bind(null, fn));
	  },

	  mapThds: function mapThds(fn, events) {
	    return events.map(E.mapThd.bind(null, fn));
	  },

	  mapArcs: function mapArcs(fn, events) {
	    return E.mapFsts(fn, E.mapSnds(fn, events));
	  },

	  showTime:function showTime(time) {
	    return time.d == 1 ? time.n.toString() : time.n + "/" + time.d;
	  },

	  showArc: function showArc(a) {
	    return a.map(E.showTime).join(" ");
	  }
	};


	module.exports = {
	  Event: Event
	};


/***/ },
/* 24 */
/***/ function(module, exports) {

	function parseRhythm(unparsed) {
	  var parsed;
	  try {
	    parsed = $p.run(pExpression, unparsed);
	    return parsed;
	  }
	  catch (e) {
	    return Pattern.silence();
	  }
	}

	function parseInc(ok, err) {
	  return $i.parseInc(pExpression, null, ok, err);
	}

	var Parse = {},
	    pIntOrFloat = $p.label("int-or-float",
	                           $l.chainl1(
	                             $p.next(
	                               $t.character("."),
	                               $p.always(function(x,y) {
	                                 return parseFloat([x, y].join("."));
	                               })
	                             ),
	                             $p.eager($p.many1($t.digit)).map(function(x) {
	                               return parseInt(x.join(""), 10);
	                             })
	                           ).map(function(x) {
	                             return new Pattern(x);
	                           })
	                          ),
	    pVocable = $p.label("string",
	                        $p.eager($p.many1($p.either(
	                          $t.letter,
	                          $t.oneOf("0123456789:.-_")
	                        )))
	                       ).map(function(x) {
	                         return x.join("");
	                       }).map(function(x) {
	                         return new Pattern(x);
	                       }),
	    pRest = $p.label("rest", $t.character("~")).map(function() {
	      return Pattern.silence();
	    }),
	    pSilence = $p.always(Pattern.silence()),
	    pSingle = $p.choice(pIntOrFloat, pVocable, pRest),
	    pPart = $p.late(function() {
	      return $l.then($p.choice(pSingle, pPolyIn), pSpaces).map(function(x) {
	        return x;
	      });
	    }),
	    pPolyIn = $p.late(function() {
	      return $l.then($l.between(
	        $t.character("["), $t.character("]"),
	        $p.eager(
	          $l.sepBy(
	            $t.character(","),
	            pSequence)
	        )
	      ), pSpaces).map(function(list) {
	        return list.reduce(function(self, other) {
	          return self.overlay(other);
	        });
	      });
	    }),
	    
	    pSpaces = $p.many($t.space),

	    pSequence = $p.next(pSpaces, $p.eager($p.many(pPart))).map(function(x) {
	      return Pattern.cat(x);
	    }),

	    pExpression = $p.late(function() {
	      return $p.either(pSequence, pSilence);
	    });



	Parse.pVocable = pVocable;
	Parse.pIntOrFloat = pIntOrFloat;

	Parse.pSingle = pSingle;

	Parse.pPart = pPart;

	Parse.pPolyIn = pPolyIn;

	Parse.pSpaces = pSpaces;
	Parse.pSequence = pSequence;
	Parse.pExpression = pExpression;


	Parse.run = parseRhythm;
	Parse.inc = parseInc;
	Parse.id = id;


	module.exports = Parse;

	// utils

	function id(x) {
	  return x;
	}


/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	/**
	This module's responsibility is to compose
	the user interface with user interactions and
	the underlying loop for pattern playback.

	It has access to the DOM and can manipulate it,
	the audio system and the scheduler.

	It is the event dispatcher hub that will connect
	relevant modules with each other, so they don't have to.
	*/
	var com = __webpack_require__(26),
	    makeDemo = __webpack_require__(27),
	    Loop = __webpack_require__(28),
	    Logic = __webpack_require__(29),
	    Tool = __webpack_require__(30),
	    Sample = __webpack_require__(31),
	    Toolbar = __webpack_require__(35),
	    Orbit = __webpack_require__(36),
	    samples = [
	      "bd",
	      "sn",
	      "cp",
	      "hh",
	      "bass"
	    ];

	// avoid scrolling
	interact(document.body).on("gesturemove", function(e) {
	  e.preventDefault();
	}, false);

	function pollSampleBank(dirt, cb) {
	  if (dirt.sampleBank && dirt.sampleBank.sampleMap) {
	    cb(dirt.sampleBank);
	  }
	  else {
	    setTimeout(function() { pollSampleBank(dirt,cb); }, 1000);    
	  }
	}

	function pollSampleBuffer(bank, name, n, cb) {
	  var buffer = bank.getBuffer(name, n);
	  if (!buffer) {
	    setTimeout(function() { pollSampleBuffer(bank,name,n,cb); }, 1000);
	  }
	  else {
	    cb(buffer);
	  }
	}


	function initializeAudio(dirt, cb) {
	  var audio = dirt.ac,
	      overlay = document.createElement("div"),
	      demo = makeDemo(),
	      prelude = document.createElement("label"),
	      btn = document.createElement("button");

	  prelude.innerText = "or, ";
	  prelude.appendChild(btn);
	  btn.innerText = "wait...";
	  overlay.appendChild(demo);
	  overlay.appendChild(prelude);
	  
	  overlay.id = "page-overlay";
	  overlay.style.position = "absolute";
	  overlay.style.left = 0;
	  overlay.style.top = 0;
	  overlay.style.right = 0;
	  overlay.style.bottom = 0;

	  pollSampleBank(dirt, function(bank) {
	    pollSampleBuffer(bank, "sn", 0, function() {
	      var ibtn = interact(btn);
	      btn.classList.add("loaded");
	      btn.innerText = "tap to play now!";
	      ibtn.on("tap", function(e) {
	        dirt.queue({"sample_name": "sn", "sample_n": 0, when: 0});
	        overlay.remove();
	        e.stopPropagation();
	        cb(dirt, audio);
	      });    
	    });
	  });

	  document.body.appendChild(overlay);
	}


	function init() {
	  try {
	    var dirt = new WebDirt(),
	        root = document.querySelector("body"),
	        tools = samples.map(function(s) {
	          return new Tool(Sample, s);
	        }),
	        size = window.innerWidth + window.outerWidth,
	        threshold = size * 0.1,        
	        loop, orbit, logic;
	    
	    initializeAudio(dirt, function(scheduler, audio) {
	      loop = new Loop(audio, scheduler, 0.2);
	      logic = new Logic(threshold);
	      orbit = new Orbit(logic);

	      orbit.$el.on("pattern:changed", function(e) {
	        loop.changePattern(e.detail);
	      });

	      toolbar = new Toolbar(tools);
	      toolbar.$el.on("node:added", orbit.add.bind(orbit));
	      toolbar.$el.on("node:removed", orbit.remove.bind(orbit));
	      toolbar.$el.on("node:moved", orbit.move.bind(orbit));      
	      
	      orbit.$el.on("tap", function(e) {
	        toolbar.currentTool.invoke(e.x, e.y);

	        e.preventDefault();
	        e.stopPropagation();
	      });
	      root.appendChild(orbit.el);
	      root.appendChild(toolbar.el);
	    });
	  }
	  catch(e) {
	    alert("fluid interaction initialization failed, oops");
	    com.log("[init] error", e);
	  }
	}

	module.exports = init;


/***/ },
/* 26 */
/***/ function(module, exports) {

	module.exports = {
	  log: function log() {
	    // eslint-disable-next-line no-console
	    console.log.apply(console, arguments);
	  },
	  extend: function(obj, props) {
	    Object.keys(props).forEach(function(key) {
	      if (props.propertyIsEnumerable(key)) {
	        obj[key] = props[key];
	      }
	    });
	  },
	  space: function(el) {
	    el.innerHTML += "&nbsp;";
	  },
	  calcDistance: function calcDistance(a, b) {
	    var x1 = a.x(),
	        y1 = a.y(),
	        x2 = b.x(),
	        y2 = b.y(),
	        dist = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	    return dist;
	  },
	  dispatcher: function dispatcher(el, name, options) {
	    return function () {
	      var d = new CustomEvent(name, options || {
	        bubbles: true,
	        cancelable: true
	      });
	      el.dispatchEvent(d);
	    };
	  }  
	};


/***/ },
/* 27 */
/***/ function(module, exports) {

	var demoUrl = "//player.vimeo.com/video/167944454";

	function makeDemo() {
	  var wrapper = document.createElement("div"),
	      prelude = document.createElement("h3"),
	      el = document.createElement("iframe");

	  prelude.innerText = "watch the demo";

	  el.src = demoUrl;
	  el.setAttribute("frameborder", 0);
	  el.setAttribute("allowfullscreen", "");
	  el.setAttribute("webkitallowfullscreen", "");
	  el.setAttribute("mozallowfullscreen", "");
	  el.id = "demo-player";

	  wrapper.appendChild(prelude);
	  wrapper.appendChild(el);
	  
	  return wrapper;
	}


	module.exports = makeDemo;


/***/ },
/* 28 */
/***/ function(module, exports) {

	/**
	This module's responsibility is to schedule events according to a given pattern.

	A loop is continously running.

	It exposes two functions to interact with it:

	`changePattern` to update its internal pattern and 
	continue to produce events for the next cycle 
	that it then sends to the scheduler

	`changeCps` to update its internal tempo data to reflect the change and 
	from then on use this tempo to schedule events.

	Actual playback of sound is done by the scheduler which has to
	supply one function, namely `queue`.

	In this case we rely on this being `WebDirt`
	*/

	function logicalTime(tempo, beat) {
	  var beatDelta = beat - tempo.beat,
	      timeDelta = beatDelta / tempo.cps,
	      changeT = tempo.at;

	  return changeT + timeDelta;
	}

	function logicalOnset(tempo, beat, onset, offset) {
	  var logicalNow = logicalTime(tempo, beat),
	      logicalPeriod = logicalTime(tempo, Math.floor(beat) + 1) - logicalNow;
	  return logicalNow + (logicalPeriod * onset) + offset;
	}

	function beatNow(clock, tempo) {
	  var now = clock.currentTime,
	      delta = now - tempo.at,
	      beatDelta = tempo.cps * delta;

	  return tempo.beat + beatDelta;
	}

	function Loop(audio, scheduler, latency) {
	  this.audio = audio;
	  this.scheduler = scheduler;
	  this.events = [];
	  this.latency = latency;

	  this.tempo = {
	    at: audio.currentTime,
	    beat: 0,
	    cps: 1.0
	  };

	  this.lastSam = Math.floor(this.tempo.beat);

	  this.start();

	  this.pattern = Pattern.silence();
	}

	Loop.prototype = {
	  changePattern: function changePattern(pattern) {
	    this.pattern = pattern;
	  },
	  changeCps: function changeCps() {
	    throw "Not implemented";
	  },
	  play: function() {
	    var self = this,
	        stime, beat, e, v, n;

	    while (self.events.length) {
	      e = self.events.shift();
	      beat = Math.floor(beatNow(self.audio, self.tempo));
	      stime = logicalOnset(self.tempo, beat, e[0], self.latency);
	      v = e[1].split(":");
	      if (v.length > 1) {
	        n = v[1];
	        v = v[0];
	      }
	      else {
	        n = 0;
	        v = v[0];
	      }

	      self.scheduler.queue({
	        "sample_name": v,
	        "sample_n": n,
	        "when": stime
	      });
	      
	    }
	  },
	  update: function(self) {
	    var start = null,
	        now, delta, ocps, obeat, beat;
	    
	    return function(time) {
	      if (!start) { start = time; }
	      now = self.audio.currentTime,
	      delta = now - self.tempo.at,
	      ocps = self.tempo.cps,
	      obeat = self.tempo.beat;
	      beat = (ocps < 0) ? 0 : (obeat + (ocps * delta));

	      self.tempo = {
	        at: now,
	        beat: beat,
	        cps: ocps 
	      };

	      // FIXME: keeps a lookahead of one whole cycle
	      if ((self.lastSam - (self.latency  / self.tempo.cps))
	          < Math.floor(self.tempo.beat)) {
	        self.events = self.events.concat(
	          self.pattern.seqToRelOnsets(
	            self.lastSam,
	            Math.floor(self.lastSam) + 1
	          )
	        );
	        // trigger conversion to float
	        self.lastSam = Math.floor(self.tempo.beat) + 1;
	      }

	      self.play();

	      requestAnimationFrame(self.update(self));
	    };
	  },
	  start: function() {
	    requestAnimationFrame(this.update(this));
	  }  
	};

	module.exports = Loop;


/***/ },
/* 29 */
/***/ function(module, exports) {

	/**
	   This module's responsibility is to convert
	   the state of the user interface into
	   a pattern.

	   It is the bridge between DOM and Pattern

	   It has no access to anything but what it gets as input

	   It does not keep track of anything.

	   It SHOULD be side-effect free.
	*/
	function byDatasetXY(a, b) {
	  var ax = a.x(),
	      bx = b.x();
	  
	  if (ax < bx) {
	    return -1;
	  }
	  else if (ax > bx) {
	    return 1;
	  }
	  else {
	    return 0;
	  }
	}

	function recombine(a) {
	  return a.value();
	}


	function Logic(threshold) {
	  this.proximityThreshold = threshold;
	}

	Logic.prototype = {
	  // instead of recalculation _all_ groups on every movement,
	  // we just calculate the difference,
	  // as we know which node changed
	  calculateChanges: function calculateChanges(nodes, change) {
	    // `nodes` contains the state as it is with the change applied
	    // so this function will rearrange things so that the return
	    // value is a list of operations to be applied to reflect
	    // those changes in DOM

	    var operations = [],
	        node = change.node,
	        inRange = [],
	        outOfRange = [],
	        i;
	    
	    switch (change.type) {
	    case "move":
	      throw "unimplemented move";
	    case "add":
	      for (i = 0; i < nodes.length; i++) {
	        if (nodes[i].collide(node, this.proximityThreshold).collides) {
	          inRange.push(nodes[i]);
	        }
	        else {
	          outOfRange.push(nodes[i]);
	        }
	      }
	      if (inRange.length) {
	        if (inRange.length == 1) {
	          // add node to existing node
	          operations.push(function() {
	            inRange[0].append(node);
	          });
	        }
	        else {
	          throw "unimplemented merging";
	          // merge chains
	          // inRange[0] first, then inRange[1]
	          // since brackets have _size_
	          // do distanceCalc inside brackets again afterwards
	        }
	      }
	      else {
	        // node is disjunct
	        operations.push(function(root) {
	          root.append(node);
	        });
	      }
	      break;
	    case "remove":
	      throw "unimplemented remove";
	    default:
	      throw "Unexpected change" + change.type;
	    }

	    return operations;
	  },  
	  createPatterns: function(chains) {
	    var maps = [], sorted, combined, parsed, j;
	  
	    if (chains.length) {
	      for (j = 0; j < chains.length; j++) {
	        sorted = chains[j].sort(byDatasetXY);

	        combined = sorted.map(recombine).join(" ");
	        parsed = Parse.run(combined);
	        maps.push(parsed);
	      }

	      return maps.reduce(function(a, b) { return a.overlay(b); });
	    }
	    else {
	      return Pattern.silence();
	    }
	  }
	};

	module.exports = Logic;


/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	/**
	provides objects that can be used to create nodes for patterns.

	It accepts a factory to create nodes and content to be 
	added to the node.
	*/

	var com = __webpack_require__(26);

	function Tool(factory, content) {
	  this.factory = factory;
	  this.content = content;

	  this.el = document.createElement("div");
	  this.$el = interact(this.el);

	  this.el.innerHTML = this.content;
	  this.el.classList.add("tool");
	  
	  this.$el.on("tap", com.dispatcher(this.el, "tool:selected", {
	    bubbles: true,
	    cancelable: false,
	    detail: this
	  }));
	}

	Tool.prototype = {
	  deselect: function() {
	    this.el.classList.remove("active");      
	  },
	  select: function() {
	    this.el.classList.add("active");
	  },
	  invoke: function(x, y) {
	    var node = new(this.factory)(this.content, x, y);

	    com.dispatcher(this.el, "node:added", {
	      bubbles: true,
	      cancelable: false,
	      detail: node
	    })();    
	  }
	};

	module.exports = Tool;


/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	var com = __webpack_require__(26),
	    Node = __webpack_require__(32),
	    sampleMethods;

	function Sample() {
	  Node.apply(this, arguments);
	  this.$el.on("tap", this.selectN.bind(this));
	  this.el.classList.add("sample");
	}

	Sample.prototype = Object.create(Node.prototype);

	sampleMethods = {
	  selectN: function selectN(e) {
	    var parent = e.target.parentNode, n = e.target.dataset.n;

	    if (e.target.classList.contains("audio-sample")) {
	      if (n) {
	        e.target.dataset.n = parseInt(n, 10) + 1;
	      }
	      else {
	        e.target.dataset.n = 1;
	      }
	      com.dispatcher(parent, "nodes:changed", {
	        bubbles: true, cancelable: false
	      })();
	    }
	    e.stopPropagation();
	  },
	  replicate: function() {
	    return new Sample(this.el.innerText, this.x(), this.y());
	  }
	};

	com.extend(Sample.prototype, sampleMethods);

	module.exports = Sample;


/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	/**
	   A simple node that provides coordinates, 
	   reflects data into data attributes of a bound DOM element 
	*/
	var com = __webpack_require__(26),
	    Atom = __webpack_require__(33),
	    Chain = __webpack_require__(34),
	    nodeMethods;

	function Node() {
	  Atom.apply(this, arguments);
	  this.$el = interact(this.el);

	  this.$el.draggable(true)
	    .on({
	//      dragstart: this.move.bind(this),
	      dragmove: this.move.bind(this),
	//      dragend: this.move.bind(this),
	      hold: this.remove.bind(this)
	    });
	}

	Node.prototype = Object.create(Atom.prototype);

	nodeMethods = {
	  move: function move(e) {
	    var x = e.dx + parseInt(this.el.dataset.x, 10),
	        y = e.dy + parseInt(this.el.dataset.y, 10);
	    
	    this.el.style.transform = "translate(" + x + "px, " + y + "px)" ;

	    this.el.dataset.x = x;
	    this.el.dataset.y = y;

	    e.stopPropagation();
	    e.preventDefault();
	    
	    com.dispatcher(this.el, "node:moved", {
	      bubbles: true,
	      cancelable: false,
	      detail: this
	    })();
	  },
	  remove: function remove() {
	    var parent = this.el.parentNode;
	    this.el.remove();

	    com.dispatcher(parent, "node:removed", {
	      bubbles: true,
	      cancelable: false,
	      detail: this
	    })();
	  },
	  replicate: function() {
	    return new Node(this.el.innerText, this.x(), this.y());
	  },
	  append: function append(other) {
	    com.log("[original] x:", this.x(), "y:", this.y());    
	    var replica = this.replicate();
	    com.log("[replica] x:", replica.x(), "y:", replica.y());
	    com.log("[other] x:", other.x(), "y:", other.y());        
	    // make a chain from this
	    this.__proto__ = Object.create(Chain.prototype);
	    this.el.className = "";
	    this.el.innerText = "";
	    this.init();
	    com.log("[chain:init] x:", this.x(), "y:", this.y(),
	            "bounds", JSON.stringify(this.bounds));    
	    this.append(replica);
	    com.log("[replica:appended] x:", replica.x(), "y:", replica.y());
	    com.log("[chain:one-child] x:", this.x(), "y:", this.y(),
	            "bounds", JSON.stringify(this.bounds));    
	    this.append(other);
	    com.log("[chain:two-children] x:", this.x(), "y:", this.y(),
	            "bounds", JSON.stringify(this.bounds));        
	    com.log("[other:appended] x:", other.x(), "y:", other.y());    
	  }
	};

	com.extend(Node.prototype, nodeMethods);

	module.exports = Node;


/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	var com = __webpack_require__(26);

	function Atom(content, x, y) {
	  this.el = document.createElement("div");

	  this.el.style.position = "absolute";

	  this.reposition(x, y);
	  this.el.innerHTML = content;
	}

	Atom.prototype = {
	  size: function size() {
	    return 48;
	  },
	  x: function x() {
	    return parseInt(this.el.dataset.x, 10);
	  },
	  y: function y() {
	    return parseInt(this.el.dataset.y, 10);
	  },
	  value: function value() {
	    return this.el.innerText;
	  },
	  collide: function collide(other, threshold) {
	    var dist = com.calcDistance(this, other);

	    if (dist <= threshold) {
	      return {
	        collides: true
	      };
	    }
	    else {          
	      return {
	        collides: false
	      };
	    }
	  },
	  reposition: function reposition(x, y) {
	    this.el.dataset.x = x;
	    this.el.dataset.y = y;
	    this.el.style.transform = "translate(" + x + "px, " + y + "px)";
	  }
	};

	module.exports = Atom;


/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	var Atom = __webpack_require__(33),
	    com = __webpack_require__(26),
	    chainMethods;

	function Chain() {
	  Atom.apply(this, arguments);
	  this.init();
	}


	Chain.prototype = Object.create(Atom.prototype);

	// always remember, innerText includes
	// :before and :after content, therefore chains should
	// add spaces between elements 

	chainMethods = {
	  init: function() {
	    this.children = [];
	    this.el.classList.add("chain");
	    this.bounds = {
	      top: this.y(),
	      right: this.x(),
	      bottom: this.y(),
	      left: this.x()
	    };
	  },
	  append: function append(other) {
	    this.recalculateBounds(other);
	    other.reposition(other.x() - this.x(), other.y() - this.y());
	    this.children.push(other);
	    if (this.children.length > 1) {
	      com.space(this.el);
	    }
	    this.el.appendChild(other.el);
	  },
	  recalculateBounds: function(added) {
	    var self = this,
	        x = added.x(),
	        y = added.y(),
	        changed = false,
	        width, height,
	        newx, newy;
	    
	    if (x > this.bounds.right) {
	      this.bounds.right = x;
	      changed = true;
	    }
	    else if (x < this.bounds.left) {
	      this.bounds.left = x;
	      changed = true;      
	    }

	    if (y > this.bounds.bottom) {
	      this.bounds.bottom = y;
	      changed = true;      
	    }
	    else if (y < this.bounds.top) {
	      this.bounds.top = y;
	      changed = true;      
	    }

	    if (changed) {
	      width = this.bounds.right - this.bounds.left;
	      height = this.bounds.bottom - this.bounds.top;
	      newx = this.bounds.left;
	      newy = this.bounds.top;

	      // reposition existing children to these new bounds
	      this.children.forEach(function(child) {
	        child.reposition(
	          child.x() - (newx - self.x()),
	          child.y() - (newy - self.y())
	        );
	      });
	      
	      this.reposition(
	        newx,
	        newy        
	      );
	      this.el.style.width = width + "px";
	      this.el.style.height = height + "px";
	    }
	  },
	  collide: function collide(other, threshold) {
	    var x = other.x(),
	        y = other.y(),
	        results, result;

	    if (((x + threshold) > this.bounds.left)
	        && ((x - threshold) < this.bounds.right)
	        && ((y + threshold) > this.bounds.top)
	        && ((y - threshold) < this.bounds.bottom)) {
	      // we refine the collision detection
	      results = this.children.map(function(child) {
	        return child.collide(other, threshold);
	      });

	      // remove all children that do NOT collide
	      result = results.shift();
	      
	      while (result && !result.collides) {
	        result = results.shift();
	      }

	      // if there are results left, _other_ collides
	      return { collides: !!results.length };
	    }
	    else {
	      // other is not within bounds
	      return { collides: false };
	    }
	  }
	};

	com.extend(Chain.prototype, chainMethods);

	module.exports = Chain;



/***/ },
/* 35 */
/***/ function(module, exports) {

	/**
	The toolbar is a widget that appears on one side of 
	the screen.

	It should adapt its width to the tools it contains and
	the orientation and size of the display.

	It should allow hiding to keep real estate for interaction.

	It should reflect the currently selected tool.

	*/
	function Toolbar(tools) {
	  var self = this;
	  this.el = document.createElement("aside");
	  this.el.classList.add("toolbar");
	  this.el.classList.add("active");
	  this.$el = interact(this.el);
	  this.$el.draggable({
	    onend: function(e) {
	      if (Math.abs(e.velocityX) > 250) {
	        self.el.classList.toggle("active");
	      }
	    }
	  }).on("tool:selected", this.change.bind(this));

	  this.tools = tools;

	  this.tools.forEach(function(t) {
	    self.add(t);
	  });
	}

	Toolbar.prototype = {
	  add: function add(tool) {
	    this.el.appendChild(tool.el);
	  },
	  change: function(e) {
	    var tool = e.detail;
	    this.tools.forEach(function(t) {
	      t.deselect();
	    });

	    tool.select();
	    
	    this.currentTool = tool;
	  }  
	};

	module.exports = Toolbar;


/***/ },
/* 36 */
/***/ function(module, exports) {

	//var com = require("./common.js");

	function Orbit(logic) {
	  this.el = document.createElement("article");
	  this.$el = interact(this.el);

	  this.el.classList.add("orbit");

	  // this.el.appendChild(com.leftBracket);
	  // be sure to insert new nodes before closing bracket
	  

	  this.logic = logic;
	  this.nodes = [];
	  

	  this.groups = [];
	}

	Orbit.prototype = {
	  add: function(e) {
	    var node = e.detail;

	    this.change({
	      type: "add",
	      node: node
	    });
	  },
	  move: function(e) {
	    var node = e.detail;

	    this.change({
	      type: "move",
	      node: node
	    });
	  },
	  remove: function(e) {
	    var node = e.detail;

	    this.change({
	      type: "remove",
	      node: node
	    });
	  },
	  change: function(detail) {
	    var operations, self = this;
	    operations = this.logic.calculateChanges(this.nodes, detail);

	    operations.forEach(function(op) {
	      op(self);
	    });

	    // com.dispatcher(this.el, "chains:changed", {
	    //   bubbles: true,
	    //   cancelable: false,
	    //   detail: groups
	    // });

	//    pattern = this.logic.createPatterns(this.);
	    
	    // com.dispatcher(this.el, "pattern:changed", {
	    //   bubbles: true,
	    //   cancelable: false,
	    //   detail: pattern
	    // })();
	  },
	  append: function(node) {
	    this.nodes.push(node);
	    this.el.appendChild(node.el);
	  }
	};

	module.exports = Orbit;


/***/ }
/******/ ]);