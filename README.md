# fluid interaction

create and transform beat patterns with multitouch

a running instance of this can be found at [beats.zerkabelt.de](http://beats.zerkabelt.de). though this might not be the latest version, it should be stable

roughly based on the livecoding pattern dsl [Tidalcycles](http://tidal.lurk.org) and [hstexture](https://github.com/yaxu/hstexture) by [Alex McLean](http://yaxu.org) and others

## Installation

__Note: __ These instructions are for developers and assume a knowledge about installing software via command line. If this is not your cup of tea, head over to the [online version](http://beats.zerkabelt.de)

Requirements are:

- `git` (obviously to get the source and dependencies)
- `nodejs` (for sanitizing both css and js and bundling everything into single files)
- `npm` and globally installed `webpack` and `bower` for frontend dependencies and packing

On debianesque distros do:

```
apt-get install nodejs npm git-core
```

for OSX with homebrew do:

```
brew install node
```

then do:

```
npm i -g bower webpack
git clone https://bitbucket.org/LMAA/product
cd product
git submodule update --init --recursive
npm i
bower i
webpack -w
```

you are now watching the source code for changes which get recompiled and put into `index.html` which includes `bundle.js`. CSS styles are inlined into `index.html`.

Fire up a simple web server pointing to the root of the cloned code (e.g. via `python -m SimpleHTTPServer`) and then visit the corresponding webpage in a browser (in case of python, http://0.0.0.0:8000).

Changes are livereloaded in the browser. To test multitouch it might be feasible to emulate touch gestures as supported by at least Google's Chrome.

Code is separated into:

- `scripts/entry.js` requires everything else, sets up globals
- `scripts/dsl.js` defines a parser for simple expressions built in the UI
- `scripts/pattern.js` defines a Pattern object, that allows combination of simple patterns like "bd" into complex patterns like "bd ~ [sn sn, cp cp cp]" as well as methods to convert an opaque pattern into a list of events
- `scripts/time.js` defines helper functions for working with rational time defined as arcs spanning from a `begin` to an `end`
- `scripts/main.js` defines everything related to UI and UX (messy)
- `css/main.css` defines styles for states of UI objects and general layout

feel free to send bug reports or feature request here on bitbucket

