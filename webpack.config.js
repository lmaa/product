var LiveReloadPlugin = require('webpack-livereload-plugin');
var HTMLPlugin = require("html-webpack-plugin");
var colorguard = require("colorguard");
var colorfn = require("postcss-color-function");
var precss = require("precss");
var autoprefixer = require("autoprefixer");
var path = require('path');

module.exports = {
  entry: "./scripts/entry.js",
  output: {
    path: path.resolve("."),
    filename: "bundle.js"
  },
  resolve: {
    root: [
      path.resolve("./scripts")
    ],
    modulesDirectories: [
      'scripts',
      'scripts/bennu/dist',
      'scripts/bennu/dependencies/',
      'bower_components'
    ],
    alias: {
      'nu-stream/stream': 'nu/dist/stream.js',
      'nu-stream/gen': 'nu/dist/gen.js',
      'seshet': 'seshet/dist/seshet.js'
    }
  },
  module: {
    preLoaders: [
      { test: /\.jsx?$/, loader: 'eslint' }
    ],
    loaders: [      
      {
        test: /\.css$/,
        loader: "style-loader!css-loader!postcss-loader"
      }
    ]
  },
  postcss: function() {
    return [precss, colorfn, autoprefixer, colorguard]
  },
  eslint: {
    failOnWarning: false,
    failOnError: true
  },
  plugins: [
    new HTMLPlugin({
      template: 'template.html'
    }),
    new LiveReloadPlugin({
      appendScriptTag: true
    })
  ]
};

