require("../css/main.css");
window.Fraction = require("fraction.js/fraction.js");
window.interact = require("interact/interact.js");
window.$p = require("./bennu/dist/parse.js");
window.$t = require("./bennu/dist/text.js");
window.$l = require("./bennu/dist/lang.js");
window.$i = require("./bennu/dist/incremental.js");


require("script!./WebDirt/Graph.js");
require("./WebDirt/SampleBank.js");
require("./WebDirt/WebDirt.js");
require("customevent-polyfill/customevent-polyfill.js");
window.nu = require("nu-stream/stream");
window.Pattern = require("pattern.js");
var T = require("./time.js");
window.Event = T.Event;
window.Parse = require("./dsl.js");

require("./main.js")();
