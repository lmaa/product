var E;

E = Event = {
  start: function eventStart(event) {
    return event[1][0]; // [ [1/2, 1] ,[ ___ 1/2 ___ , 1], "bd" ]
  },
  onset: function eventOnset(event) {
    return event[0][0]; // [ [ ___ 1/2 ___ , 1] ,[1/2, 1], "bd" ]
  },
  offset: function eventOffset(event) {
    return event[0][1]; // [ [ 1/2 , ___ 1 ___ ] ,[1/2, 1], "bd" ]    
  },
  sam :function sam(time) {
    return Math.floor(time);
  },
  nextSam: function nextSam(time) {
    return Fraction(1 + E.sam(time));
  }  ,
  cyclePos: function cyclePos(time) {
    return Fraction(time - E.sam(time));
  },

  arcCycles: function arcCycles(s, e) {
    if (Fraction(s).compare(Fraction(e)) >= 0) {
      // s is larger than or the same as e
      return [];
    }
    else if (E.sam(s) == E.sam(e)) {
      return [[s,e]];
    }
    else {
      return [
        [s, E.nextSam(s)]
      ].concat(
        E.arcCycles(E.nextSam(s), e)
      );
    }
  },

  mapArc: function mapArc(fn, x) {
    return [fn(x[0]), fn(x[1])];
  },

  mapFst: function mapFst(fn, x) {
    return [fn(x[0]), x[1], x[2]];
  },

  mapSnd:function mapSnd(fn, x) {
    return [x[0], fn(x[1]), x[2]];
  },

  mapThd: function mapThd(fn, x) {
    return [x[0], x[1], fn(x[2])];
  },

  mapFsts: function mapFsts(fn, events) {
    return events.map(E.mapFst.bind(null, fn));
  },

  mapSnds: function mapSnds(fn, events) {
    return events.map(E.mapSnd.bind(null, fn));
  },

  mapThds: function mapThds(fn, events) {
    return events.map(E.mapThd.bind(null, fn));
  },

  mapArcs: function mapArcs(fn, events) {
    return E.mapFsts(fn, E.mapSnds(fn, events));
  },

  showTime:function showTime(time) {
    return time.d == 1 ? time.n.toString() : time.n + "/" + time.d;
  },

  showArc: function showArc(a) {
    return a.map(E.showTime).join(" ");
  }
};


module.exports = {
  Event: Event
};
