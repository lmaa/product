var demoUrl = "//player.vimeo.com/video/167944454";

function makeDemo() {
  var wrapper = document.createElement("div"),
      prelude = document.createElement("h3"),
      el = document.createElement("iframe");

  prelude.innerText = "watch the demo";

  el.src = demoUrl;
  el.setAttribute("frameborder", 0);
  el.setAttribute("allowfullscreen", "");
  el.setAttribute("webkitallowfullscreen", "");
  el.setAttribute("mozallowfullscreen", "");
  el.id = "demo-player";

  wrapper.appendChild(prelude);
  wrapper.appendChild(el);
  
  return wrapper;
}


module.exports = makeDemo;
