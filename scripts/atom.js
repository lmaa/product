var com = require("./common.js");

function Atom(content, x, y) {
  this.el = document.createElement("div");

  this.el.style.position = "absolute";

  this.reposition(x, y);
  this.el.innerHTML = content;
}

Atom.prototype = {
  size: function size() {
    return 48;
  },
  x: function x() {
    return parseInt(this.el.dataset.x, 10);
  },
  y: function y() {
    return parseInt(this.el.dataset.y, 10);
  },
  value: function value() {
    return this.el.innerText;
  },
  collide: function collide(other, threshold) {
    var dist = com.calcDistance(this, other);

    if (dist <= threshold) {
      return {
        collides: true
      };
    }
    else {          
      return {
        collides: false
      };
    }
  },
  reposition: function reposition(x, y) {
    this.el.dataset.x = x;
    this.el.dataset.y = y;
    this.el.style.transform = "translate(" + x + "px, " + y + "px)";
  }
};

module.exports = Atom;
