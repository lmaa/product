/**
provides objects that can be used to create nodes for patterns.

It accepts a factory to create nodes and content to be 
added to the node.
*/

var com = require("./common.js");

function Tool(factory, content) {
  this.factory = factory;
  this.content = content;

  this.el = document.createElement("div");
  this.$el = interact(this.el);

  this.el.innerHTML = this.content;
  this.el.classList.add("tool");
  
  this.$el.on("tap", com.dispatcher(this.el, "tool:selected", {
    bubbles: true,
    cancelable: false,
    detail: this
  }));
}

Tool.prototype = {
  deselect: function() {
    this.el.classList.remove("active");      
  },
  select: function() {
    this.el.classList.add("active");
  },
  invoke: function(x, y) {
    var node = new(this.factory)(this.content, x, y);

    com.dispatcher(this.el, "node:added", {
      bubbles: true,
      cancelable: false,
      detail: node
    })();    
  }
};

module.exports = Tool;
