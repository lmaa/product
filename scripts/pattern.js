function Pattern(arc) {
  if (typeof arc === "function") {
    this.arc = arc;
  }
  else {
    this.arc = pure(arc).arc;
  }
}

var patternMethods = {
  withQueryArc: function withQueryArc(fn) {
    var pattern = this;
    return new Pattern(function(s, e) {
      return pattern.arc.apply(pattern, fn([s, e]));
    });
  },
  withQueryTime: function withQueryTime(fn) {
    return this.withQueryArc(Event.mapArc.bind(null, fn));
  },
  withResultArc: function withResultArc(fn) {
    var pattern = this;
    return new Pattern(function(s,e) {
      return Event.mapArcs(fn, pattern.arc(s, e));
    });
  },
  withResultTime: function withResultTime(fn) {
    return this.withResultArc(Event.mapArc.bind(null, fn));
  },
  overlay: function overlay(p) {
    var self = this;
    return new Pattern(function(s, e) {
      return self.arc(s, e).concat(p.arc(s, e));
    });
  },
  toString: function toString() {
    var events = this.arc(0,1);
    return events.map(function(e) {
      return "(" + e[2] + " " + Event.showArc(e[0]) + ")";
    }).join(" ");
  },
  splitAtSam: function splitAtSam() {
    var self = this,
        trimArc = function(sprime, s, e) {
          return [
            Fraction(Math.max(sprime, s)),
            Fraction(Math.min(sprime + 1, e))
          ];
        },
        p = new Pattern(function (s,e) {
          return Event.mapSnds(
            trimArc(Event.sam(s)),
            self.arc(s,e)
          );
        });
    
    return this.splitQueries(p);
  },
  splitQueries: function splitQueries() {
    var self = this;
    return new Pattern(function(s,e) {
      return Event.arcCycles(s, e).reduce(function(events, a) {
        return events.concat(self.arc(a[0], a[1]));
      }, []);
    });
  },
  density: function density(time) {
    switch (time) {
    case 0:
      return silence();
    case 1:
      return this;
    default:
      return (this.withQueryTime(function(x) {
        return Fraction(x * time);
      })).withResultTime(function(x) {
        return Fraction(x / time);
      });
    }
  },
  filterStartInRange: function filterStartInRange() {
    var self = this;
    return new Pattern(function(s,e) {
      return self.arc(s,e).filter(function(event) {
        return (Event.onset(event) < s);
      });
    });
  },
  filterOnsets: function filterOnsets() {
    var self = this;
    return new Pattern(function(s,e) {
      return self.arc(s,e).filter(function(event) {
        return (Event.onset(event) >= Event.start(event));
      });
    });
  },
  filterOnsetsInRange: function filterOnsetsInRange() {
    return this.filterOnsets(this.filterStartInRange);
  },
  // does not produce a Pattern
  seqToRelOnsets: function seqToRelOnsets(s, e) {
    return this.filterOnsetsInRange().arc(s, e).map(function(event) {
      var sprime = Event.onset(event),
          x = event[2];
      return [ (sprime - s) / (e - s), x ];
    });
  }
  
};

Object.keys(patternMethods).forEach(function(key) {
  Pattern.prototype[key] = patternMethods[key];
});

function silence() {
  return new Pattern(function() { return []; });
}

function pure(value) {
  return new Pattern(function(s, e) {
    var events = [], i;

    for (i = Math.floor(s); i < Math.ceil(e); i++) {
      events.push([
        [Fraction(i, 1), Fraction(i + 1, 1)],
        [Fraction(i, 1), Fraction(i + 1, 1)],
        value
      ]);
    }
    
    return events;
  });
}

function slowcat(patterns) {
  if (patterns.length) {
    var
        ps = patterns.map(function(p) {
          return p.splitAtSam();
        }),
        l = ps.length,
        f = function(s, e) {
          var r = Math.floor(s),
              n = r % l,
              offset = r - ((r - n) / l),
              aprime = [
                Fraction(s - offset),
                Fraction(e - offset)
              ],
              p = ps[n];          
      
          return p.withResultTime(function(x) {
            return Fraction(x + offset);
          }).arc(aprime[0], aprime[1]);
        },
        pattern = new Pattern(f);
    
    return pattern.splitQueries();
  }    
  else {
    return silence();
  }
}

function cat(patterns) {
  return slowcat(patterns).density(patterns.length);
}

Pattern.cat = cat;
Pattern.slowcat = slowcat;
Pattern.pure = pure;
Pattern.silence = silence;
module.exports = Pattern;
