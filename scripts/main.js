/**
This module's responsibility is to compose
the user interface with user interactions and
the underlying loop for pattern playback.

It has access to the DOM and can manipulate it,
the audio system and the scheduler.

It is the event dispatcher hub that will connect
relevant modules with each other, so they don't have to.
*/
var com = require("./common.js"),
    makeDemo = require("./demo.js"),
    Loop = require("./loop.js"),
    Logic = require("./logic.js"),
    Tool = require("./tool.js"),
    Sample = require("./sample.js"),
    Toolbar = require("./toolbar.js"),
    Orbit = require("./orbit.js"),
    samples = [
      "bd",
      "sn",
      "cp",
      "hh",
      "bass"
    ];

// avoid scrolling
interact(document.body).on("gesturemove", function(e) {
  e.preventDefault();
}, false);

function pollSampleBank(dirt, cb) {
  if (dirt.sampleBank && dirt.sampleBank.sampleMap) {
    cb(dirt.sampleBank);
  }
  else {
    setTimeout(function() { pollSampleBank(dirt,cb); }, 1000);    
  }
}

function pollSampleBuffer(bank, name, n, cb) {
  var buffer = bank.getBuffer(name, n);
  if (!buffer) {
    setTimeout(function() { pollSampleBuffer(bank,name,n,cb); }, 1000);
  }
  else {
    cb(buffer);
  }
}


function initializeAudio(dirt, cb) {
  var audio = dirt.ac,
      overlay = document.createElement("div"),
      demo = makeDemo(),
      prelude = document.createElement("label"),
      btn = document.createElement("button");

  prelude.innerText = "or, ";
  prelude.appendChild(btn);
  btn.innerText = "wait...";
  overlay.appendChild(demo);
  overlay.appendChild(prelude);
  
  overlay.id = "page-overlay";
  overlay.style.position = "absolute";
  overlay.style.left = 0;
  overlay.style.top = 0;
  overlay.style.right = 0;
  overlay.style.bottom = 0;

  pollSampleBank(dirt, function(bank) {
    pollSampleBuffer(bank, "sn", 0, function() {
      var ibtn = interact(btn);
      btn.classList.add("loaded");
      btn.innerText = "tap to play now!";
      ibtn.on("tap", function(e) {
        dirt.queue({"sample_name": "sn", "sample_n": 0, when: 0});
        overlay.remove();
        e.stopPropagation();
        cb(dirt, audio);
      });    
    });
  });

  document.body.appendChild(overlay);
}


function init() {
  try {
    var dirt = new WebDirt(),
        root = document.querySelector("body"),
        tools = samples.map(function(s) {
          return new Tool(Sample, s);
        }),
        size = window.innerWidth + window.outerWidth,
        threshold = size * 0.1,        
        loop, orbit, logic;
    
    initializeAudio(dirt, function(scheduler, audio) {
      loop = new Loop(audio, scheduler, 0.2);
      logic = new Logic(threshold);
      orbit = new Orbit(logic);

      orbit.$el.on("pattern:changed", function(e) {
        loop.changePattern(e.detail);
      });

      toolbar = new Toolbar(tools);
      toolbar.$el.on("node:added", orbit.add.bind(orbit));
      toolbar.$el.on("node:removed", orbit.remove.bind(orbit));
      toolbar.$el.on("node:moved", orbit.move.bind(orbit));      
      
      orbit.$el.on("tap", function(e) {
        toolbar.currentTool.invoke(e.x, e.y);

        e.preventDefault();
        e.stopPropagation();
      });
      root.appendChild(orbit.el);
      root.appendChild(toolbar.el);
    });
  }
  catch(e) {
    alert("fluid interaction initialization failed, oops");
    com.log("[init] error", e);
  }
}

module.exports = init;
