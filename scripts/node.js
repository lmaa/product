/**
   A simple node that provides coordinates, 
   reflects data into data attributes of a bound DOM element 
*/
var com = require("./common.js"),
    Atom = require("./atom.js"),
    Chain = require("./chain.js"),
    nodeMethods;

function Node() {
  Atom.apply(this, arguments);
  this.$el = interact(this.el);

  this.$el.draggable(true)
    .on({
//      dragstart: this.move.bind(this),
      dragmove: this.move.bind(this),
//      dragend: this.move.bind(this),
      hold: this.remove.bind(this)
    });
}

Node.prototype = Object.create(Atom.prototype);

nodeMethods = {
  move: function move(e) {
    var x = e.dx + parseInt(this.el.dataset.x, 10),
        y = e.dy + parseInt(this.el.dataset.y, 10);
    
    this.el.style.transform = "translate(" + x + "px, " + y + "px)" ;

    this.el.dataset.x = x;
    this.el.dataset.y = y;

    e.stopPropagation();
    e.preventDefault();
    
    com.dispatcher(this.el, "node:moved", {
      bubbles: true,
      cancelable: false,
      detail: this
    })();
  },
  remove: function remove() {
    var parent = this.el.parentNode;
    this.el.remove();

    com.dispatcher(parent, "node:removed", {
      bubbles: true,
      cancelable: false,
      detail: this
    })();
  },
  replicate: function() {
    return new Node(this.el.innerText, this.x(), this.y());
  },
  append: function append(other) {
    com.log("[original] x:", this.x(), "y:", this.y());    
    var replica = this.replicate();
    com.log("[replica] x:", replica.x(), "y:", replica.y());
    com.log("[other] x:", other.x(), "y:", other.y());        
    // make a chain from this
    this.__proto__ = Object.create(Chain.prototype);
    this.el.className = "";
    this.el.innerText = "";
    this.init();
    com.log("[chain:init] x:", this.x(), "y:", this.y(),
            "bounds", JSON.stringify(this.bounds));    
    this.append(replica);
    com.log("[replica:appended] x:", replica.x(), "y:", replica.y());
    com.log("[chain:one-child] x:", this.x(), "y:", this.y(),
            "bounds", JSON.stringify(this.bounds));    
    this.append(other);
    com.log("[chain:two-children] x:", this.x(), "y:", this.y(),
            "bounds", JSON.stringify(this.bounds));        
    com.log("[other:appended] x:", other.x(), "y:", other.y());    
  }
};

com.extend(Node.prototype, nodeMethods);

module.exports = Node;
