/**
   This module's responsibility is to convert
   the state of the user interface into
   a pattern.

   It is the bridge between DOM and Pattern

   It has no access to anything but what it gets as input

   It does not keep track of anything.

   It SHOULD be side-effect free.
*/
function byDatasetXY(a, b) {
  var ax = a.x(),
      bx = b.x();
  
  if (ax < bx) {
    return -1;
  }
  else if (ax > bx) {
    return 1;
  }
  else {
    return 0;
  }
}

function recombine(a) {
  return a.value();
}


function Logic(threshold) {
  this.proximityThreshold = threshold;
}

Logic.prototype = {
  // instead of recalculation _all_ groups on every movement,
  // we just calculate the difference,
  // as we know which node changed
  calculateChanges: function calculateChanges(nodes, change) {
    // `nodes` contains the state as it is with the change applied
    // so this function will rearrange things so that the return
    // value is a list of operations to be applied to reflect
    // those changes in DOM

    var operations = [],
        node = change.node,
        inRange = [],
        outOfRange = [],
        i;
    
    switch (change.type) {
    case "move":
      throw "unimplemented move";
    case "add":
      for (i = 0; i < nodes.length; i++) {
        if (nodes[i].collide(node, this.proximityThreshold).collides) {
          inRange.push(nodes[i]);
        }
        else {
          outOfRange.push(nodes[i]);
        }
      }
      if (inRange.length) {
        if (inRange.length == 1) {
          // add node to existing node
          operations.push(function() {
            inRange[0].append(node);
          });
        }
        else {
          throw "unimplemented merging";
          // merge chains
          // inRange[0] first, then inRange[1]
          // since brackets have _size_
          // do distanceCalc inside brackets again afterwards
        }
      }
      else {
        // node is disjunct
        operations.push(function(root) {
          root.append(node);
        });
      }
      break;
    case "remove":
      throw "unimplemented remove";
    default:
      throw "Unexpected change" + change.type;
    }

    return operations;
  },  
  createPatterns: function(chains) {
    var maps = [], sorted, combined, parsed, j;
  
    if (chains.length) {
      for (j = 0; j < chains.length; j++) {
        sorted = chains[j].sort(byDatasetXY);

        combined = sorted.map(recombine).join(" ");
        parsed = Parse.run(combined);
        maps.push(parsed);
      }

      return maps.reduce(function(a, b) { return a.overlay(b); });
    }
    else {
      return Pattern.silence();
    }
  }
};

module.exports = Logic;
