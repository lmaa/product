function parseRhythm(unparsed) {
  var parsed;
  try {
    parsed = $p.run(pExpression, unparsed);
    return parsed;
  }
  catch (e) {
    return Pattern.silence();
  }
}

function parseInc(ok, err) {
  return $i.parseInc(pExpression, null, ok, err);
}

var Parse = {},
    pIntOrFloat = $p.label("int-or-float",
                           $l.chainl1(
                             $p.next(
                               $t.character("."),
                               $p.always(function(x,y) {
                                 return parseFloat([x, y].join("."));
                               })
                             ),
                             $p.eager($p.many1($t.digit)).map(function(x) {
                               return parseInt(x.join(""), 10);
                             })
                           ).map(function(x) {
                             return new Pattern(x);
                           })
                          ),
    pVocable = $p.label("string",
                        $p.eager($p.many1($p.either(
                          $t.letter,
                          $t.oneOf("0123456789:.-_")
                        )))
                       ).map(function(x) {
                         return x.join("");
                       }).map(function(x) {
                         return new Pattern(x);
                       }),
    pRest = $p.label("rest", $t.character("~")).map(function() {
      return Pattern.silence();
    }),
    pSilence = $p.always(Pattern.silence()),
    pSingle = $p.choice(pIntOrFloat, pVocable, pRest),
    pPart = $p.late(function() {
      return $l.then($p.choice(pSingle, pPolyIn), pSpaces).map(function(x) {
        return x;
      });
    }),
    pPolyIn = $p.late(function() {
      return $l.then($l.between(
        $t.character("["), $t.character("]"),
        $p.eager(
          $l.sepBy(
            $t.character(","),
            pSequence)
        )
      ), pSpaces).map(function(list) {
        return list.reduce(function(self, other) {
          return self.overlay(other);
        });
      });
    }),
    
    pSpaces = $p.many($t.space),

    pSequence = $p.next(pSpaces, $p.eager($p.many(pPart))).map(function(x) {
      return Pattern.cat(x);
    }),

    pExpression = $p.late(function() {
      return $p.either(pSequence, pSilence);
    });



Parse.pVocable = pVocable;
Parse.pIntOrFloat = pIntOrFloat;

Parse.pSingle = pSingle;

Parse.pPart = pPart;

Parse.pPolyIn = pPolyIn;

Parse.pSpaces = pSpaces;
Parse.pSequence = pSequence;
Parse.pExpression = pExpression;


Parse.run = parseRhythm;
Parse.inc = parseInc;
Parse.id = id;


module.exports = Parse;

// utils

function id(x) {
  return x;
}
