/**
This module's responsibility is to schedule events according to a given pattern.

A loop is continously running.

It exposes two functions to interact with it:

`changePattern` to update its internal pattern and 
continue to produce events for the next cycle 
that it then sends to the scheduler

`changeCps` to update its internal tempo data to reflect the change and 
from then on use this tempo to schedule events.

Actual playback of sound is done by the scheduler which has to
supply one function, namely `queue`.

In this case we rely on this being `WebDirt`
*/

function logicalTime(tempo, beat) {
  var beatDelta = beat - tempo.beat,
      timeDelta = beatDelta / tempo.cps,
      changeT = tempo.at;

  return changeT + timeDelta;
}

function logicalOnset(tempo, beat, onset, offset) {
  var logicalNow = logicalTime(tempo, beat),
      logicalPeriod = logicalTime(tempo, Math.floor(beat) + 1) - logicalNow;
  return logicalNow + (logicalPeriod * onset) + offset;
}

function beatNow(clock, tempo) {
  var now = clock.currentTime,
      delta = now - tempo.at,
      beatDelta = tempo.cps * delta;

  return tempo.beat + beatDelta;
}

function Loop(audio, scheduler, latency) {
  this.audio = audio;
  this.scheduler = scheduler;
  this.events = [];
  this.latency = latency;

  this.tempo = {
    at: audio.currentTime,
    beat: 0,
    cps: 1.0
  };

  this.lastSam = Math.floor(this.tempo.beat);

  this.start();

  this.pattern = Pattern.silence();
}

Loop.prototype = {
  changePattern: function changePattern(pattern) {
    this.pattern = pattern;
  },
  changeCps: function changeCps() {
    throw "Not implemented";
  },
  play: function() {
    var self = this,
        stime, beat, e, v, n;

    while (self.events.length) {
      e = self.events.shift();
      beat = Math.floor(beatNow(self.audio, self.tempo));
      stime = logicalOnset(self.tempo, beat, e[0], self.latency);
      v = e[1].split(":");
      if (v.length > 1) {
        n = v[1];
        v = v[0];
      }
      else {
        n = 0;
        v = v[0];
      }

      self.scheduler.queue({
        "sample_name": v,
        "sample_n": n,
        "when": stime
      });
      
    }
  },
  update: function(self) {
    var start = null,
        now, delta, ocps, obeat, beat;
    
    return function(time) {
      if (!start) { start = time; }
      now = self.audio.currentTime,
      delta = now - self.tempo.at,
      ocps = self.tempo.cps,
      obeat = self.tempo.beat;
      beat = (ocps < 0) ? 0 : (obeat + (ocps * delta));

      self.tempo = {
        at: now,
        beat: beat,
        cps: ocps 
      };

      // FIXME: keeps a lookahead of one whole cycle
      if ((self.lastSam - (self.latency  / self.tempo.cps))
          < Math.floor(self.tempo.beat)) {
        self.events = self.events.concat(
          self.pattern.seqToRelOnsets(
            self.lastSam,
            Math.floor(self.lastSam) + 1
          )
        );
        // trigger conversion to float
        self.lastSam = Math.floor(self.tempo.beat) + 1;
      }

      self.play();

      requestAnimationFrame(self.update(self));
    };
  },
  start: function() {
    requestAnimationFrame(this.update(this));
  }  
};

module.exports = Loop;
