var com = require("./common.js"),
    Node = require("./node.js"),
    sampleMethods;

function Sample() {
  Node.apply(this, arguments);
  this.$el.on("tap", this.selectN.bind(this));
  this.el.classList.add("sample");
}

Sample.prototype = Object.create(Node.prototype);

sampleMethods = {
  selectN: function selectN(e) {
    var parent = e.target.parentNode, n = e.target.dataset.n;

    if (e.target.classList.contains("audio-sample")) {
      if (n) {
        e.target.dataset.n = parseInt(n, 10) + 1;
      }
      else {
        e.target.dataset.n = 1;
      }
      com.dispatcher(parent, "nodes:changed", {
        bubbles: true, cancelable: false
      })();
    }
    e.stopPropagation();
  },
  replicate: function() {
    return new Sample(this.el.innerText, this.x(), this.y());
  }
};

com.extend(Sample.prototype, sampleMethods);

module.exports = Sample;
