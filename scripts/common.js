module.exports = {
  log: function log() {
    // eslint-disable-next-line no-console
    console.log.apply(console, arguments);
  },
  extend: function(obj, props) {
    Object.keys(props).forEach(function(key) {
      if (props.propertyIsEnumerable(key)) {
        obj[key] = props[key];
      }
    });
  },
  space: function(el) {
    el.innerHTML += "&nbsp;";
  },
  calcDistance: function calcDistance(a, b) {
    var x1 = a.x(),
        y1 = a.y(),
        x2 = b.x(),
        y2 = b.y(),
        dist = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    return dist;
  },
  dispatcher: function dispatcher(el, name, options) {
    return function () {
      var d = new CustomEvent(name, options || {
        bubbles: true,
        cancelable: true
      });
      el.dispatchEvent(d);
    };
  }  
};
