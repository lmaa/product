/**
The toolbar is a widget that appears on one side of 
the screen.

It should adapt its width to the tools it contains and
the orientation and size of the display.

It should allow hiding to keep real estate for interaction.

It should reflect the currently selected tool.

*/
function Toolbar(tools) {
  var self = this;
  this.el = document.createElement("aside");
  this.el.classList.add("toolbar");
  this.el.classList.add("active");
  this.$el = interact(this.el);
  this.$el.draggable({
    onend: function(e) {
      if (Math.abs(e.velocityX) > 250) {
        self.el.classList.toggle("active");
      }
    }
  }).on("tool:selected", this.change.bind(this));

  this.tools = tools;

  this.tools.forEach(function(t) {
    self.add(t);
  });
}

Toolbar.prototype = {
  add: function add(tool) {
    this.el.appendChild(tool.el);
  },
  change: function(e) {
    var tool = e.detail;
    this.tools.forEach(function(t) {
      t.deselect();
    });

    tool.select();
    
    this.currentTool = tool;
  }  
};

module.exports = Toolbar;
