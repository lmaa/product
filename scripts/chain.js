var Atom = require("./atom.js"),
    com = require("./common.js"),
    chainMethods;

function Chain() {
  Atom.apply(this, arguments);
  this.init();
}


Chain.prototype = Object.create(Atom.prototype);

// always remember, innerText includes
// :before and :after content, therefore chains should
// add spaces between elements 

chainMethods = {
  init: function() {
    this.children = [];
    this.el.classList.add("chain");
    this.bounds = {
      top: this.y(),
      right: this.x(),
      bottom: this.y(),
      left: this.x()
    };
  },
  append: function append(other) {
    this.recalculateBounds(other);
    other.reposition(other.x() - this.x(), other.y() - this.y());
    this.children.push(other);
    if (this.children.length > 1) {
      com.space(this.el);
    }
    this.el.appendChild(other.el);
  },
  recalculateBounds: function(added) {
    var self = this,
        x = added.x(),
        y = added.y(),
        changed = false,
        width, height,
        newx, newy;
    
    if (x > this.bounds.right) {
      this.bounds.right = x;
      changed = true;
    }
    else if (x < this.bounds.left) {
      this.bounds.left = x;
      changed = true;      
    }

    if (y > this.bounds.bottom) {
      this.bounds.bottom = y;
      changed = true;      
    }
    else if (y < this.bounds.top) {
      this.bounds.top = y;
      changed = true;      
    }

    if (changed) {
      width = this.bounds.right - this.bounds.left;
      height = this.bounds.bottom - this.bounds.top;
      newx = this.bounds.left;
      newy = this.bounds.top;

      // reposition existing children to these new bounds
      this.children.forEach(function(child) {
        child.reposition(
          child.x() - (newx - self.x()),
          child.y() - (newy - self.y())
        );
      });
      
      this.reposition(
        newx,
        newy        
      );
      this.el.style.width = width + "px";
      this.el.style.height = height + "px";
    }
  },
  collide: function collide(other, threshold) {
    var x = other.x(),
        y = other.y(),
        results, result;

    if (((x + threshold) > this.bounds.left)
        && ((x - threshold) < this.bounds.right)
        && ((y + threshold) > this.bounds.top)
        && ((y - threshold) < this.bounds.bottom)) {
      // we refine the collision detection
      results = this.children.map(function(child) {
        return child.collide(other, threshold);
      });

      // remove all children that do NOT collide
      result = results.shift();
      
      while (result && !result.collides) {
        result = results.shift();
      }

      // if there are results left, _other_ collides
      return { collides: !!results.length };
    }
    else {
      // other is not within bounds
      return { collides: false };
    }
  }
};

com.extend(Chain.prototype, chainMethods);

module.exports = Chain;

