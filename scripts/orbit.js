//var com = require("./common.js");

function Orbit(logic) {
  this.el = document.createElement("article");
  this.$el = interact(this.el);

  this.el.classList.add("orbit");

  // this.el.appendChild(com.leftBracket);
  // be sure to insert new nodes before closing bracket
  

  this.logic = logic;
  this.nodes = [];
  

  this.groups = [];
}

Orbit.prototype = {
  add: function(e) {
    var node = e.detail;

    this.change({
      type: "add",
      node: node
    });
  },
  move: function(e) {
    var node = e.detail;

    this.change({
      type: "move",
      node: node
    });
  },
  remove: function(e) {
    var node = e.detail;

    this.change({
      type: "remove",
      node: node
    });
  },
  change: function(detail) {
    var operations, self = this;
    operations = this.logic.calculateChanges(this.nodes, detail);

    operations.forEach(function(op) {
      op(self);
    });

    // com.dispatcher(this.el, "chains:changed", {
    //   bubbles: true,
    //   cancelable: false,
    //   detail: groups
    // });

//    pattern = this.logic.createPatterns(this.);
    
    // com.dispatcher(this.el, "pattern:changed", {
    //   bubbles: true,
    //   cancelable: false,
    //   detail: pattern
    // })();
  },
  append: function(node) {
    this.nodes.push(node);
    this.el.appendChild(node.el);
  }
};

module.exports = Orbit;
