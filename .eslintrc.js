module.exports = {
  "env": {
    "browser": true,
    "commonjs": true
  },
  "extends": "eslint:recommended",
  "globals": {
    "Fraction": true,
    "$p": true,
    "$t": true,
    "$l": true,
    "$i": true,
    "interact": true,
    "game": true,
    "Parse": true,
    "Pattern": true,
    "Event": true,
    "WebDirt": true
  },
  "rules": {
    "no-debugger": ["off"],
    "one-var": ["error", "always"],
    "no-shadow": ["error"],
    "space-infix-ops": ["error"],
    "operator-linebreak": ["error", "before"],
    "max-len": ["error", { "ignoreUrls": true }],
    "indent": [
      "error",
      2,
      { "VariableDeclarator": 2 }
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "always"
    ],
    "consistent-return": "error"
  }
};
