import Control.Monad
import qualified Language.Haskell.Interpreter.Server as Server
import qualified Language.Haskell.Interpreter as I
import Language.Haskell.Interpreter (OptionVal((:=)))
import Sound.Tidal.Context
import System.IO

runme :: IO ([Pattern String])
runme = do s <- getContents
           r <- I.runInterpreter (testHint $ lines s)
           case r of
             Left err -> do printInterpreterError err
                            return []
             Right xs -> return xs

-- observe that Interpreter () is an alias for InterpreterT IO ()
testHint :: [String] -> I.Interpreter ([Pattern String])
testHint exprs =
    do
      I.set [I.languageExtensions := [I.OverloadedStrings]]
      I.setImportsQ [("Prelude", Nothing), ("Sound.Tidal.Context", Nothing)]
      mapM (f) exprs
  where f expr = do say $ "compiling: " ++ expr
                    I.interpret expr (I.as :: Pattern String)

say :: String -> I.Interpreter ()
say = I.liftIO . putStrLn

printInterpreterError :: I.InterpreterError -> IO ()
printInterpreterError e = putStrLn $ "Ups... " ++ (show e)


main = do
  handle <- Server.start
  runme
